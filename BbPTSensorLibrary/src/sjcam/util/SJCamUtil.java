package sjcam.util;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Vector;





import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ThumbnailUtils;
import android.os.Environment;
import android.provider.MediaStore.Video.Thumbnails;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;

/**
 * Utility class for doing different things, espacially with the Action-Cam connected via WLAN.
 * Loading Images/Videos, retrieve FileLists, extract ThumbNails, ....
 * @author Alexander
 *
 */
public class SJCamUtil {

	public enum FileType {IMAGES, VIDEOS};
	public static final String baseURL = "http://192.168.1.254";
	public static final String baseFolder = Environment.getExternalStorageDirectory() + "/bbpm";
	public static final String baseFolderThumbnails = baseFolder + "/thumbs";
	public static final String baseFolderPositionCalculations = baseFolder + "/posCalculations";
	private static final String TAG = "SJCamUtil";
	private static final String IMAGES = "PHOTO";
	private static final String VIDEOS = "MOVIE";
	private static final String pathToCurrentFileListImages = baseFolder + "/currentFileListImages.json"; 
	private static final String pathToCurrentFileListVideos = baseFolder + "/currentFileListVideos.json"; 
	
	public static final int THUMBSIZE = 100;	
	
	
	
	/**
	 * Helper-Method to extract a single file name from one Table-Row in the HTML-Stream of the File-List
	 * @param tr
	 * @return a single fileName (or subPath)
	 */
	public static String extractFileNameFromTableRow(String tr) {
		String[] splits = tr.split("\"");
		if(splits.length < 2) {
			log("Not enough splits");
			return null;
		}
		if(!splits[1].startsWith("/")) {
			log("file-Name not used -> not starting with /  :" + splits[1]);
			return null;
		}
//		System.out.println(Arrays.toString(splits));
		log("Found Pic-File: " + splits[1]);
		return splits[1];
	}
	
	public static void log(String log) {
		Log.e(TAG, log);
	}
	
	
	/**
	 * Extract a ThumbNail either from a Video or an Image.
	 * @param fileName
	 * @param fileType
	 * @return path to the extracted and saved ThumbNail
	 */
	public static String extractThumbNail(String fileName, FileType fileType) {
		Bitmap thumbnail = null;
		if(fileType == FileType.IMAGES) {
			thumbnail= ThumbnailUtils.extractThumbnail(BitmapFactory.decodeFile(SJCamUtil.baseFolder + fileName), THUMBSIZE, THUMBSIZE);			
		} else if(fileType == FileType.VIDEOS) {
			thumbnail = ThumbnailUtils.createVideoThumbnail(baseFolder + fileName, Thumbnails.MICRO_KIND);
		}
		if(thumbnail == null) {
			log("Error while creating ThumbNail! ");
			return null;
		}
		return SJCamUtil.saveBitmapToFile(thumbnail, SJCamUtil.baseFolderThumbnails + fileName, fileType );
	}
	
	/**
	 * Method to save a given Bitmap to a path.
	 * @param bmp
	 * @param path
	 * @param fileType
	 * @return
	 */
	public static String saveBitmapToFile(Bitmap bmp, String path, FileType fileType) {
		
//		File f;
		FileOutputStream out = null;
		try {
//			f = new File(path);
//			f.createNewFile();
			String folderPath = "";
			if(fileType == FileType.IMAGES) folderPath = Environment.getExternalStorageDirectory() + "/bbpm/thumbs/DCIM/PHOTO";
			else if(fileType == FileType.VIDEOS) folderPath = Environment.getExternalStorageDirectory() + "/bbpm/thumbs/DCIM/MOVIE";
			File mFolder = new File(folderPath);
            if (!mFolder.exists()) {
                mFolder.mkdirs();
            }
			
		    out = new FileOutputStream(path);
		    bmp.compress(Bitmap.CompressFormat.JPEG, 100, out); // bmp is your Bitmap instance
		    // PNG is a lossless format, the compression factor (100) is ignored
		} catch (Exception e) {
		    log("ex 1: " + e.toString());
		    return null;
		} finally {
		    try {
		        if (out != null) {
		            out.close();
		        }
		    } catch (IOException e) {
		    	 log("ex 2: " + e.toString());
		    	 return null;
		    }
		}
		return path;
	}
	
	
	public static String getPathToFileList(FileType fileType) {
		switch(fileType) {
		case IMAGES: return pathToCurrentFileListImages;
		case VIDEOS: return pathToCurrentFileListVideos;
		}
		return null;
	}
	
	/**
	 * Method to restore the last saved state from a JSON-File including a List of {@link ListItem}s.
	 * 
	 * @param fileType
	 * @return Returns either a list of Images or Videos which can be used to work in the app with.
	 */
	public static Vector<ListItem> restoreState(FileType fileType) {
		Gson gson = new Gson();
		ListItem[] items;
		Vector itemList = new Vector<ListItem>();
		String pathToCurrentFileList = getPathToFileList(fileType);
		
		try {
			items = gson.fromJson(new FileReader(new File(pathToCurrentFileList)), ListItem[].class);
			 itemList.addAll(Arrays.asList(items));
		} catch (JsonSyntaxException | JsonIOException | FileNotFoundException e) {
			log("Error while restoring state! " + e.toString());
		}
		return itemList;
	}

	
	
	
	/**
	 * Start video-recording on the SJCAM SJ4000 via WIFI-Connection
	 * URL: http://192.168.1.254/?custom=1&cmd=2001&par=1
	 */
	public static boolean startRecording(){
		return executeCommand("2001", "1");
	}
	
	/**
	 * Stop video-recording on the SJCAM SJ4000 via WIFI-Connection
	 * URL: http://192.168.1.254/?custom=1&cmd=2001&par=0
	 */
	public static boolean stopRecording(){
		return executeCommand("2001", "0");
	}
	
	public static final int PREVIEW_SIZE_320_x_240 = 4;
	public static final int PREVIEW_SIZE_640_x_360 = 3;
	
	public static boolean setPreviewSize(int previewSize) {
		return executeCommand("2010", "" + previewSize);
	}
	
	/**
	 * Executing any possible command on the SJCAM SJ4000 WIFI
	 * @param cmd - the Id of the command to execute. Possible values are: 2001 (start/stop recording), 2010 (changing the live preview-resolution), 2008 (showing the datetimestamp), ...
	 * @param par - an optional integer parameter for the commands. 0 means OFF and 1 means ON. On multiply choice then 0 - n for the different options.
	 * @return If the execution was successful.
	 * @throws IOException
	 */
	public static boolean executeCommand(String cmd, String par) {
		File f = new File(baseFolder + "/" + "result_cmd_" + System.currentTimeMillis() + ".txt");
		URL cmdUrl;
		try {
			cmdUrl = new URL(baseURL + "/?custom=1&cmd=" + cmd + "&par=" + par);
			Log.e(TAG, "Executing cmd: " + cmd);
			
			long startTime = System.currentTimeMillis();
			
//			FileUtils.copyURLToFile(cmdUrl, f, 1000, 1000);,
			
			
			   HttpURLConnection urlConnection = (HttpURLConnection) cmdUrl.openConnection();
			   try {
			     InputStream in = new BufferedInputStream(urlConnection.getInputStream());
//			     readStream(in);
			   } finally {
			     urlConnection.disconnect();
			   }
			 
			
			
			
			long endTime = System.currentTimeMillis();
			log("Executed Command in " + (endTime-startTime) + "ms");
			boolean result = true;
			return result;
		} catch (MalformedURLException e) {
			Log.e(TAG, e.toString());
			return false;
		} catch (IOException e) {
			Log.e(TAG, e.toString());
			return false;
		}
	}	
	
 
}
