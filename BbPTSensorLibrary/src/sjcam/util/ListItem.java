package sjcam.util;


/**
 * A {@link ListItem} is simply a data-class for the {@link ListView} in the {@link BaseFragment}.
 * Including a path, description, thumbPath and an imgPath.
 * @author Alexander
 *
 */
public class ListItem {

	public String path;
	public String description;
	public String thumbPath;
	public String imgPath;
	
	public ListItem(String path, String description, String thumbPath, String imgPath) {
		this.path = path;
		this.description = description;
		this.thumbPath = thumbPath;
		this.imgPath = imgPath;
	}
	
	@Override
	public boolean equals(Object o) {
		if(o instanceof ListItem) {
			ListItem l = (ListItem) o;
			if(l.path.equals(this.path)) {
				return true;
			}
		}
		return false;
	}
	
}
