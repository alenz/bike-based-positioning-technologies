package at.fhooe.mcm.bbpt.sensorlibrary;

import java.util.Arrays;
import java.util.HashSet;

import at.fhooe.mcm.bbpt.sensorlibrary.util.Util;

public class DummyDistanceSensor extends Sensor {

	private static final int UPDATE_TIME = 500;	// time between 2 sensor-value updates in ms
	private static final float DISTANCE_INCREMENT = 0.5f; // the distance which will be added to the total distance on every update-cycle (so we have a constant speed in this DummyDistanceSensor)
	
	public static final String TAG = "DummyDistanceSensor::";
	
	private static final HashSet<Integer> availableSensorDataTypes = new HashSet<Integer>(Arrays.asList(new Integer[]{SensorData.TYPE_DISTANCE}));
		
	private int accuracy = at.fhooe.mcm.bbpt.sensorlibrary.Sensor.ACCURACY_GOOD;
	
	private DummyDistanceSensorUpdateThread updateThread;
	
	public DummyDistanceSensor(SensorManager sensorManager) {
		super(sensorManager);
	}

	@Override
	public boolean connect() {
		if(updateThread != null) {
			updateThread.setRun(false);
		}
		
		updateThread = new DummyDistanceSensorUpdateThread();
		updateThread.setRun(true);
		updateThread.start();
		return true;
	}

	@Override
	public void disconnect() {
		if(updateThread != null) {
			updateThread.setRun(false);		
		}
	}

	@Override
	public boolean isConnected() {
		if(updateThread == null) {
			return false;
		} else {
			return updateThread.isRun();
		}
	}

	@Override
	public HashSet<Integer> getAvailableSensorDataTypes() {
		return availableSensorDataTypes;
	}
	
	private class DummyDistanceSensorUpdateThread extends Thread {
		
		private static final String TAG = "DummyDistanceSensorUpdateThread";
		
		private boolean run;
		
		private float distance;
		
		public DummyDistanceSensorUpdateThread () {
			this.run = false;
			this.distance = 0;
		}
		
		public boolean isRun() {
			return run;
		}

		public void setRun(boolean run) {
			this.run = run;
		}

		@Override
		public void run() {
			while(run) {				
				try {
					sleep(UPDATE_TIME);
					
				} catch (InterruptedException e) {
					Util.log(TAG, e.toString());
				}
				distance = distance + DISTANCE_INCREMENT;
				sensorManager.onNewSensorDataReceived(new SensorEvent(SensorData.TYPE_DISTANCE, Sensor.TYPE_DUMMY_DISTANCE_SENSOR, new float[]{distance}, System.currentTimeMillis(), ACCURACY_GOOD));
			}
		}
	}

}
