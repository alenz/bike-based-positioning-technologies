package at.fhooe.mcm.bbpt.sensorlibrary;

/**
 * Data-Container-Class to hold relevant information for inertial sensing (sensor fusion) will be used in the Prediction-Model
 * of the Kalman-Filter-Implementation
 * @author Alexander
 *
 */
public class InertialData {

	public double angle;
	public double distance;
	public long timestamp;
	public long sessionTime;
	
	public InertialData( double angle, double distance, long timestamp, long sessionTime) {
		this.angle = angle;
		this.distance = distance;
		this.timestamp = timestamp;
		this.sessionTime = sessionTime;
	}
	
	@Override
	public String toString() {
		return "<InnertialData: distance=" + distance + " , angle=" + angle + " , sessiontime: " + sessionTime + ">";
	}
	
}
