package at.fhooe.mcm.bbpt.sensorlibrary;

import java.util.Arrays;
import java.util.HashSet;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import at.fhooe.mcm.bbpt.sensorlibrary.filter.SimpleWindowAvgFilter;
import at.fhooe.mcm.bbpt.sensorlibrary.util.Util;

public class AltitudeSensor extends at.fhooe.mcm.bbpt.sensorlibrary.Sensor implements SensorEventListener {

	public static final String TAG = "AltitudeSensor::";
	
	private static final HashSet<Integer> availableSensorDataTypes = new HashSet<Integer>(Arrays.asList(new Integer[]{SensorData.TYPE_ALTITUDE}));
	
	private float startAltitude = Float.MIN_VALUE;
	
	// android device sensor manager
	private SensorManager androidSensorManager;
	protected Context context;
	
	private SimpleWindowAvgFilter filter;
	private static final int FILTER_WINDOW_SIZE = 10;
	
	private int accuracy = at.fhooe.mcm.bbpt.sensorlibrary.Sensor.ACCURACY_GOOD;
	
	public AltitudeSensor(at.fhooe.mcm.bbpt.sensorlibrary.SensorManager sensorManager, Context context) {
		super(sensorManager);
		this.context = context;
		
		filter = new SimpleWindowAvgFilter(FILTER_WINDOW_SIZE);
		// initialize your android device sensor capabilities
		androidSensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
	}

	@Override
	public boolean connect() {
		// for the system's orientation sensor registered listeners
		boolean connected;
		connected = androidSensorManager.registerListener(this, androidSensorManager.getDefaultSensor(Sensor.TYPE_PRESSURE),
						SensorManager.SENSOR_DELAY_UI);
		if(connected) {
			connectionState = at.fhooe.mcm.bbpt.sensorlibrary.Sensor.STATE_CONNECTED;
		} else {
			connectionState = at.fhooe.mcm.bbpt.sensorlibrary.Sensor.STATE_DISCONNECTED;
		}
		return connected;
	}

	@Override
	public void disconnect() {
		androidSensorManager.unregisterListener(this);
		connectionState = at.fhooe.mcm.bbpt.sensorlibrary.Sensor.STATE_DISCONNECTED;
		Util.log(TAG, "disconnected AltitudeSensor!");
	}

	@Override
	public boolean isConnected() {
		return (connectionState == STATE_CONNECTED);
	}

	@Override
	public HashSet<Integer> getAvailableSensorDataTypes() {
		return availableSensorDataTypes;
	}

	@Override
	public void onAccuracyChanged(Sensor sensor, int androidAccuracy) {
		switch(androidAccuracy) {
		case SensorManager.SENSOR_STATUS_ACCURACY_HIGH:
			accuracy = at.fhooe.mcm.bbpt.sensorlibrary.Sensor.ACCURACY_GOOD;
			break;
		case SensorManager.SENSOR_STATUS_ACCURACY_MEDIUM:
			accuracy = at.fhooe.mcm.bbpt.sensorlibrary.Sensor.ACCURACY_MEDIUM;
			break;
		default:
			accuracy = at.fhooe.mcm.bbpt.sensorlibrary.Sensor.ACCURACY_BAD;
			break;
		}
		sensorManager.onSensorAccuracyChanged(at.fhooe.mcm.bbpt.sensorlibrary.Sensor.TYPE_BAROMETRIC_ALTITUDE_SENSOR, SensorData.TYPE_ALTITUDE, accuracy);
	}

	@Override
	public void onSensorChanged(SensorEvent event) {
		// when pressure value is changed, this method will be called.
        float pressure_value = 0.0f;
        float height = 0.0f;
        float relativeAltitude = Float.MIN_VALUE;
        if( Sensor.TYPE_PRESSURE == event.sensor.getType() ) {
          pressure_value = event.values[0];
//          System.out.println("PRESSURE" + pressure_value);
          height = SensorManager.getAltitude(SensorManager.PRESSURE_STANDARD_ATMOSPHERE, pressure_value);
          height = filter.filterNewValue(height);
          if(startAltitude == Float.MIN_VALUE) {
        	  startAltitude = height;
          }
         
          relativeAltitude = height - startAltitude;
          
        }
		sensorManager.onNewSensorDataReceived(new at.fhooe.mcm.bbpt.sensorlibrary.SensorEvent(SensorData.TYPE_ALTITUDE, at.fhooe.mcm.bbpt.sensorlibrary.Sensor.TYPE_BAROMETRIC_ALTITUDE_SENSOR, new float[]{height, pressure_value, relativeAltitude}, System.currentTimeMillis(), accuracy));
	}

}
