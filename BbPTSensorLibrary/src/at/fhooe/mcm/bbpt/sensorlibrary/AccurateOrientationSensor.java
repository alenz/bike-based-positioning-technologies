package at.fhooe.mcm.bbpt.sensorlibrary;

import java.util.Arrays;
import java.util.HashSet;

import android.content.Context;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import at.fhooe.mcm.bbpt.sensorlibrary.util.Util;
import android.hardware.Sensor;

/**
 * The {@link AccurateOrientationSensor} is a Wrapper of the Android {@link Sensor}.TYPE_ROTATION_VECTOR which is a
 * sensor fusion of Accelerametor, Gyroscoope and Magnetometer resulting in accurate and stable Orientation-values.
 * @author Alexander
 *
 */
public class AccurateOrientationSensor extends at.fhooe.mcm.bbpt.sensorlibrary.Sensor implements SensorEventListener {

public static final String TAG = "AccurateOrientationSensor::";
	
	private static final HashSet<Integer> availableSensorDataTypes = new HashSet<Integer>(Arrays.asList(new Integer[]{SensorData.TYPE_ORIENTATION}));
	
	// android device sensor manager
	private SensorManager androidSensorManager;
	protected Context context;
	
	private int accuracy = at.fhooe.mcm.bbpt.sensorlibrary.Sensor.ACCURACY_GOOD;
	
	private float[] orientation = new float[3];
	private float[] rMat = new float[9];
	
	public AccurateOrientationSensor(at.fhooe.mcm.bbpt.sensorlibrary.SensorManager sensorManager, Context context) {
		super(sensorManager);
		this.context = context;
		
		// initialize your android device sensor capabilities
		androidSensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
	}

	@Override
	public boolean connect() {
		// for the system's orientation sensor registered listeners
		boolean connected;
		connected = androidSensorManager.registerListener(this, androidSensorManager.getDefaultSensor(android.hardware.Sensor.TYPE_ROTATION_VECTOR),
						SensorManager.SENSOR_DELAY_GAME);
		Util.log(TAG, "AccOrrient state: " + connected);
		
		if(connected) {
			Util.log(TAG, "connected AccurateOrientationSensor!");
			connectionState = at.fhooe.mcm.bbpt.sensorlibrary.Sensor.STATE_CONNECTED;
		} else {
			connectionState = at.fhooe.mcm.bbpt.sensorlibrary.Sensor.STATE_DISCONNECTED;
		}
		return connected;
	}

	@Override
	public void disconnect() {
		androidSensorManager.unregisterListener(this);
		connectionState = at.fhooe.mcm.bbpt.sensorlibrary.Sensor.STATE_DISCONNECTED;
		Util.log(TAG, "disconnected AccurateOrientationSensor!");
	}

	@Override
	public boolean isConnected() {
		return (connectionState == STATE_CONNECTED);
	}

	@Override
	public HashSet<Integer> getAvailableSensorDataTypes() {
		return availableSensorDataTypes;
	}

	@Override
	public void onAccuracyChanged(Sensor sensor, int androidAccuracy) {
		switch(androidAccuracy) {
		case SensorManager.SENSOR_STATUS_ACCURACY_HIGH:
			accuracy = at.fhooe.mcm.bbpt.sensorlibrary.Sensor.ACCURACY_GOOD;
			break;
		case SensorManager.SENSOR_STATUS_ACCURACY_MEDIUM:
			accuracy = at.fhooe.mcm.bbpt.sensorlibrary.Sensor.ACCURACY_MEDIUM;
			break;
		default:
			accuracy = at.fhooe.mcm.bbpt.sensorlibrary.Sensor.ACCURACY_BAD;
			break;
		}
		sensorManager.onSensorAccuracyChanged(at.fhooe.mcm.bbpt.sensorlibrary.Sensor.TYPE_ACCURATE_ANDROID_ORIENTATION_SENSOR, SensorData.TYPE_ORIENTATION, accuracy);
	}

	@Override
	public void onSensorChanged(SensorEvent event) {
		// get the angle around the z-axis rotated
	
		if( event.sensor.getType() == Sensor.TYPE_ROTATION_VECTOR ){
			// calculate the rotation matrix
			SensorManager.getRotationMatrixFromVector( rMat, event.values );
			// get the azimuth value (orientation[0]) in degree
			int mAzimuth = (int) ( Math.toDegrees( SensorManager.getOrientation( rMat, orientation )[0] ) + 360 ) % 360;
			sensorManager.onNewSensorDataReceived(new at.fhooe.mcm.bbpt.sensorlibrary.SensorEvent(SensorData.TYPE_ORIENTATION, at.fhooe.mcm.bbpt.sensorlibrary.Sensor.TYPE_ACCURATE_ANDROID_ORIENTATION_SENSOR, new float[]{mAzimuth}, System.currentTimeMillis(), accuracy));
		}
	}


}
