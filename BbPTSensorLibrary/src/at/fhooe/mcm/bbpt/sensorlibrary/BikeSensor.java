package at.fhooe.mcm.bbpt.sensorlibrary;

import java.util.Arrays;
import java.util.HashSet;

import android.bluetooth.BluetoothGattCharacteristic;
import android.content.Context;
import android.util.Log;
import at.fhooe.mcm.bbpt.sensorlibrary.util.Util;

public class BikeSensor extends BLESensor{

	private static final HashSet<Integer> availableSensorDataTypes = new HashSet<Integer>(Arrays.asList(new Integer[]{SensorData.TYPE_CADENCE, SensorData.TYPE_DISTANCE, SensorData.TYPE_SPEED}));
	
	private float circumference;
	
	private int totalCrankRevolutions;
	private int lastCrankRevolutionsSensor;
	private int currentCadence; // revolutions/minute
	private int lastCrankRevolutionTime;
	private int lastCrankRevolutionSessionTime;
	
	private int totalWheelRevolutions;
	private int lastWheelRevolutionsSensor;
	private float distance; // m
	private int lastWheelRevolutionTime;
	private int lastWheelRevolutionSessionTime;
	private float currentSpeed; // m/s
	
	public BikeSensor(SensorManager sensorManager, Context context, String name, String deviceAddress, float wheelCircumference) {
		super(sensorManager, context, name, deviceAddress, CSC_Measurement, CSC_Service);
		totalCrankRevolutions = 0;
		lastCrankRevolutionsSensor = -1;
		currentCadence = 0;
		lastCrankRevolutionTime = -1;
		lastCrankRevolutionSessionTime = -1;
		
		totalWheelRevolutions = 0;
		lastWheelRevolutionsSensor = -1;
		circumference = wheelCircumference;
		distance = 0;
		lastWheelRevolutionTime = -1;
		currentSpeed = 0f;
		lastWheelRevolutionSessionTime = -1;
	}

	@Override
	public void processChangedCharacteristic(
			BluetoothGattCharacteristic characteristic) {
//		if(CSC_Measurement.equals(characteristic.getUuid().toString())) {
		
		long time = System.currentTimeMillis();
		
        	final int cumulativeWheelRevolutions = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT32, 1);
        	if(lastWheelRevolutionsSensor < 0) {
        		lastWheelRevolutionsSensor = cumulativeWheelRevolutions;
        	}
        	int wheelRevolutionsSinceLastDataTransmission = cumulativeWheelRevolutions-lastWheelRevolutionsSensor;
        	if(wheelRevolutionsSinceLastDataTransmission < 0) {
        		wheelRevolutionsSinceLastDataTransmission = 0;        		
        	}
        	totalWheelRevolutions = totalWheelRevolutions + wheelRevolutionsSinceLastDataTransmission;
        	lastWheelRevolutionsSensor = cumulativeWheelRevolutions;
        	distance = (totalWheelRevolutions * circumference);
        	
        	Log.d(TAG, String.format("Received cumulative Wheel revolutions: %d", cumulativeWheelRevolutions));
     	
        	final int currentWheelEventTime = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT16, 5);
        	// begin speed calculation
        	if(lastWheelRevolutionTime < 0) {
        		lastWheelRevolutionTime = currentWheelEventTime;
        		currentSpeed = 0;
        	} else {
        		int timeForLastWheelRevolutions = currentWheelEventTime - lastWheelRevolutionTime;
        		if(timeForLastWheelRevolutions < 0) {	// check if we have an overflow (every 64 seconds)
        			timeForLastWheelRevolutions = timeForLastWheelRevolutions + 65536;
        		}
        		float timeForLastWheelRevolutionsInSeconds = ((float)timeForLastWheelRevolutions) / 1024f;
        		currentSpeed = (((float)wheelRevolutionsSinceLastDataTransmission)*((float)circumference))/timeForLastWheelRevolutionsInSeconds;
        		
        	}
        	Log.d(TAG, "Current Speed: " + currentSpeed);
        	lastWheelRevolutionTime = currentWheelEventTime;
        	// end speed calculation
        	Log.d(TAG, String.format("Received last Wheel Event time: %d", currentWheelEventTime));
      	
        	
        	final int cumulativeCrankRevolutions = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT16, 7);
        	if(lastCrankRevolutionsSensor < 0) {
        		lastCrankRevolutionsSensor = cumulativeCrankRevolutions;
        	}
        	int crankRevolutionsSinceLastDataTransmission = cumulativeCrankRevolutions - lastCrankRevolutionsSensor;
        	totalCrankRevolutions = totalCrankRevolutions + crankRevolutionsSinceLastDataTransmission;
        	lastCrankRevolutionsSensor = cumulativeCrankRevolutions;
        	
        	Log.d(TAG, String.format("Received cumulative Crank revolutions: %d", cumulativeCrankRevolutions));
       
        	final int currentCrankEventTime = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT16, 9);
        	if(lastCrankRevolutionTime < 0) {
        		lastCrankRevolutionTime = currentCrankEventTime;
        		currentCadence = 0;
        	} else {
        		int timeForLastCrankRevolutions = currentCrankEventTime - lastCrankRevolutionTime;
        		if(timeForLastCrankRevolutions < 0) {	// check if we have an overflow (every 64 seconds)
        			timeForLastCrankRevolutions = timeForLastCrankRevolutions + 65536;
        		}
        		float timeForLastCrankRevolutionsInSeconds = ((float)timeForLastCrankRevolutions) / 1024f;
        		if(crankRevolutionsSinceLastDataTransmission > 0) {
        			float timePerCrankRevolution = timeForLastCrankRevolutionsInSeconds / crankRevolutionsSinceLastDataTransmission;
        			currentCadence = (int)(60 / timePerCrankRevolution);        			
        		} else {
        			currentCadence = 0;
        		}
        		Log.d(TAG, "CurrentCadence: " + currentCadence);
        	}
        	lastCrankRevolutionTime = currentCrankEventTime;
        	
        	Log.d(TAG, String.format("Received last Crank Event time: %d", currentCrankEventTime));

        	
        
//        }
        	Util.log(TAG, "Send new Bike-Distance-Event: distance: " + distance + " , totalWheelRevolutions: " + totalWheelRevolutions);
        	sensorManager.onNewSensorDataReceived(new SensorEvent(SensorData.TYPE_DISTANCE, Sensor.TYPE_BLE_BIKE_SENSOR, new float[]{distance, totalWheelRevolutions}, time, Sensor.ACCURACY_GOOD));
        	sensorManager.onNewSensorDataReceived(new SensorEvent(SensorData.TYPE_CADENCE, Sensor.TYPE_BLE_BIKE_SENSOR, new float[]{currentCadence, totalCrankRevolutions}, time, Sensor.ACCURACY_GOOD));
        	sensorManager.onNewSensorDataReceived(new SensorEvent(SensorData.TYPE_SPEED, Sensor.TYPE_BLE_BIKE_SENSOR, new float[]{currentSpeed}, time, Sensor.ACCURACY_GOOD));
        	//TODO: eventually add some more Events which will be fired: avgSpeed, avgCadence!
	}

	@Override
	public HashSet<Integer> getAvailableSensorDataTypes() {
		return availableSensorDataTypes;
	}
	
	@Override
	public void disconnect() {
		totalCrankRevolutions = 0;
		lastCrankRevolutionsSensor = -1;
		currentCadence = 0;
		lastCrankRevolutionTime = -1;
		lastCrankRevolutionSessionTime = -1;
		
		totalWheelRevolutions = 0;
		lastWheelRevolutionsSensor = -1;
		distance = 0;
		lastWheelRevolutionTime = -1;
		currentSpeed = 0f;
		lastWheelRevolutionSessionTime = -1;
		
		Util.log(TAG, "disconnected BikeSensor!");
		super.disconnect();
	}

}
