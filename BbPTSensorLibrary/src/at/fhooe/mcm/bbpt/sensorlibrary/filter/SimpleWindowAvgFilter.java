package at.fhooe.mcm.bbpt.sensorlibrary.filter;

import java.util.Iterator;

import org.apache.commons.collections4.queue.CircularFifoQueue;


public class SimpleWindowAvgFilter extends Filter{

	private static final String FILTER_NAME = "SimpleWindowAvgFilter";
	
	private int windowSize = 5;
	private CircularFifoQueue<Float> fifoQue;
	
	public SimpleWindowAvgFilter(int windowSize) {
		this.windowSize = windowSize;
		fifoQue = new CircularFifoQueue<Float>(windowSize);
	}
	
	@Override
	public String getFilterName() {
		return FILTER_NAME;
	}

	@Override
	public float filterNewValue(float newValue) {
		fifoQue.add(newValue);
		Iterator<Float> iterator = fifoQue.iterator();
		float v;
		float sum = 0;
		int counter = 0;
		
		while(iterator.hasNext()) {
			v = iterator.next();
			sum += v;
			counter++;
		}
		return sum/counter;
	}	

}
