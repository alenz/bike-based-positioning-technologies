package at.fhooe.mcm.bbpt.sensorlibrary;

import java.util.HashSet;

public abstract class Sensor {
	
	public static final int TOTAL_NUMBER_OF_SENSORS = 5;

	public static final int TYPE_BLE_BIKE_SENSOR = 0;
	public static final int TYPE_ANDROID_ORIENTATION_SENSOR = 1;
	public static final int TYPE_DUMMY_DISTANCE_SENSOR = 2;
	public static final int TYPE_ACCURATE_ANDROID_ORIENTATION_SENSOR = 3;
	public static final int TYPE_BAROMETRIC_ALTITUDE_SENSOR = 4;
	//... any other Sensor-Types supported by this sensorlibrary -> !!! important note: don't forget to increment also the TOTAL_NUMBER_OF_SENSORS when adding a new sensor !!!
	
	protected int connectionState = STATE_DISCONNECTED;

    public static final int STATE_DISCONNECTED = 0;
    public static final int STATE_CONNECTING = 1;
    public static final int STATE_CONNECTED = 2;
    
    public static final int ACCURACY_GOOD = 0;
    public static final int ACCURACY_MEDIUM = 1;
    public static final int ACCURACY_BAD = 2;

	
	public abstract boolean connect();
	
	public abstract void disconnect();
	
	public abstract boolean isConnected();
	
	protected SensorManager sensorManager;
	
	
	public Sensor(SensorManager sensorManager) {
		this.sensorManager = sensorManager;
	}
	
	public int getConnectionState() {
		return connectionState;
	}
	
	private void setConnectionState(int newConnectionState) {
		connectionState = newConnectionState;
	}
	
	public abstract HashSet<Integer> getAvailableSensorDataTypes();
}
