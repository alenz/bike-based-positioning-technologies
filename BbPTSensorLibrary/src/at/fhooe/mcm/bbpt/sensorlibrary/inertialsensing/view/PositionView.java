package at.fhooe.mcm.bbpt.sensorlibrary.inertialsensing.view;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import at.fhooe.mcm.bbpt.sensorlibrary.inertialsensing.Position;
import at.fhooe.mcm.bbpt.sensorlibrary.inertialsensing.PositionVector;

public class PositionView extends View{
	
	private static final String TAG = "PositionView::";
	
	public static final int AXIS_DRAWING_MODE_X_Y = 0;
	public static final int AXIS_DRAWING_MODE_X_Z = 1;
	private int axisDrawingMode = AXIS_DRAWING_MODE_X_Y;
	
	private float scaleFactorForPositionDrawing = 2f;	// every pos-coordinate gets multiplied by this factor for visualizing it on the screen
	
	
	private Paint linePaint1;
	private Paint linePaint2;
	private Paint textPaint;

	private Position currentPosition;
	private int currentOrientation;

	private ArrayList<Position> positionHistory;
	private Path path = new Path();
	
	private int centerScreenX = 0;
	private int centerScreenY = 0;
	
	private boolean alreadyMovedPathStartPointToCenter = false;
	
	private int initialXDown = 0;
	private int initialYDown = 0;
	
	private int movingOffsetX = 0;
	private int movingOffsetY = 0;
	
	public PositionView(Context context, AttributeSet attrs) {
		super(context, attrs);

		linePaint1 = new Paint();
		linePaint1.setAntiAlias(true);
		linePaint1.setStrokeWidth(5f);
		linePaint1.setColor(Color.BLACK);
		linePaint1.setStyle(Paint.Style.STROKE);
		linePaint1.setStrokeJoin(Paint.Join.ROUND);
		
		linePaint2 = new Paint();
		linePaint2.setAntiAlias(true);
		linePaint2.setStrokeWidth(5f);
		linePaint2.setColor(Color.GREEN);
		linePaint2.setStyle(Paint.Style.STROKE);
		linePaint2.setStrokeJoin(Paint.Join.ROUND);
		
		textPaint = new Paint();
		textPaint.setAntiAlias(true);
		textPaint.setStrokeWidth(1f);
		textPaint.setColor(Color.RED);
		textPaint.setStyle(Paint.Style.FILL_AND_STROKE);
		textPaint.setStrokeJoin(Paint.Join.ROUND);
		textPaint.setTextSize(20);
		
		positionHistory = new ArrayList<Position>();
		path = new Path();
		centerScreenX = getWidth() / 2;
		centerScreenY = getHeight() / 2;
		currentPosition = new Position(0d, 0d, 0d, -1);
		movingOffsetX = 0;
		movingOffsetY = 0;
	}	
	
	

	@Override
	protected void onDraw(Canvas canvas) {
		centerScreenX = getWidth() / 2;
		centerScreenY = getHeight() / 2;
		if(!alreadyMovedPathStartPointToCenter) {
			alreadyMovedPathStartPointToCenter = true;
		}
		if(axisDrawingMode == AXIS_DRAWING_MODE_X_Y) {
//			canvas.drawText("M1  (-300, 0, -250)" , centerScreenX + Marker1.x, centerScreenY-Marker1.y, textPaint);
//			canvas.drawText("M2  (300, 0, 250)", centerScreenX + Marker2.x, centerScreenY-Marker2.y, textPaint);
			canvas.drawPath(path, linePaint1);
			canvas.drawCircle(centerScreenX + movingOffsetX + ((int)currentPosition.x), centerScreenY + movingOffsetY -((int)currentPosition.y), 15, textPaint);			
		} else {
//			canvas.drawText("M1  (-300, 0, -250)", centerScreenX + Marker1.x, Marker1.z, textPaint);
//			canvas.drawText("M2  (300, 0, 250)", centerScreenX + Marker2.x, Marker2.z, textPaint);
			canvas.drawPath(path, linePaint2);
			canvas.drawCircle(centerScreenX + movingOffsetX + ((int)currentPosition.x), centerScreenY + movingOffsetY -((int)currentPosition.z), 15, textPaint);			
		}
		canvas.drawText("Pos.: " + String.format("%.2f", currentPosition.x) + " , " + String.format("%.2f", currentPosition.y) + " , o:" + currentOrientation , 50, 50, textPaint);
	}

	public void addCurrentPosition(Position currentPos) {
		double x = (currentPos.x*scaleFactorForPositionDrawing);
		double y = (currentPos.y*scaleFactorForPositionDrawing);
		double z = (currentPos.z*scaleFactorForPositionDrawing);
		currentPosition = new Position(x, y, z, -1);
		positionHistory.add(currentPosition);

		if(axisDrawingMode == AXIS_DRAWING_MODE_X_Y) {
			if(path.isEmpty()) {
				path.moveTo(centerScreenX + movingOffsetX + ((int)x), centerScreenY + movingOffsetY -((int)y));
			}
			path.lineTo(centerScreenX + movingOffsetX + ((int)x), centerScreenY + movingOffsetY -((int)y));			
		} else {
			if(path.isEmpty()) {
				path.moveTo(centerScreenX + movingOffsetX + ((int)x), centerScreenY + movingOffsetY -((int)z));	
			}
			path.lineTo(centerScreenX + movingOffsetX + ((int)x), centerScreenY + movingOffsetY -((int)z));	
		}
		postInvalidate();
	}
	
	public void addCurrentPositionVector(PositionVector posVector) {
		Position currentPos = posVector.endPosition;
		double x = (currentPos.x*scaleFactorForPositionDrawing);
		double y = (currentPos.y*scaleFactorForPositionDrawing);
		double z = (currentPos.z*scaleFactorForPositionDrawing);
		currentPosition = new Position(x, y, z, -1);
		positionHistory.add(currentPosition);
		currentOrientation = (int) posVector.orientation;
		
		if(axisDrawingMode == AXIS_DRAWING_MODE_X_Y) {
			if(path.isEmpty()) {
				path.moveTo(centerScreenX + movingOffsetX + ((int)x), centerScreenY + movingOffsetY -((int)y));
			}
			path.lineTo(centerScreenX + movingOffsetX + ((int)x), centerScreenY + movingOffsetY -((int)y));			
		} else {
			if(path.isEmpty()) {
				path.moveTo(centerScreenX + movingOffsetX + ((int)x), centerScreenY + movingOffsetY -((int)z));	
			}
			path.lineTo(centerScreenX + movingOffsetX + ((int)x), centerScreenY + movingOffsetY -((int)z));	
		}
		postInvalidate();
	}
	
	public void clearPath() {
		path.reset();
		postInvalidate();
	}
	
	public void clearPositionHistory() {
		path.reset();
		positionHistory.clear();
		postInvalidate();
	}
	
	public int changeAxisDrawingMode() {
		path.reset();
		postInvalidate();
		
		if(axisDrawingMode == AXIS_DRAWING_MODE_X_Y) {
			axisDrawingMode = AXIS_DRAWING_MODE_X_Z;
		} else {
			axisDrawingMode = AXIS_DRAWING_MODE_X_Y;
		}
		
		return axisDrawingMode;
	}


	public float getScaleFactorForPositionDrawing() {
		return scaleFactorForPositionDrawing;
	}


	public void setScaleFactorForPositionDrawing(float scaleFactorForPositionDrawing) {
		this.scaleFactorForPositionDrawing = scaleFactorForPositionDrawing;
	}
	
	public void zoomIn() {
		if(scaleFactorForPositionDrawing > 1) {
			scaleFactorForPositionDrawing = scaleFactorForPositionDrawing + 1;			
		} else {
			scaleFactorForPositionDrawing = scaleFactorForPositionDrawing * 2;
		}
		clearPath();
		drawPositionHistoryOnPath();
	}
	
	public void zoomOut() {
		if(scaleFactorForPositionDrawing <= 1) {
			scaleFactorForPositionDrawing = scaleFactorForPositionDrawing / 2;
		} else {
			scaleFactorForPositionDrawing = scaleFactorForPositionDrawing - 1;
		}
		clearPath();
		drawPositionHistoryOnPath();
	}
	
	private void drawPositionHistoryOnPath() {
		for(Position p: positionHistory) {
			double x =  (p.x*scaleFactorForPositionDrawing);
			double y =  (p.y*scaleFactorForPositionDrawing);
			double z =  (p.z*scaleFactorForPositionDrawing);
			currentPosition = new Position(x, y, z, -1);
			if(axisDrawingMode == AXIS_DRAWING_MODE_X_Y) {
				if(path.isEmpty()) {
					path.moveTo(centerScreenX + movingOffsetX + ((int)x), centerScreenY + movingOffsetY -((int)y));
				}
				path.lineTo(centerScreenX + movingOffsetX + ((int)x), centerScreenY + movingOffsetY -((int)y));			
			} else {
				if(path.isEmpty()) {
					path.moveTo(centerScreenX + movingOffsetX + ((int)x), centerScreenY + movingOffsetY -((int)z));	
				}
				path.lineTo(centerScreenX + movingOffsetX + ((int)x), centerScreenY + movingOffsetY -((int)z));	
			}
			
		}
		postInvalidate();
	}
	

	public int getMovingOffsetX() {
		return movingOffsetX;
	}

	public void setMovingOffsetX(int movingOffsetX) {
		this.movingOffsetX = movingOffsetX;
	}
	
	public void move(int deltaX, int deltaY) {
		movingOffsetX = movingOffsetX + deltaX;
		movingOffsetY = movingOffsetY + deltaY;
		clearPath();
		drawPositionHistoryOnPath();
	}

	public int getMovingOffsetY() {
		return movingOffsetY;
	}

	public void setMovingOffsetY(int movingOffsetY) {
		this.movingOffsetY = movingOffsetY;
	}

	
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		// This prevents touchscreen events from flooding the main thread
		synchronized (event) {		
				
				// when user touches the screen
				if (event.getAction() == MotionEvent.ACTION_DOWN) {

					// get initial positions
					initialXDown = (int) event.getRawX();
					initialYDown = (int) event.getRawY();
				}
				
				if (event.getAction() == MotionEvent.ACTION_MOVE) {
					int deltaX = (int) (event.getRawX() - initialXDown);
					int deltaY = (int) (event.getRawY() - initialYDown);
					move(deltaX, deltaY);			
					
					// get initial positions
					initialXDown = (int) event.getRawX();
					initialYDown = (int) event.getRawY();
				}

				// when screen is released -> move Postion Offset
				if (event.getAction() == MotionEvent.ACTION_UP) {
					int deltaX = (int) (event.getRawX() - initialXDown);
					int deltaY = (int) (event.getRawY() - initialYDown);
					move(deltaX, deltaY);
				}
			

			return true;
		}
	}

}