package at.fhooe.mcm.bbpt.sensorlibrary.inertialsensing;

public interface InertialSensorListener {

	/**
	 * Listener-method gets called when a new PositionVector was calculated
	 * @param posVector - A {@link PositionVector} with a start- and end-Postion as well as a distance and an orientation
	 */
	public abstract void onNewPositionVectorReceived(PositionVector posVector);
	
}
