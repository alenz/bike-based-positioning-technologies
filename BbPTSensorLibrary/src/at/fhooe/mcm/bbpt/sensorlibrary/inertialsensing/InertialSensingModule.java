package at.fhooe.mcm.bbpt.sensorlibrary.inertialsensing;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

import org.apache.commons.io.FileUtils;

import sjcam.util.SJCamUtil;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.text.format.DateFormat;
import android.util.Log;
import at.fhooe.mcm.bbpt.sensorlibrary.InertialData;
import at.fhooe.mcm.bbpt.sensorlibrary.InertialSessionSensorEvent;
import at.fhooe.mcm.bbpt.sensorlibrary.Sensor;
import at.fhooe.mcm.bbpt.sensorlibrary.SensorData;
import at.fhooe.mcm.bbpt.sensorlibrary.SensorEvent;
import at.fhooe.mcm.bbpt.sensorlibrary.SensorEventListener;
import at.fhooe.mcm.bbpt.sensorlibrary.SensorManager;
import at.fhooe.mcm.bbpt.sensorlibrary.util.Util;

import com.google.gson.Gson;

public class InertialSensingModule implements SensorEventListener{

	private static final String TAG = "InertialSensingModule";

	private Context context;
	
	private SensorManager sensorManager;
	
//	private float sumOrientationValuesSinceLastDistanceUpdate = 0;
//	private int countOrientationValuesSinceLastDistanceUpdate = 0;
	private float startOrientation = Float.NaN;
	
	private ArrayList<PositionVector> positionVectors;
	private float lastDistance = Float.NaN;
	private Position lastPosition = null;
	
	private InertialSensorListener inertialSensorListener;
	
	private float sumOrientations = 0;
	private int orientationsCounter = 0;
	
	private boolean startWithDummyDistanceSensor = false;
	private boolean startWithAccurateOrientationSensor = true;
	private boolean startCameraRecording = false;
	
	private long startTime = 0;
	
	private ArrayList<InertialData> inertialDataList = new ArrayList<InertialData>();
	private ArrayList<InertialData> rawOrientationList = new ArrayList<InertialData>();
	
	private ArrayList<InertialSessionSensorEvent> distanceBikeSensorEvents = new ArrayList<InertialSessionSensorEvent>();
	private ArrayList<InertialSessionSensorEvent> orientationSensorEvents = new ArrayList<InertialSessionSensorEvent>();
	
	
	private float calcAvgOrientation() {
		float avg = sumOrientations/(float)orientationsCounter;
		sumOrientations = 0;
		orientationsCounter = 0;
		return avg;
	}
	
	public InertialSensingModule(Context context, boolean startWithDummyDistanceSensor, boolean startWithAccurateOrientationSensor, boolean startCameraRecording) {
		this.context = context;
		this.startWithDummyDistanceSensor = startWithDummyDistanceSensor;
		this.startWithAccurateOrientationSensor = startWithAccurateOrientationSensor;
		this.startCameraRecording = startCameraRecording;
		sensorManager = SensorManager.getInstance(context);
		positionVectors = new ArrayList<PositionVector>();
		lastPosition = new Position(0, 0, 0, -1);
		
//		sumOrientationValuesSinceLastDistanceUpdate = 0;
//		countOrientationValuesSinceLastDistanceUpdate = 0;
		startOrientation = Float.NaN;
		lastDistance = -1;
		startTime = 0;
	}
	
	public void registerInertialSensorListener(InertialSensorListener listener) {
		inertialSensorListener = listener;
	}
	
	public void startSensing() {
		inertialDataList.clear();
		rawOrientationList.clear();
		distanceBikeSensorEvents.clear();
		orientationSensorEvents.clear();
		new Thread(new Runnable() {
			
			@Override
			public void run() {
				if(startCameraRecording) {
					Util.log(TAG, "Start sending command: " + System.currentTimeMillis());
					SJCamUtil.startRecording();					
				} else {
					Util.log(TAG, "Started InertialSensingModule without Camera Recording.");
				}
				
				startTime = System.currentTimeMillis();
				Util.log(TAG, "StartTime is: " + startTime);
			}
		}).start();
		if(startWithDummyDistanceSensor) {
			sensorManager.registerSensorEventListener(this, Sensor.TYPE_DUMMY_DISTANCE_SENSOR, SensorData.TYPE_DISTANCE);
		} else {
			sensorManager.registerSensorEventListener(this, Sensor.TYPE_BLE_BIKE_SENSOR, SensorData.TYPE_DISTANCE);			
		}
		if(startWithAccurateOrientationSensor) {
			sensorManager.registerSensorEventListener(this, Sensor.TYPE_ACCURATE_ANDROID_ORIENTATION_SENSOR, SensorData.TYPE_ORIENTATION);
			
		} else {
			sensorManager.registerSensorEventListener(this, Sensor.TYPE_ANDROID_ORIENTATION_SENSOR, SensorData.TYPE_ORIENTATION);			
		}
	}
	
	public void stopSensing(Context contex) {
		
		saveInertialData(contex);
		if(startWithDummyDistanceSensor) {
			sensorManager.unregisterSensorEventListener(this, Sensor.TYPE_DUMMY_DISTANCE_SENSOR, SensorData.TYPE_DISTANCE);
		} else {
			sensorManager.unregisterSensorEventListener(this, Sensor.TYPE_BLE_BIKE_SENSOR, SensorData.TYPE_DISTANCE);						
		}
		if(startWithAccurateOrientationSensor) {
			sensorManager.unregisterSensorEventListener(this, Sensor.TYPE_ACCURATE_ANDROID_ORIENTATION_SENSOR, SensorData.TYPE_ORIENTATION);
		} else {
			sensorManager.unregisterSensorEventListener(this, Sensor.TYPE_ANDROID_ORIENTATION_SENSOR, SensorData.TYPE_ORIENTATION);			
		}
	}

	@Override
	public void onNewSensorDataReceived(SensorEvent event) {
		long currentTime = System.currentTimeMillis();
		switch(event.sensorDataType) {
		case SensorData.TYPE_DISTANCE:
			if(lastDistance < 0) {
				lastDistance = event.values[0];
			}
			// process new distance event
			float distanceChanged = event.values[0] - lastDistance;
			lastDistance = event.values[0];
			if(distanceChanged < 0.01) {
				Util.log(TAG, "Distance Change is too less (<0.01) -> no pos-calc");
				return;
			}
			
			if(Float.isNaN(startOrientation)) {
				Util.log(TAG, "Distance event has no StartOrientation so far ! -> its not possible to calc a PositionVector without it.");
				return;
			}
			
			float avgOrientation = calcAvgOrientation();
			float calcOrientation = (90 + (startOrientation - avgOrientation)) % 360;
			
			if(startTime > 0) {
				inertialDataList.add(new InertialData(avgOrientation, distanceChanged, currentTime, currentTime-startTime));
				distanceBikeSensorEvents.add(new InertialSessionSensorEvent(event, currentTime-startTime));
			}
			
//			float avgOrientation = sumOrientationValuesSinceLastDistanceUpdate / countOrientationValuesSinceLastDistanceUpdate;
			PositionVector currentPositionVector = new PositionVector(lastPosition, distanceChanged, calcOrientation);
			positionVectors.add(currentPositionVector);
			lastPosition = currentPositionVector.endPosition;
			Util.log(TAG,"rawOrientation: " + avgOrientation + "startOrientation: " + startOrientation + " , calcOrientation: " + calcOrientation + " , distanceCahnged: " + distanceChanged);
			Util.log(TAG, "Calculated new PositionsVector: " + lastPosition.toStringShort());
			
			if(inertialSensorListener != null) {
				inertialSensorListener.onNewPositionVectorReceived(currentPositionVector);				
			}
//			sumOrientationValuesSinceLastDistanceUpdate = 0;
//			countOrientationValuesSinceLastDistanceUpdate = 0;
			break;
		case SensorData.TYPE_ORIENTATION:
			// process new orientation event
			if(Float.isNaN(startOrientation)) {
				startOrientation = event.values[0];
			}
			
			if(startTime > 0) {
				sumOrientations += event.values[0];
				orientationsCounter++;
				rawOrientationList.add(new InertialData(event.values[0], -1, currentTime, currentTime-startTime));
				orientationSensorEvents.add(new InertialSessionSensorEvent(event, currentTime-startTime));
			}
//			sumOrientationValuesSinceLastDistanceUpdate += event.values[0];
//			countOrientationValuesSinceLastDistanceUpdate++;
			break;
		}
	}

	@Override
	public void onSensorAccuracyChanged(int sensorType, int sensorDataType, int accuracy) {
		Util.log(TAG, "OnSensorAccuracyChanged: sensorType: " + sensorType + " sensorDataType: " + sensorDataType + " to " + accuracy);
	}
	
	
	
	public void saveInertialData(Context context) {
		String path = Environment.getExternalStorageDirectory() + "/bbptsensorlibrary/";
		String pathRawOrientation = path + "rawOrientation_" + startTime;
		String pathInertialData = path + "inertialData_" + startTime;
		String pathPositionVectors = path + "positionVectors_" + startTime;
		String pathSensorEventsBikeDistance = path + "sensorEventsBikeDistance_" + startTime;
		String pathSensorEventsOrienations = path + "sensorEventsOrienations_" + startTime;
		
		Log.e(TAG, "Saved SensorData as JSON-Array to sdcard. " + pathRawOrientation + " and " + pathInertialData);
		Gson gson = new Gson();
		InertialData[] inertialDataArray = new InertialData[inertialDataList.size()];
		inertialDataList.toArray(inertialDataArray);
		String str = gson.toJson(inertialDataArray, InertialData[].class);
		try {
			FileUtils.writeStringToFile(new File(pathInertialData), str);
		} catch (IOException e) {
			Log.e(TAG, "Error while saving inertialDataList: " + e.toString());
		}
		
		InertialData[] rawOrientationArray = new InertialData[rawOrientationList.size()];
		rawOrientationList.toArray(rawOrientationArray);
		String strOrientations = gson.toJson(rawOrientationArray, InertialData[].class);
		try {
			FileUtils.writeStringToFile(new File(pathRawOrientation), strOrientations);
		} catch (IOException e) {
			Log.e(TAG, "Error while saving rawOrientations: " + e.toString());
		}
		
		PositionVector[] positionVectorsArray = new PositionVector[positionVectors.size()];
		positionVectors.toArray(positionVectorsArray);
		String strPosVectors = gson.toJson(positionVectorsArray, PositionVector[].class);
		try {
			FileUtils.writeStringToFile(new File(pathPositionVectors), strPosVectors);
		} catch (IOException e) {
			Log.e(TAG, "Error while saving posVectors: " + e.toString());
		}
		
		InertialSessionSensorEvent[] sensorEventsDistanceBikeArray = new InertialSessionSensorEvent[distanceBikeSensorEvents.size()];
		distanceBikeSensorEvents.toArray(sensorEventsDistanceBikeArray);
		String strSensorEventsDistanceBike = gson.toJson(sensorEventsDistanceBikeArray, InertialSessionSensorEvent[].class);
		try {
			FileUtils.writeStringToFile(new File(pathSensorEventsBikeDistance), strSensorEventsDistanceBike);
		} catch (IOException e) {
			Log.e(TAG, "Error while saving sensorEventsDistanceBikeArray: " + e.toString());
		}
		
		InertialSessionSensorEvent[] sensorEventsOrienationsArray = new InertialSessionSensorEvent[orientationSensorEvents.size()];
		orientationSensorEvents.toArray(sensorEventsOrienationsArray);
		String strSensorEventsOrienation = gson.toJson(sensorEventsOrienationsArray, InertialSessionSensorEvent[].class);
		try {
			FileUtils.writeStringToFile(new File(pathSensorEventsOrienations), strSensorEventsOrienation);
		} catch (IOException e) {
			Log.e(TAG, "Error while saving sensorEventsOrientationsArray: " + e.toString());
		}
		
		
//		Date date = new Date(startTime);
//		java.text.DateFormat dateFormat = android.text.format.DateFormat.getDateFormat(context);
//		String formattedTime = dateFormat.format(date);
		
		String formattedTime = android.text.format.DateFormat.format("yyyy-MM-dd hh:mm:ss", new java.util.Date(startTime)).toString();
		
		
		Intent intent = new Intent(Intent.ACTION_SEND_MULTIPLE);
		intent.setType("text/plain");
		intent.putExtra(Intent.EXTRA_SUBJECT, "Inertial Test Results " + formattedTime + "  (" + startTime + ")");
		intent.putExtra(Intent.EXTRA_TEXT, "Inertial Test Results");
		intent.putExtra(Intent.EXTRA_EMAIL, new String[]{"ateamsolutions@gmx.at"});
		                                
		ArrayList<Uri> uris = new ArrayList<Uri>();
		uris.add(Uri.fromFile(new File(pathInertialData)));
		uris.add(Uri.fromFile(new File(pathRawOrientation)));
		uris.add(Uri.fromFile(new File(pathPositionVectors)));
		uris.add(Uri.fromFile(new File(pathSensorEventsBikeDistance)));
		uris.add(Uri.fromFile(new File(pathSensorEventsOrienations)));

		intent.putParcelableArrayListExtra(Intent.EXTRA_STREAM, uris); 
		context.startActivity(Intent.createChooser(intent, "Send mail"));
	}
}
