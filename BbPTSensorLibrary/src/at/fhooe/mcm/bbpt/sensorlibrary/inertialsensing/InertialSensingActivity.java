package at.fhooe.mcm.bbpt.sensorlibrary.inertialsensing;

import sjcam.util.SJCamUtil;
import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup.LayoutParams;
import at.fhooe.mcm.bbpt.sensorlibrary.R;
import at.fhooe.mcm.bbpt.sensorlibrary.inertialsensing.view.PositionView;
import at.fhooe.mcm.bbpt.sensorlibrary.service.LocalBluetoothService;

import com.google.gson.Gson;

public class InertialSensingActivity extends Activity implements InertialSensorListener {

	protected static final String TAG = "InertialSensingActivity";
	
	public static final String EXTRA_USE_DUMMY = "extra_use_dummy";
	public static final String EXTRA_START_CAMERA_RECORDING = "extra_start_camera_recording";
	public static final String EXTRA_USE_EXTENDED_ORIENTATION_SENSOR = "extra_use_extended_orientation_sensor";

	private PositionView posView;
	
	private InertialSensingModule inertialSensingModule;
	
	private boolean useDummyDistanceSensor;
	private boolean useAccurateOrientationSensor = true;
	private boolean startCameraRecording = false;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		useDummyDistanceSensor = getIntent().getExtras().getBoolean(EXTRA_USE_DUMMY);
		startCameraRecording = getIntent().getExtras().getBoolean(EXTRA_START_CAMERA_RECORDING);
		useAccurateOrientationSensor = getIntent().getExtras().getBoolean(EXTRA_USE_EXTENDED_ORIENTATION_SENSOR, true);
		inertialSensingModule = new InertialSensingModule(this, useDummyDistanceSensor, useAccurateOrientationSensor, startCameraRecording);
		inertialSensingModule.registerInertialSensorListener(this);
		posView = new PositionView(this, null);
		setContentView(posView, new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
	}
	
	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		//start Camera recording and inertial sensing
		new Thread(new Runnable() {
			
			@Override
			public void run() {
				inertialSensingModule.startSensing();
				
			}
		}).start();
		
		
	}

	/** Messenger for communicating with the service. */
	Messenger mService = null;

	/** Flag indicating whether we have called bind on the service. */
	boolean mBound;

	/**
	 * Class for interacting with the main interface of the service.
	 */
	private ServiceConnection mConnection = new ServiceConnection() {
		public void onServiceConnected(ComponentName className, IBinder service) {
			// This is called when the connection with the service has been
			// established, giving us the object we can use to
			// interact with the service. We are communicating with the
			// service using a Messenger, so here we get a client-side
			// representation of that from the raw IBinder object.
			mService = new Messenger(service);
			mBound = true;
			Log.e(TAG, "onServiceConnected - Services is bound!");
			Message msg = Message.obtain(null,
					LocalBluetoothService.MSG_START_BT, 0, 0);
			try {
				mService.send(msg);
			} catch (RemoteException e) {
				e.printStackTrace();
			}
		}

		public void onServiceDisconnected(ComponentName className) {
			// This is called when the connection with the service has been
			// unexpectedly disconnected -- that is, its process crashed.
			mService = null;
			mBound = false;
			Log.e(TAG, "Services isnt bound anymore!");
		}
	};

	private Gson gson = new Gson();

	public void sendPositionOverBT(Position p) {
		if (!mBound) {
			Log.e(TAG, "Services isnt bound!");
			return;
		}
		// Create and send a message to the service, using a supported 'what'
		// value
		String jsonString = gson.toJson(p);
		Message msg = Message.obtain(null,
				LocalBluetoothService.MSG_WRITE_DATA, 0, 0);
		Bundle b = new Bundle();
		b.putString(LocalBluetoothService.KEY_WRITE_DATA, jsonString);
		msg.setData(b);
		try {
			mService.send(msg);
		} catch (RemoteException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onStart() {
		super.onStart();
		// Bind to the service
		bindService(new Intent(this, LocalBluetoothService.class), mConnection,
				Context.BIND_AUTO_CREATE);
		Log.e(TAG, "Services is now bound!");
	}

	@Override
	protected void onDestroy() {
		
		// Unbind from the service
		if (mBound) {
			unbindService(mConnection);
			mBound = false;
		}
		super.onDestroy();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.inertial_sensing, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.sendData) {
			new Thread(new Runnable() {
				
				@Override
				public void run() {
					if(startCameraRecording) {
						SJCamUtil.stopRecording();						
					}
					inertialSensingModule.stopSensing(InertialSensingActivity.this);
					
				}
			}).start();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onNewPositionVectorReceived(PositionVector posVector) {
		posView.addCurrentPositionVector(posVector);
		sendPositionOverBT(posVector.endPosition);
	}
}
