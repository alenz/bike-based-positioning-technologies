package at.fhooe.mcm.bbpt.sensorlibrary;

import java.util.LinkedList;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import at.fhooe.mcm.bbpt.sensorlibrary.util.Util;

public class SensorManager implements SensorEventListener{

	public static final String TAG = "SensorManager:";
	
	private static SensorManager instance;
	
//	private static final int[] DEFAULT_SENSOR_CONFIGURATION = new int[]{Sensor.TYPE_BLE_BIKE_SENSOR};
	
	private Context context;
	private LinkedList<SensorEventListener>[][] sensorEventListeners;
	private Sensor[] sensors;
	/**
	 * contains the numbers of listeners of every single Sensor. If a module is
	 * interested in 2 or more SensorDatas of the same Sensor than this leads to
	 * an numberOfRegisteredListener for this sensor of 2. When the number is 0 then
	 * the sensor gets disconnected after the last deRegistration of an Listener because
	 * no one is interested anymore in this Sensor
	 */
	private int[] numbersOfRegisteredListeners;
	
	private SensorManager(Context context) {
		this.context = context;
		
		initSensorEventListenerArray();
		initSensors();		
	}
	
	@SuppressWarnings("unchecked")
	private void initSensorEventListenerArray() {		
		sensorEventListeners = (LinkedList<SensorEventListener>[][]) new LinkedList<?>[Sensor.TOTAL_NUMBER_OF_SENSORS][SensorData.TOTAL_NUMBER_OF_SENSOR_DATA];
		
		for(int j=0; j<Sensor.TOTAL_NUMBER_OF_SENSORS; j++) {
			for(int i=0; i<SensorData.TOTAL_NUMBER_OF_SENSOR_DATA; i++) {
				sensorEventListeners[j][i] = new LinkedList<SensorEventListener>();
			}			
		}
	}
	
	private void initSensors() {
		sensors = new Sensor[Sensor.TOTAL_NUMBER_OF_SENSORS];
		SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
				
		// create BikeSensor with the address stored in the SharedPreferences
		String deviceName = sharedPreferences.getString(context.getString(R.string.pref_key_ble_scs_device_name), "");
		String deviceAddress = sharedPreferences.getString(context.getString(R.string.pref_key_ble_scs_device_address), "");
		Util.log(TAG, "BT-DeviceName from pref: " + deviceName + " : " + deviceAddress);
		sensors[Sensor.TYPE_BLE_BIKE_SENSOR] = new BikeSensor(this, context, deviceName, deviceAddress, 0.26375f);  //34.33f
		sensors[Sensor.TYPE_ANDROID_ORIENTATION_SENSOR] = new OrientationSensor(this, context);
		sensors[Sensor.TYPE_DUMMY_DISTANCE_SENSOR] = new DummyDistanceSensor(this);
		sensors[Sensor.TYPE_ACCURATE_ANDROID_ORIENTATION_SENSOR] = new AccurateOrientationSensor(this, context);
		sensors[Sensor.TYPE_BAROMETRIC_ALTITUDE_SENSOR] = new AltitudeSensor(this, context);
		// TODO: create and add all different other sensors here.
		
		numbersOfRegisteredListeners = new int[Sensor.TOTAL_NUMBER_OF_SENSORS];
		for(int i=0; i<numbersOfRegisteredListeners.length; i++) {
			numbersOfRegisteredListeners[i] = 0;
		}
	}
	
	public static SensorManager getInstance(Context context) {
		if(instance == null) {
			instance = new SensorManager(context);
		}
		return instance;
	}
	
	public void registerSensorEventListener(SensorEventListener listener, int sensorType, int sensorDataType) {
		sensorEventListeners[sensorType][sensorDataType].remove(listener); 	// remove first an already added Listener (if the calling component is already a listener)
		sensorEventListeners[sensorType][sensorDataType].add(listener);
		Sensor sensor;
//		for(int i=0; i<sensors.length; i++) {
//			sensor = sensors[i];
//			if(sensor != null) {
//				if( sensor.getAvailableSensorDataTypes().contains(sensorDataType)) {
//					numbersOfRegisteredListeners[i]++;	//increase the count of the registered Listeners to this Sensor
//					if(sensor.getConnectionState() == Sensor.STATE_DISCONNECTED) { // connect to the sensor if it's disconnected so far
//						sensor.connect();
//					}
//				}				
//			}
//		}
		
		sensor = sensors[sensorType];
		if(sensor != null) {
			if( sensor.getAvailableSensorDataTypes().contains(sensorDataType)) {
				numbersOfRegisteredListeners[sensorType]++;	//increase the count of the registered Listeners to this Sensor
				if(sensor.getConnectionState() == Sensor.STATE_DISCONNECTED) { // connect to the sensor if it's disconnected so far
					sensor.connect();
				}
			}				
		}
	}
	
	public void closeAllSensors() {
		for(Sensor sensor: sensors) {
			if(sensor != null) {
				sensor.disconnect();				
			}
		}
	}
	
	public void unregisterSensorEventListener(SensorEventListener listener, int sensorType, int sensorDataType) {
		sensorEventListeners[sensorType][sensorDataType].remove(listener);
		Sensor sensor;
//		for(int i=0; i<sensors.length; i++) {
//			sensor = sensors[i];
//			if(sensor != null) {
//				if( sensor.getAvailableSensorDataTypes().contains(sensorDataType)) {
//					numbersOfRegisteredListeners[i]--;	//decrease the count of the registered Listeners to this Sensor
//					if(numbersOfRegisteredListeners[i] < 1) {
//						sensor.disconnect();					
//					}
//				}
//			}
//		}
		sensor = sensors[sensorType];
		if (sensor != null) {
			if (sensor.getAvailableSensorDataTypes().contains(sensorDataType)) {
				numbersOfRegisteredListeners[sensorType]--; // decrease the count of the
													// registered Listeners to
													// this Sensor
				if (numbersOfRegisteredListeners[sensorType] < 1) {
					sensor.disconnect();
				}
			}
		}
	}

	@Override
	public void onNewSensorDataReceived(SensorEvent event) {
		for (SensorEventListener listener : sensorEventListeners[event.sensorType][event.sensorDataType]) {
			listener.onNewSensorDataReceived(event);			
		}	
	}
	
	

	@Override
	public void onSensorAccuracyChanged(int sensorType, int sensorDataType, int accuracy) {
		for (SensorEventListener listener : sensorEventListeners[sensorType][sensorDataType]) {
			listener.onSensorAccuracyChanged(sensorType, sensorDataType, accuracy);			
		}
	}
	

	public void onSensorStatusChanged(int sensorType, Sensor sensor, int sensorStatus) {
		switch(sensorStatus) {
		case Sensor.STATE_DISCONNECTED:		
			if(numbersOfRegisteredListeners[sensorType] > 0) {
				Util.log(TAG, "Try reconnect of sensor " + sensorType + " after STATE_DISCONNECTED.");
				sensor.disconnect();
				sensor.connect();
			}
			break;			
		}
	}
	
}
