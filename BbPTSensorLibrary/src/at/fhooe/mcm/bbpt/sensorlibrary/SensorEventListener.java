package at.fhooe.mcm.bbpt.sensorlibrary;

public interface SensorEventListener {

	public void onNewSensorDataReceived(SensorEvent event);
	
	public void onSensorAccuracyChanged(int sensorType, int sensorDataType, int accuracy);
	
}
