package at.fhooe.mcm.bbpt.sensorlibrary;

public class SensorData {

	public static final int TOTAL_NUMBER_OF_SENSOR_DATA = 7;	// !!! important !!!! increase this number for every new SensorData
	
	public static final int TYPE_SPEED = 0;
	public static final int TYPE_DISTANCE = 1;
	public static final int TYPE_CADENCE = 2;
	public static final int TYPE_HEARTRATE = 3;
	public static final int TYPE_ANGLE = 4;
	public static final int TYPE_ORIENTATION = 5;
	public static final int TYPE_ALTITUDE = 6;
	
}
