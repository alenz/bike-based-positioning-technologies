package at.fhooe.mcm.bbpt.sensorlibrary.service;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.UUID;

import com.google.gson.Gson;

import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.util.Log;
import android.widget.Toast;
import at.fhooe.mcm.bbpt.sensorlibrary.inertialsensing.Position;

/**
 * A LocalService for acting as  BT-Server. Clients can connect to this BT-Server to receive Position-Updates.
 * Activities (and other Components) can bind to this service to send out Positions to connected Clients.
 * 
 * @author Alexander
 *
 */
public class LocalBluetoothService extends Service {
	/** Command to the service to display a message */
    public static final int MSG_WRITE_DATA = 1;
    public static final int MSG_START_BT = 2;
    
    public static final String KEY_WRITE_DATA = "key_write_data";
    
    public boolean running = false;

    /**
     * Handler of incoming messages from clients.
     */
    class IncomingHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
        	
            switch (msg.what) {
                case MSG_WRITE_DATA:
                    Toast.makeText(getApplicationContext(), "write data", Toast.LENGTH_SHORT).show();
                    String dataToWrite = msg.getData().getString(KEY_WRITE_DATA);
                    //write to bt connection socket
                    if(connected && (dataToWrite != null)) {
    					mBluetoothConnection.write(dataToWrite.getBytes());
    					addNewMessage(mBluetoothAdapter.getName(), "writed Position: " + dataToWrite);
    				}
                    
                    break;
                case MSG_START_BT:
                	mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
                	if (mBluetoothAdapter == null) {
                		
            			Toast.makeText(getApplicationContext(), "Bluetooth is not available", Toast.LENGTH_LONG)
            				.show();
            		} else {
            			int btState = mBluetoothAdapter.getState();
            			
            			if (btState == BluetoothAdapter.STATE_OFF) {
            				Log.i(TAG, "Bluetooth is off");
            			} else if (btState == BluetoothAdapter.STATE_ON) {
            				initializeBluetooth();
            			}
            		}	
                default:
                    super.handleMessage(msg);
            }
        }
    }
    
//    @Override
//    public int onStartCommand(Intent intent, int flags, int startId) {
//    	
//    	
//    	return START_STICKY;
//    }

    /**
     * Target we publish for clients to send messages to IncomingHandler.
     */
    final Messenger mMessenger = new Messenger(new IncomingHandler());

    /**
     * When binding to the service, we return an interface to our messenger
     * for sending messages to the service.
     */
    @Override
    public IBinder onBind(Intent intent) {
    	Log.e(TAG, "binding - onBind() called");
        Toast.makeText(getApplicationContext(), "binding", Toast.LENGTH_SHORT).show();
        return mMessenger.getBinder();
    }
    
    public static final UUID MY_UUID = UUID
			.fromString("f895eaf0-867f-11e3-baa7-0800200c9a66");
	
	private static final int SOCKET_CONNECTED = 1;
	private static final int DATA_RECEIVED = 2;
	public static final int WRITE_POS_DATA = 3;

	protected static final String TAG = "BTConnection";
	
	private BluetoothAdapter mBluetoothAdapter = null;
	
	private ConnectionThread mBluetoothConnection;
	
	private boolean connected = false;
	
	private Handler btHandler = new Handler(new Handler.Callback() {
		
		private Gson gson = new Gson();
		
		@Override
		public boolean handleMessage(Message msg) {
			switch (msg.what) {
			case SOCKET_CONNECTED:
				mBluetoothConnection = (ConnectionThread) msg.obj;
				BluetoothDevice device = mBluetoothConnection.mmBluetoothSocket
						.getRemoteDevice();
				Log.i(TAG, "Connected to " + device.getName() +
						" (" + device.getAddress() + ")");
				
				connected = true;
				break;
			case DATA_RECEIVED:
				String data = (String) msg.obj;
				addNewMessage(mBluetoothConnection.mmBluetoothSocket
						.getRemoteDevice().getName()
						, data);
				break;
			case WRITE_POS_DATA:
				Position pos = (Position) msg.obj;
				if(connected && (pos != null)) {
					String jsonString = gson.toJson(pos);	
					mBluetoothConnection.write(jsonString.getBytes());
					addNewMessage(mBluetoothAdapter.getName(), "writed Position: " + jsonString);
				}
				break;
			}
			return true;
		}
	});
	
	public class ConnectionThread extends Thread {
		BluetoothSocket mmBluetoothSocket;
		private final Handler mmHandler;
		private InputStream mmInStream;
		private OutputStream mmOutStream;
		
		public ConnectionThread(BluetoothSocket socket, Handler handler) {
			super();
			mmBluetoothSocket = socket;
			mmHandler = handler;
			try {
				mmInStream = mmBluetoothSocket.getInputStream();
				mmOutStream = mmBluetoothSocket.getOutputStream();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		@Override
		public void run() {
			byte[] buffer = new byte[1024];
			int len;
			while (true) {
				try {
					len = mmInStream.read(buffer);
					String data = new String(buffer, 0, len);
					mmHandler.obtainMessage(DATA_RECEIVED, data)
						.sendToTarget();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		
		public void write(byte[] bytes) {
			try {
				mmOutStream.write(bytes);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	class AcceptThread extends Thread {
		private final Handler mmHandler;
		private BluetoothServerSocket mmServerSocket;
		private BluetoothSocket mmSocket = null;
		
		public AcceptThread(Handler handler) {
			mmHandler = handler;
			try {
				mmServerSocket = mBluetoothAdapter
						.listenUsingRfcommWithServiceRecord
						("Bluetooth demo", MY_UUID);
			} catch (IOException e) {
				
			}
		}
		
		public void run() {
			while (true) {
				try {
					mmSocket = mmServerSocket.accept();
					ConnectionThread conn = new ConnectionThread
							(mmSocket, mmHandler);
					mmHandler.obtainMessage(SOCKET_CONNECTED, conn)
						.sendToTarget();
					conn.start();
					mmServerSocket.close();
					
					break;
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	private void addNewMessage(String name, String message) {
		Log.e(TAG, name + ": " + message + "\n");
//		Intent intent = new Intent();
	}
	
	private void initializeBluetooth() {
		Log.e(TAG, "Bluetooth is on");
		Log.e(TAG, "My device name: " +
					mBluetoothAdapter.getName() + " (" +
					mBluetoothAdapter.getAddress() + ")");
		
		if(!running) {
			new AcceptThread(btHandler).start();
			Log.e(TAG, "Started AcceptThread to receive incomming connections from clients.");
			running = true;
		} else {
			Log.e(TAG, "AcceptThread was already started -> so no new AcceptThread!!");
		}
		
	}
}
