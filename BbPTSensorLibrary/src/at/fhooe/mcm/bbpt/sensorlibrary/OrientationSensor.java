package at.fhooe.mcm.bbpt.sensorlibrary;

import java.util.Arrays;
import java.util.HashSet;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import at.fhooe.mcm.bbpt.sensorlibrary.util.Util;

public class OrientationSensor extends at.fhooe.mcm.bbpt.sensorlibrary.Sensor implements SensorEventListener {

	public static final String TAG = "OrientationSensor::";
	
	private static final HashSet<Integer> availableSensorDataTypes = new HashSet<Integer>(Arrays.asList(new Integer[]{SensorData.TYPE_ORIENTATION}));
	
	// android device sensor manager
	private SensorManager androidSensorManager;
	protected Context context;
	
	private int accuracy = at.fhooe.mcm.bbpt.sensorlibrary.Sensor.ACCURACY_GOOD;
	
	public OrientationSensor(at.fhooe.mcm.bbpt.sensorlibrary.SensorManager sensorManager, Context context) {
		super(sensorManager);
		this.context = context;
		
		// initialize your android device sensor capabilities
		androidSensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
	}

	@Override
	public boolean connect() {
		// for the system's orientation sensor registered listeners
		boolean connected;
		connected = androidSensorManager.registerListener(this, androidSensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION),
						SensorManager.SENSOR_DELAY_GAME);
		if(connected) {
			Util.log(TAG, "connected OrientationSensor!");
			connectionState = at.fhooe.mcm.bbpt.sensorlibrary.Sensor.STATE_CONNECTED;
		} else {
			connectionState = at.fhooe.mcm.bbpt.sensorlibrary.Sensor.STATE_DISCONNECTED;
		}
		return connected;
	}

	@Override
	public void disconnect() {
		androidSensorManager.unregisterListener(this);
		connectionState = at.fhooe.mcm.bbpt.sensorlibrary.Sensor.STATE_DISCONNECTED;
		Util.log(TAG, "disconnected OrientationSensor!");
	}

	@Override
	public boolean isConnected() {
		return (connectionState == STATE_CONNECTED);
	}

	@Override
	public HashSet<Integer> getAvailableSensorDataTypes() {
		return availableSensorDataTypes;
	}

	@Override
	public void onAccuracyChanged(Sensor sensor, int androidAccuracy) {
		switch(androidAccuracy) {
		case SensorManager.SENSOR_STATUS_ACCURACY_HIGH:
			accuracy = at.fhooe.mcm.bbpt.sensorlibrary.Sensor.ACCURACY_GOOD;
			break;
		case SensorManager.SENSOR_STATUS_ACCURACY_MEDIUM:
			accuracy = at.fhooe.mcm.bbpt.sensorlibrary.Sensor.ACCURACY_MEDIUM;
			break;
		default:
			accuracy = at.fhooe.mcm.bbpt.sensorlibrary.Sensor.ACCURACY_BAD;
			break;
		}
		sensorManager.onSensorAccuracyChanged(at.fhooe.mcm.bbpt.sensorlibrary.Sensor.TYPE_ANDROID_ORIENTATION_SENSOR, SensorData.TYPE_ORIENTATION, accuracy);
	}

	@Override
	public void onSensorChanged(SensorEvent event) {
		// get the angle around the z-axis rotated
		float degree = Math.round(event.values[0]);
		sensorManager.onNewSensorDataReceived(new at.fhooe.mcm.bbpt.sensorlibrary.SensorEvent(SensorData.TYPE_ORIENTATION, at.fhooe.mcm.bbpt.sensorlibrary.Sensor.TYPE_ANDROID_ORIENTATION_SENSOR, new float[]{degree}, System.currentTimeMillis(), accuracy));
	}

}
