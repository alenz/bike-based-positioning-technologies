package at.fhooe.mcm.bbpt.sensorlibrary;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;
import at.fhooe.mcm.bbpt.sensorlibrary.inertialsensing.InertialSensingActivity;
import at.fhooe.mcm.bbpt.sensorlibrary.util.Util;

public class MainActivity extends Activity implements SensorEventListener, OnClickListener{

	public static final String TAG = "sensorlibrary::MainActivity";
	
	private static final int REQUEST_ENABLE_BT = 1;
	
	private TextView deviceNameTxt, deviceAddressTxt, dataDistanceTxt, dataSpeedTxt, dataCadenceTxt, dataOrientationTxt, dataAltitudeTxt;
	private Button registerSensorsBtn, unRegisterSensorsBtn, startInertialSensingModuleBtn, startInertialSensingModuleWithDummDistanceSensorBtn, startInertialSensingModuleWithCameraRecordingBtn;
	private CheckBox useExtendedOrientationSensorCheckBox;
	
	private BluetoothAdapter bluetoothAdapter;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		deviceAddressTxt = (TextView) findViewById(R.id.sensor_main_txt_deviceAddress);
		deviceNameTxt = (TextView) findViewById(R.id.sensor_main_txt_deviceName);
		dataDistanceTxt = (TextView) findViewById(R.id.sensor_main_txt_distance);
		dataSpeedTxt = (TextView) findViewById(R.id.sensor_main_txt_speed);
		dataCadenceTxt = (TextView) findViewById(R.id.sensor_main_txt_cadence);
		dataOrientationTxt = (TextView) findViewById(R.id.sensor_main_txt_Orientation);
		dataAltitudeTxt = (TextView) findViewById(R.id.sensor_main_txt_Altitude);
		
		registerSensorsBtn = (Button) findViewById(R.id.sensor_main_btn_registerSensors);
		registerSensorsBtn.setOnClickListener(this);
		unRegisterSensorsBtn = (Button) findViewById(R.id.sensor_main_btn_unregisterSensors);
		unRegisterSensorsBtn.setOnClickListener(this);
		startInertialSensingModuleBtn = (Button) findViewById(R.id.sensor_main_btn_startInertialSensing);
		startInertialSensingModuleBtn.setOnClickListener(this);
		startInertialSensingModuleWithDummDistanceSensorBtn = (Button) findViewById(R.id.sensor_main_btn_startInertialSensingWithDummyDistanceSensor);
		startInertialSensingModuleWithDummDistanceSensorBtn.setOnClickListener(this);
		startInertialSensingModuleWithCameraRecordingBtn = (Button) findViewById(R.id.sensor_main_btn_startInertialSensingWithCameraRecording);
		startInertialSensingModuleWithCameraRecordingBtn.setOnClickListener(this);
		
		useExtendedOrientationSensorCheckBox = (CheckBox) findViewById(R.id.sensor_main_chbx_use_extendedOrientationSensor);
		
		// Use this check to determine whether BLE is supported on the device.  Then you can
        // selectively disable BLE-related features.
        if (!getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
            Toast.makeText(this, R.string.ble_not_supported, Toast.LENGTH_SHORT).show();
            finish();
        }

        // Initializes a Bluetooth adapter.  For API level 18 and above, get a reference to
        // BluetoothAdapter through BluetoothManager.
        final BluetoothManager bluetoothManager =
                (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        bluetoothAdapter = bluetoothManager.getAdapter();

        // Checks if Bluetooth is supported on the device.
        if (bluetoothAdapter == null) {
            Toast.makeText(this, R.string.error_bluetooth_not_supported, Toast.LENGTH_SHORT).show();
            finish();
            return;
        }
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main_menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			startActivity(new Intent(getApplicationContext(), DeviceScanActivity.class));
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
		
	@Override
	protected void onResume() {
		super.onResume();
		
		// Ensures Bluetooth is enabled on the device.  If Bluetooth is not currently enabled,
        // fire an intent to display a dialog asking the user to grant permission to enable it.
        if (!bluetoothAdapter.isEnabled()) {
            if (!bluetoothAdapter.isEnabled()) {
                Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
            }
        }
	}
	
	private void registerSensors() {
		SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
		String deviceName = sharedPreferences.getString(getString(R.string.pref_key_ble_scs_device_name), null);
		String deviceAddress = sharedPreferences.getString(getString(R.string.pref_key_ble_scs_device_address), null);
		Util.log(TAG, "BT-DeviceName from pref: " + deviceName + " : " + deviceAddress);
		
		SensorManager sensorManager = SensorManager.getInstance(getApplicationContext());
		if(useExtendedOrientationSensorCheckBox.isChecked()) {
			sensorManager.registerSensorEventListener(this, Sensor.TYPE_ACCURATE_ANDROID_ORIENTATION_SENSOR, SensorData.TYPE_ORIENTATION);			
		} else {
			sensorManager.registerSensorEventListener(this, Sensor.TYPE_ANDROID_ORIENTATION_SENSOR, SensorData.TYPE_ORIENTATION);	
		}
		sensorManager.registerSensorEventListener(this, Sensor.TYPE_BAROMETRIC_ALTITUDE_SENSOR, SensorData.TYPE_ALTITUDE);
		
		if(deviceName != null && deviceAddress != null) {
			deviceAddressTxt.setText("BT-MAC-Address: " + deviceAddress);
			deviceNameTxt.setText("Device-Name: " + deviceName);
			
			// start BLE-BikeSensor for testing			
			sensorManager.registerSensorEventListener(this, Sensor.TYPE_BLE_BIKE_SENSOR, SensorData.TYPE_DISTANCE);
			sensorManager.registerSensorEventListener(this, Sensor.TYPE_BLE_BIKE_SENSOR, SensorData.TYPE_CADENCE);
			sensorManager.registerSensorEventListener(this, Sensor.TYPE_BLE_BIKE_SENSOR, SensorData.TYPE_SPEED);
			
			
			
		} else {
			Util.log(TAG, "Can'load BT-Adress from preferences : " + deviceAddress);
		}
		
	}
	
	private void unRegisterSensors() {
		SensorManager sensorManager = SensorManager.getInstance(getApplicationContext());
		sensorManager.unregisterSensorEventListener(this, Sensor.TYPE_BLE_BIKE_SENSOR, SensorData.TYPE_DISTANCE);
		sensorManager.unregisterSensorEventListener(this, Sensor.TYPE_BLE_BIKE_SENSOR, SensorData.TYPE_CADENCE);
		sensorManager.unregisterSensorEventListener(this, Sensor.TYPE_BLE_BIKE_SENSOR, SensorData.TYPE_SPEED);
		sensorManager.unregisterSensorEventListener(this, Sensor.TYPE_ACCURATE_ANDROID_ORIENTATION_SENSOR, SensorData.TYPE_ORIENTATION);
		sensorManager.unregisterSensorEventListener(this, Sensor.TYPE_ANDROID_ORIENTATION_SENSOR, SensorData.TYPE_ORIENTATION);
		sensorManager.unregisterSensorEventListener(this, Sensor.TYPE_BAROMETRIC_ALTITUDE_SENSOR, SensorData.TYPE_ALTITUDE);
		
	}
	
	
//	@Override
//	protected void onStop() {
//		
//		SensorManager sensorManager = SensorManager.getInstance(getApplicationContext());
//		sensorManager.unregisterSensorEventListener(this, SensorData.TYPE_DISTANCE);
//		sensorManager.unregisterSensorEventListener(this, SensorData.TYPE_CADENCE);
//		sensorManager.unregisterSensorEventListener(this, SensorData.TYPE_SPEED);
//		sensorManager.unregisterSensorEventListener(this, SensorData.TYPE_ORIENTATION);
//	}

	@Override
	public void onNewSensorDataReceived(final SensorEvent event) {
//		Log.e(TAG, "Received new Data :) - " + event.values[0]);
		runOnUiThread(new Runnable() {
			
			@Override
			public void run() {
				switch(event.sensorDataType) {
				case SensorData.TYPE_DISTANCE:
					dataDistanceTxt.setText("Distance[cm]: " + event.values[0] + "cm (" + event.values[1] + ")");				
					break;
				case SensorData.TYPE_SPEED:
					dataSpeedTxt.setText("Speed[cm/s]: " + event.values[0]);
					break;
				case SensorData.TYPE_CADENCE:
					dataCadenceTxt.setText("Cadence[U/min]: " + event.values[0] + "U/min (" + event.values[1] + ")");
					break;
				case SensorData.TYPE_ORIENTATION:
					dataOrientationTxt.setText("Orientation[�]: " + event.values[0] + "�" + "(sensor: " +  event.sensorType + ")");
					break;
				case SensorData.TYPE_ALTITUDE:
					dataAltitudeTxt.setText("RelAlt: " + event.values[2] + "m Altitude[m]: " + event.values[0] + " p: " + event.values[1]);
				}
			}
		});
	}

	@Override
	public void onSensorAccuracyChanged(int sensorType, int sensorDataType, int accuracy) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onClick(View v) {
		boolean useExtendedOrientationSensor = useExtendedOrientationSensorCheckBox.isChecked();
		
		if(v.equals(registerSensorsBtn)) {
			registerSensors();
			registerSensorsBtn.setVisibility(View.GONE);
			unRegisterSensorsBtn.setVisibility(View.VISIBLE);
		} else if(v.equals(unRegisterSensorsBtn)) {
			unRegisterSensors();
			unRegisterSensorsBtn.setVisibility(View.GONE);
			registerSensorsBtn.setVisibility(View.VISIBLE);			
		} else if(v.equals(startInertialSensingModuleBtn)) {
			if(unRegisterSensorsBtn.getVisibility() == View.VISIBLE) {
				Bundle bundle = new Bundle();
				bundle.putBoolean(InertialSensingActivity.EXTRA_USE_DUMMY, false);
				bundle.putBoolean(InertialSensingActivity.EXTRA_START_CAMERA_RECORDING, false);
				bundle.putBoolean(InertialSensingActivity.EXTRA_USE_EXTENDED_ORIENTATION_SENSOR, useExtendedOrientationSensor);
				Intent intent = new Intent(this, InertialSensingActivity.class);
				intent.putExtras(bundle);
				startActivity(intent);				
			} else {
				Toast.makeText(this, "InetialSensingModule can only be started when the sensors are connected.", Toast.LENGTH_SHORT).show();
			}
		} else if(v.equals(startInertialSensingModuleWithDummDistanceSensorBtn)) {
			Bundle bundle = new Bundle();
			bundle.putBoolean(InertialSensingActivity.EXTRA_USE_DUMMY, true);
			bundle.putBoolean(InertialSensingActivity.EXTRA_START_CAMERA_RECORDING, false);
			bundle.putBoolean(InertialSensingActivity.EXTRA_USE_EXTENDED_ORIENTATION_SENSOR, useExtendedOrientationSensor);
			Intent intent = new Intent(this, InertialSensingActivity.class);
			intent.putExtras(bundle);
			startActivity(intent);		
		} else if(v.equals(startInertialSensingModuleWithCameraRecordingBtn)) {
			Bundle bundle = new Bundle();
			bundle.putBoolean(InertialSensingActivity.EXTRA_USE_DUMMY, false);
			bundle.putBoolean(InertialSensingActivity.EXTRA_START_CAMERA_RECORDING, true);
			bundle.putBoolean(InertialSensingActivity.EXTRA_USE_EXTENDED_ORIENTATION_SENSOR, useExtendedOrientationSensor);
			Intent intent = new Intent(this, InertialSensingActivity.class);
			intent.putExtras(bundle);
			startActivity(intent);		
		}
	}
}
