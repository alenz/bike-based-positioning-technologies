Bike Based positioning technologies - bbpt
Marker based positioning technologies - mbpt

# README #
### What is this repository for? ###

This repo stores the results and source code of a Marker based Bike Positioning method using AprilTags Marker detection algorithm ported to android.
The focus of this work is to calculate the position of Mountainbikers (Downhillers) cm-accurate using only an action-camera like the famous GoPro and a android-smartphone + some inertial sensors (for better accuracy).
This work can be further used as a basis for training comparising tool of Mountainbikers (Downhillers).

## Presentation & current status: ##

A presentation which gives a good overview and also and current status report can be found here: https://sway.com/GAG4lvduBrCGn3RT


### Who do I talk to? ###

Alexander Lenz,
Master student of the FH-Hagenberg -'Mobile Computing'-department
alexanderlenz85@gmail.com