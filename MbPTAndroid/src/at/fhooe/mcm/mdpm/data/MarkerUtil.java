package at.fhooe.mcm.mdpm.data;

import com.example.apriltagandroid.R;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.SparseArray;

/**
 * Static class to setup marker configurations for a specific track:
 * importang to know: only the angleY of a marker will be used in pos-calculation for now  !!!!! (0, 90, 180, 270 � can be used for now)
 * @author Alexander
 *
 */
public class MarkerUtil {

	
	/**
	 * setup makers for basic tests (all have position 0,0,0 and no angle)
	 * @return
	 */
	public static SparseArray<Marker> getMarkerHashMapUnplaced() {
		SparseArray<Marker> sparse = new SparseArray<Marker>();
		sparse.append(2, new Marker(2, new Position(0, 0, 0, 0), 0.2015f, 0, 0, 0));
		sparse.append(21, new Marker(21, new Position(0, 0, 0, 0), 0.193f, 0, 0, 0));
		sparse.append(0, new Marker(0,  new Position(0, 0, 0, 0), 0.2855f, 0, 0, 0));		//big 0er
		
//		sparse.append(0, new Marker(0,  null, 0.144f, 0, 0, 0));		//small 0er
		return sparse;
	}	
	

	/**
	 * setup makers dual marker test (2 markers) to test enhancement of pos accuracy when detecting multiple markers at the same time
	 * @return
	 */
	public static SparseArray<Marker> getMarkerHashMapDualMarkerTest2() {
		SparseArray<Marker> sparse = new SparseArray<Marker>();
		sparse.append(3, new Marker(3, new Position(-0.79, 0, 0, 0), 0.386f, 0, 0, 0));
		sparse.append(6, new Marker(6, new Position(0.80, 0, 0, 0), 0.3855f, 0, 0, 0));
		
		return sparse;
	}	
	
	/**
	 * setup makers for basic tests (all have position 0,0,0 and no angle)
	 * @return
	 */
	public static SparseArray<Marker> getMarkerHashMapDualMarkerTest1() {
		SparseArray<Marker> sparse = new SparseArray<Marker>();
		sparse.append(2, new Marker(2, new Position(-0.52, 0, 0, 0), 0.2015f, 0, 0, 0));
		sparse.append(21, new Marker(21, new Position(0.52, 0, 0, 0), 0.193f, 0, 0, 0));
		
//		sparse.append(0, new Marker(0,  null, 0.144f, 0, 0, 0));		//small 0er
		return sparse;
	}	

	/**
	 * Returns the MarkerSetup of one specific test, depending on the given id.
	 * 
	 * @param id
	 * @return
	 */
	private static SparseArray<Marker> getMarkerSetup(int id) {
		switch (id) {
		case 0:
			return getMarkerHashMapUnplaced();
		case 1:
			return getMarkerHashMapCornerTest();
		case 2:
			return getMarkerHashMapQuadratTestNussbaum1();
		case 3:
			return getMarkerHashMapSnakeTest();
		case 4:
			return getMarkerHashMap_U_Test();
		case 5:
			return getMarkerHashMap_Big_S_Test();
		case 6:
			return getMarkerHashMapCombinedTest1Straight();
		case 7:
			return getMarkerHashMapDualMarkerTest1();
		case 8:
			return getMarkerHashMapDualMarkerTest2();
		case 9:
			return getMarkerHashMap_CombTest_1();
		case 10:
			return getMarkerHashMap_Big_S_Test_Plus();
		default:
			return getMarkerHashMapUnplaced();
		}
	}
	
	public static SparseArray<Marker> getUserPreferredMarkerSetup(Context context) {
		SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
		int selectedTrackSetup = Integer.valueOf(sharedPreferences.getString(context.getString(R.string.pref_key_tracksetup), "0"));
		return getMarkerSetup(selectedTrackSetup);
	}
	
	/**
	 * Marker setup for extended Big S Test for final Tests
	 * @return
	 */
	private static SparseArray<Marker> getMarkerHashMap_Big_S_Test_Plus() {
		SparseArray<Marker> sparse = new SparseArray<Marker>();
		sparse.append(8, new Marker(8, new Position(0.0d, 0.0d, 17.25d, 0), 0.385f, 0, 90, 0)); 
		sparse.append(6, new Marker(6, new Position(0.0d, 0.0d, 10.90d, 0), 0.3855f, 0, 180, 0));		
		sparse.append(3, new Marker(3, new Position(8.83d, 0.0d, 10.90d, 0), 0.386f, 0, 270, 0));			
		sparse.append(30, new Marker(30, new Position(8.83d, 0.0d, 0.0d, 0), 0.385f, 0, 180, 0));
		sparse.append(21, new Marker(21, new Position(-0.07d, 0, 0.0d, 0), 0.4197f, 0, 90, 0));
		sparse.append(1, new Marker(1, new Position(-0.07d, 0.0d, -6.91d, 0), 0.349f, 0, 180, 0));
		
		return sparse;
	}
	
	/**
	 * Marker setup for Combined Test with Intertial Data: CombTest 1 (around Nussbaum)
	 * @return
	 */
	private static SparseArray<Marker> getMarkerHashMap_CombTest_1() {
		SparseArray<Marker> sparse = new SparseArray<Marker>();
		sparse.append(6, new Marker(6, new Position(0.0d, 0.0d, 0.0d, 0), 0.3855f, 0, 180, 0));
		sparse.append(3, new Marker(3, new Position(8.83d, 0.0d, 0.0d, 0), 0.386f, 0, 270, 0));
		
		sparse.append(8, new Marker(8, new Position(0.0d, 0.0d, 7.95d, 0), 0.385f, 0, 90, 0)); 
		
		return sparse;
	}
	
	/**
	 * Marker setup for big S test 
	 * @return
	 */
	private static SparseArray<Marker> getMarkerHashMap_Big_S_Test() {
		SparseArray<Marker> sparse = new SparseArray<Marker>();
		sparse.append(6, new Marker(6, new Position(1.75d, 0.0d, 0d, 0), 0.3855f, 0, 90, 0));
		sparse.append(1, new Marker(1, new Position(9.0d, 0.0d, 4.5d, 0), 0.349f, 0, 180, 0));
		sparse.append(0, new Marker(0, new Position(10.0d, 0.0d, 14.5d, 0), 0.2852f, 0, 270, 0));
		sparse.append(3, new Marker(3, new Position(2.0d, 0.0d, 15.50d, 0), 0.386f, 0, 180, 0));
		sparse.append(21, new Marker(21, new Position(0, 0, 23.5d, 0), 0.4197f, 0, 90, 0));
		
		return sparse;
	}

	
	/**
	 * Marker setup for U test around the 'gem�se-garten'
	 * @return
	 */
	private static SparseArray<Marker> getMarkerHashMap_U_Test() {
		SparseArray<Marker> sparse = new SparseArray<Marker>();
		sparse.append(1, new Marker(1, new Position(0.0d, 0.0d, 0.0, 0), 0.349f, 0, 90, 0));
		sparse.append(3, new Marker(3, new Position(7.25d, 0.0d, 4.50d, 0), 0.386f, 0, 180, 0));
		sparse.append(6, new Marker(6, new Position(7.25d, 0.0d, 14.50d, 0), 0.3855f, 0, 270, 0));
		
		return sparse;
	}

	
	/**
	 * Marker setup for test test 4 markers in a line to drive a snake line around them (lenght = 25m)
	 * @return
	 */
	private static SparseArray<Marker> getMarkerHashMapSnakeTest() {
		SparseArray<Marker> sparse = new SparseArray<Marker>();
		sparse.append(3, new Marker(3, new Position(0.0d, 0.0d, 5.0d, 0), 0.386f, 0, 0, 0));
		sparse.append(6, new Marker(6, new Position(0.0d, 0.0d, 10.0d, 0), 0.3855f, 0, 0, 0));
		sparse.append(1, new Marker(1, new Position(0.0d, 0.0d, 15.0, 0), 0.349f, 0, 0, 0));
		sparse.append(0, new Marker(0, new Position(0.0d, 0.0d, 20.0d,0), 0.285f, 0, 0, 0));
		return sparse;
	}
	
	/**
	 * Marker setup for quadrat test around the nussbaum 1 (5x5 quadrat)
	 * @return
	 */
	private static SparseArray<Marker> getMarkerHashMapQuadratTestNussbaum1() {
		SparseArray<Marker> sparse = new SparseArray<Marker>();
		sparse.append(6, new Marker(6, new Position(5.0, 0.0d, 0.0d, 0), 0.3855f, 0, 270, 0));
		sparse.append(3, new Marker(3, new Position(5.0d, 0.0d, 5.0d, 0), 0.386f, 0, 0, 0));
		sparse.append(1, new Marker(1, new Position(0.0d, 0.0d, 5.0, 0), 0.349f, 0, 90, 0));		//bei verdrehtem hier 180 zum schluss
		sparse.append(0, new Marker(0, new Position(0.0d, 0.0d, 0.0d, 0), 0.285f, 0, 180, 0));
		return sparse;
	}


	/**
	 * Marker setup for the simple corner test with 2 markers 
	 * @return
	 */
	private static SparseArray<Marker> getMarkerHashMapCornerTest() {
		SparseArray<Marker> sparse = new SparseArray<Marker>();
		sparse.append(21, new Marker(21, new Position(0d, 0.50d, 2.50d, 0), 0.193f, 0, 0, 0));
		sparse.append(2, new Marker(2, new Position(2.60d, 1.00d, 2.50d, 0), 0.2015f, 0, 270, 0));

		return sparse;
	}
	
	private static SparseArray<Marker> getMarkerHashMapCombinedTest1Straight() {
		SparseArray<Marker> sparse = new SparseArray<Marker>();
		sparse.append(0, new Marker(0, new Position(100d, 0d, 500d, 0), 28.5f, 0, 0, 0));
		return sparse;
	}
}
