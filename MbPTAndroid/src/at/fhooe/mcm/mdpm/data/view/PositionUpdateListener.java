package at.fhooe.mcm.mdpm.data.view;

import at.fhooe.mcm.mdpm.data.Position;

/**
 * Interface to notify the listener about the processing-status of an Video/Image.
 * @author Alexander
 *
 */
public interface PositionUpdateListener {

	/**
	 * Method will be called when a new Position was calculated out of a frame.
	 * @param p - the new Position of the camera
	 */
	public void onNewPositionCalculated(Position p);
	
	/**
	 * Method will be called when it wasnt possible to calculate a position out of the frame
	 * just to inform the listening component that a frame was processed but no position could be calculated out of it
	 */
	public void onBadFrameReceived();
	
	/**
	 * Method will be called when all frames where processed.
	 * @param numOfProcessedFrames - Number of the processed Frames.
	 */
	public void onCalculationFinished(int numOfProcessedFrames);
}
