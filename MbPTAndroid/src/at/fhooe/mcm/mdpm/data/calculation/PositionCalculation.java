package at.fhooe.mcm.mdpm.data.calculation;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import org.opencv.calib3d.Calib3d;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfDouble;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.MatOfPoint3f;
import org.opencv.core.Point;
import org.opencv.core.Point3;

import android.util.SparseArray;
import at.fhooe.mcm.mdpm.data.Position;
import Jama.Matrix;

/**
 * With a {@link PositionCalculation} it is possible to calculate the Camera-Position in respect to a found Marker
 * out of the 4-corner-Points or the Marker-Homography by calculating the extrinsics (R-Matrix and T-Vector) and inverting them.
 * 
 * The extrinsics are calculated with the 4-corner points and the Camera-Parameter (Camera-Matrix, Distortion-Coefficients) as well
 * as the size of the marker.
 * 
 * The outcome of this calculation is a {@link Position}.
 * 
 * @author Alexander
 *
 */
public class PositionCalculation {

	public Mat Rvec;
	public Mat Tvec;
	public MatOfPoint2f points;	//corner points of the marker in image
	private Position position;
	public Matrix rotMatrix;
	
//	private SparseArray<Marker> markerMap;
	
	
	private PositionCalculation() {
		Rvec = new Mat(3,1,CvType.CV_64FC1);
		Tvec = new Mat(3,1,CvType.CV_64FC1);
		points = new MatOfPoint2f();		
	}
	
	public PositionCalculation(Vector<Point> p) {
		this();
		points.fromList(p);
		System.out.println(points.dump());
	}
	
	public PositionCalculation(float[] homograhy) {
		this();
		Vector<Point> tagCornerPoints = new Vector<Point>();
		tagCornerPoints.add(projectHomography(homograhy, -1, -1));
		tagCornerPoints.add(projectHomography(homograhy, 1, -1));
		tagCornerPoints.add(projectHomography(homograhy, 1, 1));
		tagCornerPoints.add(projectHomography(homograhy, -1, 1));
		points.fromList(tagCornerPoints);
		System.out.println(points.dump());
	}
	
	public Point projectHomography(float[] H, double x, double y) {
		double xx = H[0]*x + H[1]*y + H[2];
		double yy = H[3]*x + H[4]*y + H[5];
		double zz = H[6]*x + H[7]*y + H[8];
		
		double ox = xx / zz;
		double oy = yy / zz;
		return new Point(ox, oy);
	}
	
	/**
	 * Calculate 3D position of the marker based on its translation and rotation matrix.
	 * This method fills in these matrix properly.
	 * @param camMatrix
	 * @param distCoeff
	 */
	public void calculateExtrinsics(Mat camMatrix, MatOfDouble distCoeffs, float sizeMeters){
		// TODO check params
		
		// set the obj 3D points
		double halfSize = sizeMeters/2.0;
		List<Point3> objPoints = new ArrayList<Point3>();
		objPoints.add(new Point3(-halfSize, -halfSize,0));
		objPoints.add(new Point3( halfSize, -halfSize,0));
		objPoints.add(new Point3( halfSize,  halfSize,0));
		objPoints.add(new Point3(-halfSize,  halfSize,0));
		
		
//		objPoints.add(new Point3(halfSize, -halfSize,0));
//		objPoints.add(new Point3(halfSize,  halfSize,0));
//		objPoints.add(new Point3(-halfSize,  halfSize,0));
//		objPoints.add(new Point3( -halfSize, -halfSize,0));

		MatOfPoint3f objPointsMat = new MatOfPoint3f();
		objPointsMat.fromList(objPoints);
		Calib3d.solvePnP(objPointsMat, points, camMatrix, distCoeffs, Rvec, Tvec);
//		Utils.rotateXAxis(Rvec);
	}
	
	public  Position calculateCameraPos() {	
		Mat rMatrix = new Mat(3,3,CvType.CV_64FC1);
		Calib3d.Rodrigues(Rvec, rMatrix);
		System.out.println("RMatrix: " + rMatrix.dump());
		
		rotMatrix = new Matrix(new double[][]{	{rMatrix.get(0, 0)[0], rMatrix.get(0, 1)[0], rMatrix.get(0, 2)[0]},
				{rMatrix.get(1, 0)[0], rMatrix.get(1, 1)[0], rMatrix.get(1, 2)[0]},
				{rMatrix.get(2, 0)[0], rMatrix.get(2, 1)[0], rMatrix.get(2, 2)[0]}});
		
		
		Matrix transpRotMatrix = rotMatrix.inverse();
		Mat tRMatrix = new Mat(3,3,CvType.CV_64FC1);
		tRMatrix = rMatrix.t();
		Mat nTVec = new Mat(3,1,CvType.CV_64FC1);
//		nTVec = tRMatrix * tvec;
		
		Matrix tMatrix = new Matrix(new double[][]{ {Tvec.get(0, 0)[0]},  {Tvec.get(1, 0)[0]},  {Tvec.get(2, 0)[0]} });
		
		Matrix invertedTMatrix = transpRotMatrix.times(tMatrix);
		double invertedX = -invertedTMatrix.get(0, 0);
		double invertedY = -invertedTMatrix.get(1, 0);
		double invertedZ = -invertedTMatrix.get(2, 0);
//		log("Invereted corrected TransMat: " + invertedX + " , " + invertedY + " , " + invertedZ);
		
		
		//calculate euler angles
		double angleX = Math.atan2(rotMatrix.get(2, 1), rotMatrix.get(2, 2));
		double angleY = Math.atan2(-rotMatrix.get(2, 0), Math.sqrt((rotMatrix.get(2, 1)*rotMatrix.get(2, 1))+(rotMatrix.get(2, 2)*rotMatrix.get(2, 2))));
		double angleZ = Math.atan2(rotMatrix.get(1, 0), rotMatrix.get(0, 0));
		
		double angleXdeg = angleX * 180 / Math.PI;
		double angleYdeg = angleY * 180 / Math.PI;
		double angleZdeg = angleZ * 180 / Math.PI;
		
		
		position = new Position(invertedX, invertedY, invertedZ, angleXdeg, angleYdeg, angleZdeg);
		
		return position;
	}
	
//	private void calculateEulerAngles() {
//		double angleX = Math.atan2(rotMatrix.get(2, 1), rotMatrix.get(2, 2));
//		double angleY = Math.atan2(-rotMatrix.get(2, 0), Math.sqrt((rotMatrix.get(2, 1)*rotMatrix.get(2, 1))+(rotMatrix.get(2, 2)*rotMatrix.get(2, 2))));
//		double angleZ = Math.atan2(rotMatrix.get(1, 0), rotMatrix.get(0, 0));
//		
//		double angleXdeg = angleX * 180 / Math.PI;
//		double angleYdeg = angleY * 180 / Math.PI;
//		double angleZdeg = angleZ * 180 / Math.PI;
//		System.out.println("angleX: " + angleX + " , angleY: " + angleY + " , angleZ: " + angleZ);
//		System.out.println("angleX: " + angleXdeg + " , angleY: " + angleYdeg + " , angleZ: " + angleZdeg);
//	}
//	

}
