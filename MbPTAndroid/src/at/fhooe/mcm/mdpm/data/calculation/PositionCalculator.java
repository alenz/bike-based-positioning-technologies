package at.fhooe.mcm.mdpm.data.calculation;

import java.io.File;
import java.io.IOException;
import java.util.Vector;

import org.apache.commons.io.FileUtils;
import org.opencv.core.Mat;

import com.google.gson.Gson;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.util.Log;
import android.util.SparseArray;
import android.widget.Toast;
import at.fhooe.mcm.mdpm.data.Marker;
import at.fhooe.mcm.mdpm.data.MarkerUtil;
import at.fhooe.mcm.mdpm.data.Position;
import at.fhooe.mcm.mdpm.data.view.ListItem;
import at.fhooe.mcm.mdpm.data.view.PositionUpdateListener;
import at.fhooe.mcm.mdpm.util.Util;

/**
 * The {@link PositionCalculator} takes as input the {@link CameraParameters} which should be used to calculate positions for the following images (frames), 
 * a path where the positions should be saved an instance of the {@link PositionUpdateListener} so that the
 * caller get notivied on different events.
 * 
 *  After the initialization you can call {@link PositionCalculator#addImage(String, Mat, long, int)} for every image/frame you want to find markers and try to
 *  calculate the position of the camera out of them. For every call on this method
 *   a new {@link VideoProcTask} will be started where most of the calculation is done than
 *   for making the VideoProcessing faster.
 * @author Alexander
 *
 */
public class PositionCalculator {

	public static final String TAG = "PositionCalculator";
	
	private Vector<Position> positions;
	private Vector<Position> multiMarkerPositions;
	public String pathToSaveCalculatedPositions;
	public String pathToSaveMultiMarkerPositions;
	protected CameraParameters camParam;
	private PositionUpdateListener updateListener = null;	// if it is set the listener gets notified when the position is calculated out of this image.
	public static SparseArray<Marker> markerMap = null;
	
	private int nrOfCurrentRunningVideoProcTasks = 0;
	private static final int MAX_NR_OF_PARALLEL_RUNNING_ASYNC_TASKS = 5;
	
	public PositionCalculator(String pathToSaveCalculatedPositions, String pathToSaveMultiMarkerPositions, CameraParameters campParams, PositionUpdateListener updateListener, SparseArray<Marker> trackSetup) {
		positions = new Vector<Position>();
		multiMarkerPositions = new Vector<Position>();
		this.pathToSaveCalculatedPositions = pathToSaveCalculatedPositions;
		this.pathToSaveMultiMarkerPositions = pathToSaveMultiMarkerPositions;
		this.camParam = campParams;
		this.updateListener = updateListener;
		this.markerMap = trackSetup;
		this.nrOfCurrentRunningVideoProcTasks = 0;
	}
	
	public void addImage(String path, Mat m, long time, int frameCount) {
		
		while(nrOfCurrentRunningVideoProcTasks >= MAX_NR_OF_PARALLEL_RUNNING_ASYNC_TASKS) {
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		
		nrOfCurrentRunningVideoProcTasks++;
		
		 // ImageLoader extends AsyncTask
        VideoProcTask imageLoader = new VideoProcTask(path, m, this, time, frameCount);

        // Execute in parallel
        imageLoader.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "http://url.com/image.png");
        Log.e(TAG, "Started VideoProcessTask. nrOfCurrentRunningVideoProcTasks = " + nrOfCurrentRunningVideoProcTasks);
	}
	
	public void addCalculatedPositions(Position[] positions, long time, int frameCount) {
		if(positions.length > 1) {
			for(Position p: positions) {
				this.multiMarkerPositions.add(p);
			}
		}
		Position avgPosition = avgPositions(positions);
		avgPosition.time = time;
		avgPosition.frameNr = frameCount;
		Util.log(TAG, "Frame " + frameCount + " (" + time + ") Averaged " + positions.length + " positions to this position: " + avgPosition.toString());
		
		nrOfCurrentRunningVideoProcTasks--;
		 Log.e(TAG, "Ended VideoProcessTask. nrOfCurrentRunningVideoProcTasks = " + nrOfCurrentRunningVideoProcTasks);
		this.positions.add(avgPosition);
		if(updateListener != null) updateListener.onNewPositionCalculated(avgPosition);
		
	} 
	
	public void onBadFrameReceived() {
		nrOfCurrentRunningVideoProcTasks--;
		Log.e(TAG, "Ended VideoProcessTask wit BadFrameReceived. nrOfCurrentRunningVideoProcTasks = " + nrOfCurrentRunningVideoProcTasks);
		if(updateListener != null) {
			updateListener.onBadFrameReceived();
		}
	}
	
	public void onCalculationFinished(int numOfProcessedFrames) {
		if(updateListener != null) {
			updateListener.onCalculationFinished(numOfProcessedFrames);
		}
	}
	
	public Position avgPositions(Position[] positions) {
		double sumX = 0, sumY = 0, sumZ = 0;
		double sumAx = 0, sumAy = 0, sumAz = 0;
		int numberOfVisibleMarkers = positions.length;
		for (Position p: positions) {
				if(p == null) continue;
					sumX = sumX + p.x;
					sumY = sumY + p.y;
					sumZ = sumZ + p.z;
					
					sumAx = sumAx + p.angleX;
					sumAy = sumAy + p.angleY;
					sumAz = sumAz + p.angleZ;
					
		}			
		if(numberOfVisibleMarkers < 1) {
			return null;
		}
		
		Position calculatedAvgPosition = new Position(sumX / numberOfVisibleMarkers, sumY / numberOfVisibleMarkers, sumZ / numberOfVisibleMarkers, 0);
		calculatedAvgPosition.angleX = (int) (sumAx / numberOfVisibleMarkers);
		calculatedAvgPosition.angleY = (int) (sumAy / numberOfVisibleMarkers);
		calculatedAvgPosition.angleZ = (int) (sumAz / numberOfVisibleMarkers);
		return calculatedAvgPosition;
	}
	
	// values: an array of numbers that will be modified in place 
	// smoothing: the strength of the smoothing filter; 1=no change, larger values smoothes more function 
//	smoothArray( values, smoothing ){ var value = values[0]; // start with the first input for (var i=1, len=values.length; i<len; ++i){ var currentValue = values[i]; value += (currentValue - value) / smoothing; values[i] = value; } }
	
	public void savePositions() {
		Log.e(TAG, "Saved calculated Positions as JSON-Array to sdcard. " + pathToSaveCalculatedPositions);
		Gson gson = new Gson();
		Position[] array = new Position[positions.size()];
		positions.toArray(array);
		String str = gson.toJson(array, Position[].class);
		try {
			FileUtils.writeStringToFile(new File(pathToSaveCalculatedPositions), str);
		} catch (IOException e) {
			Log.e(TAG, "Error while saving calculated positions: " + e.toString());
		}
		
		Position[] array1 = new Position[multiMarkerPositions.size()];
		multiMarkerPositions.toArray(array1);
		String str1 = gson.toJson(array1, Position[].class);
		try {
			FileUtils.writeStringToFile(new File(pathToSaveMultiMarkerPositions), str1);
		} catch (IOException e) {
			Log.e(TAG, "Error while saving multimarker positions: " + e.toString());
		}
	}
}
