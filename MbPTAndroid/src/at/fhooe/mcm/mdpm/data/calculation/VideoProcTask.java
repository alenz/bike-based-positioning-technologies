package at.fhooe.mcm.mdpm.data.calculation;

import java.util.Arrays;
import java.util.Vector;

import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfDouble;
import org.opencv.core.Point;
import org.opencv.core.Size;
import org.opencv.highgui.Highgui;
import org.opencv.imgproc.Imgproc;

import com.example.hellojni.HelloJni;

import android.os.AsyncTask;
import android.util.Log;
import at.fhooe.mcm.mdpm.data.Marker;
import at.fhooe.mcm.mdpm.data.Position;

/**
 * The {@link VideoProcTask} is an {@link AsyncTask} for calculating the {@link Position} of the camera in a frame.
 * 
 * This is done via the following steps:
 * 1) Either take the {@link Mat} as the input-image or load it from the given path.
 * 2) Gray it and scale it down ({@link Imgproc#pyrDown(Mat, Mat)}) to the halve to save computation time (TODO: probably remove it for better results)
 * 3) call the Native-Library for finding the markers + Id and their corner-positions + Homography.
 * 4) calculate the Position via the given {@link PositionCalculator}-Instance. 
 * (also transfer it to the general track-coordinate-system by taking the Marker-Position and Orientation into account)
 * 5) update the caller ({@link PositionCalculator}) that 0-x positions are there.
 * @author Alexander
 *
 */
public class VideoProcTask extends AsyncTask {

	private static final int NUM_OF_CORNER_POINT_VALUES_PER_MARKER = 8;	// 4*2 (x and y)
	
	public String imgPath;
	public Mat mat;
	public PositionCalculator posCalculator;
	public long time;
	public int frameCount;
	
	public static final String TAG = "VideoProcessing::";
	
	public VideoProcTask(String pathToImage, Mat m, PositionCalculator posCalculator, long time, int frameCount) {
		super();
		this.imgPath = pathToImage;
		this.mat = m;
		this.posCalculator = posCalculator;
		this.time = time;
		this.frameCount = frameCount;
	}
	
	
	
	@Override
	protected void onPostExecute(Object result) {		
		if(result == null) {
			posCalculator.onBadFrameReceived();
			return;
		}
		posCalculator.addCalculatedPositions((Position[])result, time, frameCount);
	}
	
	
	
	@Override
	protected Object doInBackground(Object... params) {
		if(mat == null) {
			long t1 = System.currentTimeMillis();
			mat = Highgui.imread(imgPath);
			long t2 = System.currentTimeMillis();	
			log("Time to load img from file-system: " + (t2-t1));
		}	
		
		long startTime = System.currentTimeMillis();
		Mat gray = new Mat(mat.rows(), mat.cols(), CvType.CV_8UC1);
		Imgproc.cvtColor(mat, gray, 11);
//		Highgui.imwrite(Environment.getExternalStorageDirectory() + "/grayi_" + System.currentTimeMillis() + ".png", gray);
		long endTime = System.currentTimeMillis();
		Log.e(TAG, "Time to store png: " + (endTime-startTime));
		
		
		Imgproc.pyrDown(gray, gray);
		Imgproc.pyrDown(gray, gray);
		Log.e(TAG, "pyred gray: " + gray.rows() + " , " + gray.cols());
		byte[] imgBytes = new byte[(int) gray.total()];
		gray.get(0, 0, imgBytes);
		byte[] foundIds = new byte[21];
		for(int i=0; i<foundIds.length; i++) {
			foundIds[i] = Byte.MAX_VALUE;
		}
		double[] cornerPoints = new double[80];
		double[] H = new double[45];	//space for storing 5 Homographys (9 values = 3x3 matrices)
		int numOfFoundTags = (new HelloJni().findTags(imgBytes, gray.cols(), gray.rows(), foundIds, cornerPoints, H));
		logError("jni findTags method: nr of found tags: " + numOfFoundTags);
		endTime = System.currentTimeMillis();
		log("Found TagIds: " + Arrays.toString(foundIds));

		logError("Time to process Imag for finding Tags: " + (endTime-startTime));
		
		if(foundIds[0]==(Byte.MAX_VALUE)) {
			return null;
		}
		Position[] positions = new Position[numOfFoundTags];
		for(int i=0; i<numOfFoundTags; i++) {
			Vector<Point> points = new Vector<Point>();
			points.add(new Point(cornerPoints[(i*8)+0] , cornerPoints[(i*8)+1]));		//1m
			points.add(new Point(cornerPoints[(i*8)+2] , cornerPoints[(i*8)+3]));
			points.add(new Point(cornerPoints[(i*8)+4] , cornerPoints[(i*8)+5]));
			points.add(new Point(cornerPoints[(i*8)+6] , cornerPoints[(i*8)+7]));			
			
			PositionCalculation posCalc = new PositionCalculation(points);
			posCalculator.camParam.resize(new Size(gray.cols(), gray.rows()));
			Marker m = posCalculator.markerMap.get(foundIds[i]);
			if(m==null) {
				log("Marker with id " + foundIds[i] + " couldnt be found in the MarkerMap");
				continue;
			}
			posCalc.calculateExtrinsics(posCalculator.camParam.getCameraMatrix(), posCalculator.camParam.getDistCoeff(), m.size);
			Position p1 = posCalc.calculateCameraPos();
			log("calculated Postion relative to Marker: -> " + p1.toString());
			
			Position translatedPosition = new Position();
			// correct Position with marker orientation and position in track-coordinate system
			switch(m.angleZ) {		
			case 180:	// for the case when the marker stands head
						// z is the same
						p1.y = -p1.y;
						p1.x = -p1.x;
						break;
			}
			
			switch(m.angleY) {
			case 0:		translatedPosition = p1; 
						break;
			case 90:	translatedPosition.z = p1.x;
						translatedPosition.y = p1.y;
						translatedPosition.x = -p1.z;
						break;
			case 180:	translatedPosition.z = -p1.z;
						translatedPosition.y = p1.y;
						translatedPosition.x = -p1.x;
						break;
			case 270:	translatedPosition.z = -p1.x;
						translatedPosition.y = p1.y;
						translatedPosition.x = p1.z;
						break;
			}
			
			
			log("calculated Postion in Track Coord system (orientation changed): -> " + translatedPosition.toString());
			translatedPosition.x += m.pos.x;
			translatedPosition.y += m.pos.y;
			translatedPosition.z += m.pos.z;
			translatedPosition.markerId = m.id;
			translatedPosition.time = time;
			translatedPosition.frameNr = frameCount;
			log("calculated Postion in Track Coord system (orientation and position of marker used): -> " + translatedPosition.toString());
			positions[i] = translatedPosition;
		}
		
		return positions;
	}
	
	private void log(String string) {
		Log.i(TAG, string);
	}
	
	private void logError(String string) {
		Log.e(TAG, string);
	}

}
