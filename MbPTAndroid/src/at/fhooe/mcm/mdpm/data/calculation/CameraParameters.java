package at.fhooe.mcm.mdpm.data.calculation;

import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfDouble;
import org.opencv.core.Size;

import android.util.Log;

/**
 * The CameraParameter includes the cameraMatrix (Fx,Fy, Cx, Cy) and the distortion-coefficients (5 elements)
 * of one specific camera. These parameters can be taken for example from the standard openCV-Camera Calibration
 * Sample.
 * Predefined parameters for the SJCam 4000 wifi hd   and a digicam are included and can be accessed statically.
 * @author Alexander
 *
 */
public class CameraParameters {

	/** cameraMatrix will be of the form
	// | Fx 0  Cx |
	// | 0  Fy Cy |
	// | 0  0   1 | */
	private Mat cameraMatrix;
	
	/**
	 * The distortion-Matrix is a vector of 5 elements
	 */
	private MatOfDouble distorsionMatrix;
	private Size camSize;
	
	public static final String TAG = "CameraParameters";
	
	public CameraParameters(){
		cameraMatrix = new Mat(3,3,CvType.CV_32FC1);
		distorsionMatrix = new MatOfDouble();
	}
	
    /**Indicates whether this object is valid
     */
    public boolean isValid(){
    	if(cameraMatrix != null)
    		return cameraMatrix.rows()!=0 && cameraMatrix.cols()!=0  && 
    		distorsionMatrix.total() > 0;
    	else
    		return false;
    }
	
	public Mat getCameraMatrix(){
		return cameraMatrix;
	}
	
	public MatOfDouble getDistCoeff(){
		return distorsionMatrix;
	}
	
	public CameraParameters(Mat cameraMat, MatOfDouble distortionMatrix, Size size) {
		this.cameraMatrix = cameraMat;
		this.distorsionMatrix = distortionMatrix;
		this.camSize = size;
	}
	
	public void resize(Size size){
	   
	    if (size == camSize)
	    	return;
	    //now, read the camera size
	    //resize the camera parameters to fit this image size
	    float AxFactor= (float)(size.width)/ (float)(camSize.width);
	    float AyFactor= (float)(size.height)/ (float)(camSize.height);
	    Log.e(TAG, "Resized Cam-Parameters: from: (" + camSize.width + "," + camSize.height + ") to (" + size.width + "," + size.height + ")  =  AxFactor: " +  AxFactor + " , AyFactor: " + AyFactor);
		float[] current = new float[9];
	    cameraMatrix.get(0, 0, current);
		float[] buff = {current[0]*AxFactor, current[1],          current[2]*AxFactor,
				        current[3],          current[4]*AyFactor, current[5]*AyFactor,
				        current[6],          current[7],          current[8]};
		cameraMatrix.put(0, 0, buff);
		camSize = size;
	}
//	
	
	public static CameraParameters createSJCamParameters() {
		Mat cameraMatrix = new Mat(3,3,CvType.CV_32FC1);
		MatOfDouble distorsionMatrix = new MatOfDouble();
		
		//sjcam calib (not so accurate now)
		cameraMatrix.put(0, 0, 1001.7792, 0, 962.3616,
		  0, 1012.7232000000001, 561.27600000000007,
		  0, 0, 1);
		
		
		
		double[] coeffArray = new double[]{-0.31331,
				0.12965,
		  0.00073,
		  -0.00022,
		  -0.02912};
		
		distorsionMatrix.fromArray(coeffArray);	
		
		return new CameraParameters(cameraMatrix, distorsionMatrix, new Size(1920, 1080));
	}
	
	/**
	 * Method creates and returns the camera parameter for the 4:3 photos of the digicam (z.b.: 20M aufl�sung)
	 * @return
	 */
	public static CameraParameters createDigiCamPhotoParameters() {
		Mat cameraMatrix = new Mat(3,3,CvType.CV_32FC1);
		MatOfDouble distorsionMatrix = new MatOfDouble();
		
		//sjcam calib (not so accurate now)
		cameraMatrix.put(0, 0, 	3832.172539362872, 0, 2575.5,
				  				0, 3834.696893399188, 1931.5,
				  				0, 0				, 1);		
		
		double[] coeffArray = new double[]{-0.02054010112876467,
		  0.05140762916622808,
		  0,
		  0,
		  -0.05760247710190284};	
		
		distorsionMatrix.fromArray(coeffArray);	
		
		return new CameraParameters(cameraMatrix, distorsionMatrix, new Size(5152, 3864));
	}
	
	/**
	 * Method creates and returns the camera parameter for the 16:9 videos of the digicam (z.b.: 720p videos with 1280x720 resolutions)
	 * @return
	 */
	public static CameraParameters createDigiCamVideoParameters() {
		Mat cameraMatrix = new Mat(3,3,CvType.CV_32FC1);
		MatOfDouble distorsionMatrix = new MatOfDouble();
		
		//sjcam calib (not so accurate now)
		cameraMatrix.put(0, 0, 	1458.85942264203, 0, 959.5,
								0, 1462.876987995615, 539.5,
								0, 0, 1);		
		
		double[] coeffArray = new double[]{0.0007155146776515218,
		  -0.01385496065714805,
		  0,
		  0,
		  0.03135741548439466};	
		
		distorsionMatrix.fromArray(coeffArray);	
		
		return new CameraParameters(cameraMatrix, distorsionMatrix, new Size(1920, 1080));
	}
	
	/**
	 * Method creates and returns the camera parameter for the Nexus 5 camera, clibrated with 3264x2448
	 * @return
	 */
	public static CameraParameters createNexus5PhotoParameters() {
		Mat cameraMatrix = new Mat(3,3,CvType.CV_32FC1);
		MatOfDouble distorsionMatrix = new MatOfDouble();
		
		//sjcam calib (not so accurate now)
		cameraMatrix.put(0, 0, 	2896.69152428844, 	0	, 1631.5,
								 0, 2899.3962850324	, 1223.5,
								 0, 0					, 1);		
		
		double[] coeffArray = new double[]{0.02310174033034288,
		  -0.1210194445483629,
		  0,
		  0,
		  -0.2269938717278248};	
		
		distorsionMatrix.fromArray(coeffArray);	
		
		return new CameraParameters(cameraMatrix, distorsionMatrix, new Size(3264, 2448));
	}

}
