package at.fhooe.mcm.mdpm.data;

/**
 * A Marker is a fiducial black 2D-quadrat including a black/white pattern to identify them.
 * The current used Marker-Detection Algorithm is AprilTag.
 * Important attributes besides the {@link Marker#id} are the 3d-position of the Marker in
 * the Track-Coordinate System, its physical size (length of one quadrat-side) and the orientation (angels).
 * @author Alexander
 *
 */
public class Marker {

	public int id;
	public Position pos;
	public float size;	//one side of the marker-square
	public int angleX;
	public int angleY;
	public int angleZ;
	
	public Marker(int id, Position pos, float size, int angleX, int angleY, int angleZ) {
		this.id = id;
		this.pos = pos;
		this.size = size;
		this.angleX = angleX;
		this.angleY = angleY;
		this.angleZ = angleZ;
	}
}
