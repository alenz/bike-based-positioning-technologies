package at.fhooe.mcm.mdpm.util;

import android.util.Log;

/**
 * Utility-Class for some common tasks like logging
 * @author Alexander
 *
 */
public class Util {

	/**
	 * Log only if this is enabled (for debugging purposes in the devlopment phase)
	 */
	public static final boolean shouldLog = true;
	
	/**
	 * writes a log-message to the Android-Log-Output with the given TAG if logging is enabled.
	 * In a release version this should be disabled.
	 * @param TAG
	 * @param log
	 */
	public static void log(String TAG, String log) {
		if(shouldLog) Log.e(TAG, log);
	}
}
