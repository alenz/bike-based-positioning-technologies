package at.fhooe.mcm.mdpm.videotest;

import java.util.Arrays;
import java.util.Vector;

import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfDouble;
import org.opencv.core.Point;
import org.opencv.imgproc.Imgproc;

import umich.cem.live.marvigator.Native;
import android.util.Log;
import at.fhooe.mcm.mdpm.data.Position;
import at.fhooe.mcm.mdpm.data.calculation.PositionCalculation;
import at.fhooe.mcm.mdpm.data.calculation.VideoProcTask;

import com.example.hellojni.HelloJni;

/**
 * Initially the Frame-Processing inclusive the Pos-Calc was done in this Thread-Class - but now its done in the {@link VideoProcTask}
 * @author Alexander
 *
 */
public class VideoProcessThread extends Thread {
	
	public String item;
	public Mat mat;
	
	public static final String TAG = "VideoProcessing::";
	
	public VideoProcessThread(String item, Mat m) {
		super();
		this.item = item;
		this.mat = m;
	}

	@Override
	public void run() {			
		
		long startTime = System.currentTimeMillis();
		Mat gray = new Mat(mat.rows(), mat.cols(), CvType.CV_8UC1);
		Imgproc.cvtColor(mat, gray, 11);
//		Highgui.imwrite(Environment.getExternalStorageDirectory() + "/grayi_" + System.currentTimeMillis() + ".png", gray);
		long endTime = System.currentTimeMillis();
		Log.e(TAG, "Time to store png: " + (endTime-startTime));
		
		
//		Imgproc.pyrDown(gray, gray);
		Log.e(TAG, "pyred gray: " + gray.rows() + " , " + gray.cols());
		byte[] imgBytes = new byte[(int) gray.total()];
		gray.get(0, 0, imgBytes);
		byte[] foundIds = new byte[21];
		for(int i=0; i<foundIds.length; i++) {
			foundIds[i] = Byte.MAX_VALUE;
		}
		double[] cornerPoints = new double[80];
		double[] H = new double[45];	//space for storing 5 Homographys (9 values = 3x3 matrices)
		log("jni findTags method: " + (new HelloJni().findTags(imgBytes, gray.cols(), gray.rows(), foundIds, cornerPoints, H)));
		endTime = System.currentTimeMillis();
		log("Found TagIds: " + Arrays.toString(foundIds));
//		log("CornerPoints: " + Arrays.toString(cornerPoints));
//		log("Homography: " + Arrays.toString(H));
//		log("Time to call JNI findTags method: " + (endTime-startTime));
//		summendTimesForProcessing = summendTimesForProcessing + (endTime-startTime);
//		frameCount++;
		Log.e(TAG, "Time to process Imag for finding Tags: " + (endTime-startTime));
//		Log.e(TAG, "Current Summed Time for processing all Tags: " + summendTimesForProcessing + " frameCount: " + frameCount + "  avgProcTimePerFrame: " + summendTimesForProcessing/frameCount);
	
//		int[] ids = new int[10];
//		Native.SetTagFamily(Native.TAG25H9);
//		float[] homo = new float[9];
//		Native.FindTag(gray.getNativeObjAddr(), homo, ids);
//		log("Marvigator Homo: " + Arrays.toString(homo));
//		log("Marvigator Ids: " + Arrays.toString(ids));
		
		if(foundIds[0]==(Byte.MAX_VALUE)) {
			return;
		}
		
		
		Mat cameraMatrix = new Mat(3,3,CvType.CV_32FC1);
		MatOfDouble distorsionMatrix = new MatOfDouble();
		
		//sjcam calib (not so accurate now)
		cameraMatrix.put(0, 0, 1001.7792, 0, 962.3616,
		  0, 1012.7232000000001, 561.27600000000007,
		  0, 0, 1);
		
		
		
		double[] coeffArray = new double[]{-0.31331,
				0.12965,
		  0.00073,
		  -0.00022,
		  -0.02912};
		
		distorsionMatrix.fromArray(coeffArray);	
		
		Vector<Point> points = new Vector<Point>();
		points.add(new Point(cornerPoints[0] , cornerPoints[1]));		//1m
		points.add(new Point(cornerPoints[2] , cornerPoints[3]));
		points.add(new Point(cornerPoints[4] , cornerPoints[5]));
		points.add(new Point(cornerPoints[6] , cornerPoints[7]));			
		
		float[] hf = new float[]{(float) H[0],(float) H[1],(float) H[2],(float) H[3],(float) H[4],(float) H[5], (float) H[6],(float) H[7],(float) H[8]};
//		log("homo f: " + Arrays.toString(hf));
		PositionCalculation posCalc = new PositionCalculation(points);
		posCalc.calculateExtrinsics(cameraMatrix, distorsionMatrix, 0.144f);
		final Position p1 = posCalc.calculateCameraPos();
		log("calculated Postion: -> " + p1.toString());
		
	}

	private void log(String string) {
		Log.e(TAG, string);
	}
}