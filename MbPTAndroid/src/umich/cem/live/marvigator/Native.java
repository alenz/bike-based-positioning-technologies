// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package umich.cem.live.marvigator;

/**
 * Native-AprilTag Library decompiled from the Marvigator-Project (which is an Indoor-Navigation-System).
 * But it doesnt fit for me, so it is not used.
 *
 */
public class Native
{

    public static final int TAG16H5 = 0;
    public static final int TAG25H7 = 1;
    public static final int TAG25H9 = 2;

    public Native()
    {
    }

    public static native void FindTag(long l, float af[], int ai[]);

    public static native void SetTagFamily(int i);
}
