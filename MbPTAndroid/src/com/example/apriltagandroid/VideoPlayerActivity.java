package com.example.apriltagandroid;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

import android.app.Activity;
import android.app.AlertDialog;
import android.graphics.Bitmap;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.Toast;
import android.widget.VideoView;

/**
 * {@link Activity} for playing videos. The path to videos which are already stored on the SDCard
 * are given via an Intent-Bundle-Extra.
 * It simply useds a VideoView which has already some basic built-in controls like start/pause/stop
 * 
 * @author Alexander
 *
 */
public class VideoPlayerActivity extends Activity {

	public static final String KEY_VIDEO_PATH = "keyVideoPath";

	MediaMetadataRetriever mediaMetadataRetriever;
	MediaController myMediaController;
	VideoView myVideoView;
	String viewSource = "rtsp://192.168.1.254/2014_1225_162257_001.MOV";

	View main_ll;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		Bundle b = getIntent().getExtras();
		String temp;
		if (b != null) {
			temp = b.getString(KEY_VIDEO_PATH);
			if(temp != null) {
				viewSource = temp;
			}
		}

		setContentView(R.layout.activity_videoplayer);

		mediaMetadataRetriever = new MediaMetadataRetriever();
		mediaMetadataRetriever.setDataSource(viewSource);

		myVideoView = (VideoView) findViewById(R.id.videoview);
		// myVideoView.setVideoURI(Uri.parse(viewSource));
		myVideoView.setVideoURI(Uri.parse(viewSource));
		myVideoView.setDrawingCacheEnabled(true);

		myMediaController = new MediaController(this);
		myVideoView.setMediaController(myMediaController);

		myVideoView.setOnCompletionListener(myVideoViewCompletionListener);
		myVideoView.setOnPreparedListener(MyVideoViewPreparedListener);
		myVideoView.setOnErrorListener(myVideoViewErrorListener);

		myVideoView.requestFocus();
		myVideoView.start();

		main_ll = findViewById(R.id.main_ll);
		main_ll.setDrawingCacheEnabled(true);

	}

	MediaPlayer.OnCompletionListener myVideoViewCompletionListener = new MediaPlayer.OnCompletionListener() {

		@Override
		public void onCompletion(MediaPlayer arg0) {
			Toast.makeText(VideoPlayerActivity.this, "End of Video",
					Toast.LENGTH_LONG).show();
		}
	};

	MediaPlayer.OnPreparedListener MyVideoViewPreparedListener = new MediaPlayer.OnPreparedListener() {

		@Override
		public void onPrepared(MediaPlayer mp) {

			long duration = myVideoView.getDuration(); // in millisecond
			// Toast.makeText(MainActivity1.this,
			// "Duration: " + duration + " (ms)",
			// Toast.LENGTH_LONG).show();

		}
	};

	MediaPlayer.OnErrorListener myVideoViewErrorListener = new MediaPlayer.OnErrorListener() {

		@Override
		public boolean onError(MediaPlayer mp, int what, int extra) {
			// MediaPlayer.
			Toast.makeText(VideoPlayerActivity.this,
					"Error!!! What: " + what + " extra: " + extra,
					Toast.LENGTH_LONG).show();
			return true;
		}
	};

}