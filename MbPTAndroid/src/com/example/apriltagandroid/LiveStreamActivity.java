package com.example.apriltagandroid;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.android.Utils;
import org.opencv.core.Mat;

import sjcam.util.SJCamUtil;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.SurfaceTexture;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnBufferingUpdateListener;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.media.MediaPlayer.OnVideoSizeChangedListener;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;
import android.util.SparseArray;
import android.view.MotionEvent;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.widget.ImageView;
import android.widget.Toast;
import at.fhooe.mcm.mdpm.data.Marker;
import at.fhooe.mcm.mdpm.data.MarkerUtil;
import at.fhooe.mcm.mdpm.data.Position;
import at.fhooe.mcm.mdpm.data.calculation.CameraParameters;
import at.fhooe.mcm.mdpm.data.calculation.PositionCalculator;
import at.fhooe.mcm.mdpm.data.view.PositionUpdateListener;
import at.fhooe.mcm.mdpm.services.LocalBluetoothService;
import at.fhooe.mcm.mdpm.util.Util;
import at.fhooe.mcm.mdpm.videotest.ExtractMpegFrames;

import com.example.drawertest.util.SystemUiHider;
import com.google.gson.Gson;

/**
 * Full-Screen Activity for showing the live-stream of the Action-Camera and calculate the camera-positions
 * from the frames received from the action-camera (connected via Wifi)
 * 
 * TODO: adding the binding to the {@link LocalBluetoothService} for seeing the positions on a Tablet.
 *
 * @author Alexander
 */
public class LiveStreamActivity extends Activity implements
		TextureView.SurfaceTextureListener, OnBufferingUpdateListener,
		OnCompletionListener, OnPreparedListener, OnVideoSizeChangedListener, PositionUpdateListener {

	private PositionCalculator posCalculator = null;
	private MediaPlayer mp;
	private TextureView tv;
	public static String MY_VIDEO = "rtsp://192.168.1.254/2014_1225_162257_001.MOV";
	public static String TAG = "LiveStreamActivity";

	public float fps = 30;
	public int frameCount = 0;	;
	
	private SparseArray<Marker> trackSetup = null;;
	
	/**
	 * Whether or not the system UI should be auto-hidden after
	 * {@link #AUTO_HIDE_DELAY_MILLIS} milliseconds.
	 */
	private static final boolean AUTO_HIDE = true;

	/**
	 * If {@link #AUTO_HIDE} is set, the number of milliseconds to wait after
	 * user interaction before hiding the system UI.
	 */
	private static final int AUTO_HIDE_DELAY_MILLIS = 3000;

	/**
	 * If set, will toggle the system UI visibility upon interaction. Otherwise,
	 * will show the system UI visibility upon interaction.
	 */
	private static final boolean TOGGLE_ON_CLICK = true;

	/**
	 * The flags to pass to {@link SystemUiHider#getInstance}.
	 */
	private static final int HIDER_FLAGS = SystemUiHider.FLAG_HIDE_NAVIGATION;

	/**
	 * The instance of the {@link SystemUiHider} for this activity.
	 */
	private SystemUiHider mSystemUiHider;
	
	// begin service connection stuff
	/** Messenger for communicating with the service. */
	Messenger mService = null;

	/** Flag indicating whether we have called bind on the service. */
	boolean mBound;
	
	/**
	 * Class for interacting with the main interface of the service.
	 */
	private ServiceConnection mConnection = new ServiceConnection() {
		public void onServiceConnected(ComponentName className, IBinder service) {
			// This is called when the connection with the service has been
			// established, giving us the object we can use to
			// interact with the service. We are communicating with the
			// service using a Messenger, so here we get a client-side
			// representation of that from the raw IBinder object.
			mService = new Messenger(service);
			mBound = true;
			Log.e(TAG, "Services is bound!");
			Message msg = Message.obtain(null,
					LocalBluetoothService.MSG_START_BT, 0, 0);
			try {
				mService.send(msg);
			} catch (RemoteException e) {
				e.printStackTrace();
			}
		}

		public void onServiceDisconnected(ComponentName className) {
			// This is called when the connection with the service has been
			// unexpectedly disconnected -- that is, its process crashed.
			mService = null;
			mBound = false;
			Log.e(TAG, "Services isnt bound anymore!");
		}
	};

	private Gson gson = new Gson();

	public void sendPositionOverBT(Position p) {
		if (!mBound) {
			Log.e(TAG, "Services isnt bound!");
			return;
		}
		// Create and send a message to the service, using a supported 'what'
		// value
		String jsonString = gson.toJson(p);
		Message msg = Message.obtain(null,
				LocalBluetoothService.MSG_WRITE_DATA, 0, 0);
		Bundle b = new Bundle();
		b.putString(LocalBluetoothService.KEY_WRITE_DATA, jsonString);
		msg.setData(b);
		try {
			mService.send(msg);
		} catch (RemoteException e) {
			e.printStackTrace();
		}
	}
	  
	@Override
	public void onStart() {
		super.onStart();
		// Bind to the service
		bindService(new Intent(this, LocalBluetoothService.class), mConnection,
				Context.BIND_AUTO_CREATE);
		Log.e(TAG, "Services is now bound!");
	}
		
	@Override
	public void onStop() {
		Util.log(TAG, "onStop:: unbind service");
		super.onStop();
		// Unbind from the service
		if (mBound) {
			unbindService(mConnection);
			mBound = false;
		}
	}

	// end service connection stuff

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().requestFeature(Window.FEATURE_ACTION_BAR); // new
		getActionBar().hide(); // new
		setContentView(R.layout.activity_livestream_fullscreen);
		
		trackSetup = MarkerUtil.getUserPreferredMarkerSetup(this);
		posCalculator = new PositionCalculator(SJCamUtil.baseFolder + "LiveposCalcData_" + System.currentTimeMillis() + ".txt", SJCamUtil.baseFolder + "LiveposMultiMarkerCalcData_" + startTime + ".txt", CameraParameters.createSJCamParameters(), this, trackSetup);
		

		// final View controlsView =
		// findViewById(R.id.fullscreen_content_controls);
		tv = (TextureView) findViewById(R.id.texture_view1);
		tv.setSurfaceTextureListener(this);
		tv.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
//				getBitmap(tv);
				posCalculator.savePositions();
			}
		});

		// Set up an instance of SystemUiHider to control the system UI for
		// this activity.
		mSystemUiHider = SystemUiHider.getInstance(this, tv, HIDER_FLAGS);
		mSystemUiHider.setup();
		// mSystemUiHider
		// .setOnVisibilityChangeListener(new
		// SystemUiHider.OnVisibilityChangeListener() {
		// // Cached values.
		// int mControlsHeight;
		// int mShortAnimTime;
		//
		// @Override
		// @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
		// public void onVisibilityChange(boolean visible) {
		//
		// if (visible && AUTO_HIDE) {
		// // Schedule a hide().
		// delayedHide(AUTO_HIDE_DELAY_MILLIS);
		// }
		// }
		// });

		// Set up the user interaction to manually show or hide the system UI.
		// tv.setOnClickListener(new View.OnClickListener() {
		// @Override
		// public void onClick(View view) {
		// if (TOGGLE_ON_CLICK) {
		// mSystemUiHider.toggle();
		// } else {
		// mSystemUiHider.show();
		// }
		// }
		// });

		// Upon interacting with UI controls, delay any scheduled hide()
		// operations to prevent the jarring behavior of controls going away
		// while interacting with the UI.
		// findViewById(R.id.dummy_button).setOnTouchListener(
		// mDelayHideTouchListener);
		
		OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_2_4_6, this, mLoaderCallback);
		
		
		System.loadLibrary("mbpt-jni");	
	}
	
	boolean openCVLoaded = false;
	long startTime = -1;
	
	private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
		
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS:
                {
                    Log.i(TAG, "OpenCV loaded successfully");
                    openCVLoaded = true;
                  //start image processing
                    startTime = System.currentTimeMillis();
            		processImageHandler.postDelayed(processImageRunnable, 300);
                    
                } break;
                default:
                {
                    super.onManagerConnected(status);
                } break;
            }
        }
    };

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);

		// Trigger the initial hide() shortly after the activity has been
		// created, to briefly hint to the user that UI controls
		// are available.
		delayedHide(100);
		
		
	}

	/**
	 * Touch listener to use for in-layout UI controls to delay hiding the
	 * system UI. This is to prevent the jarring behavior of controls going away
	 * while interacting with activity UI.
	 */
	View.OnTouchListener mDelayHideTouchListener = new View.OnTouchListener() {
		@Override
		public boolean onTouch(View view, MotionEvent motionEvent) {
			if (AUTO_HIDE) {
				delayedHide(AUTO_HIDE_DELAY_MILLIS);
			}
			return false;
		}
	};

	Handler mHideHandler = new Handler();
	Runnable mHideRunnable = new Runnable() {
		@Override
		public void run() {
			mSystemUiHider.hide();
		}
	};

	/**
	 * Schedules a call to hide() in [delay] milliseconds, canceling any
	 * previously scheduled calls.
	 */
	private void delayedHide(int delayMillis) {
		mHideHandler.removeCallbacks(mHideRunnable);
		mHideHandler.postDelayed(mHideRunnable, delayMillis);
	}

	Handler processImageHandler = new Handler();
	Runnable processImageRunnable = new Runnable() {

		@Override
		public void run() {
			frameCount++;
			long currentTime = System.currentTimeMillis();
			Bitmap bm = getBitmap(tv);
			if(bm == null) return;
			 Mat m = new Mat();
	         Utils.bitmapToMat(bm, m);
	         posCalculator.addImage("livestream", m, (currentTime-startTime), frameCount);
			processImageHandler.postDelayed(processImageRunnable, 300);
		}
	};

	public Bitmap getBitmap(TextureView vv) {
//		String mPath = Environment.getExternalStorageDirectory().toString()
//				+ "/fullscreen_" + System.currentTimeMillis() + ".png";
//		Toast.makeText(getApplicationContext(),
//				"Capturing Screenshot: " + mPath, Toast.LENGTH_SHORT).show();

		Bitmap bm = vv.getBitmap();

		if (bm == null) {
			Log.e(TAG, "bitmap is null");
//			Toast.makeText(LiveStreamActivity.this, "bmFrame == null!",
//					Toast.LENGTH_LONG).show();
			return null;
		} else {
//			AlertDialog.Builder myCaptureDialog = new AlertDialog.Builder(
//					LiveStreamActivity.this);
//			ImageView capturedImageView = new ImageView(LiveStreamActivity.this);
//			capturedImageView.setImageBitmap(bm);
//			LayoutParams capturedImageViewLayoutParams = new LayoutParams(
//					LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
//			capturedImageView.setLayoutParams(capturedImageViewLayoutParams);
//
//			myCaptureDialog.setView(capturedImageView);
//			myCaptureDialog.show();
			
			return bm;
		}

//		OutputStream fout = null;
//		File imageFile = new File(mPath);
//
//		try {
//			fout = new FileOutputStream(imageFile);
//			bm.compress(Bitmap.CompressFormat.PNG, 90, fout);
//			fout.flush();
//			fout.close();
//		} catch (FileNotFoundException e) {
//			Log.e(TAG, "FileNotFoundException");
//			e.printStackTrace();
//		} catch (IOException e) {
//			Log.e(TAG, "IOException");
//			e.printStackTrace();
//		}
		
		

	}

	@Override
	public void onVideoSizeChanged(MediaPlayer mp, int width, int height) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onPrepared(MediaPlayer mp) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onCompletion(MediaPlayer mp) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onBufferingUpdate(MediaPlayer mp, int percent) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onSurfaceTextureAvailable(SurfaceTexture surface, int width,
			int height) {
		Surface s = new Surface(surface);

		try {
			mp = new MediaPlayer();
			mp.setDataSource(MY_VIDEO);
			mp.setSurface(s);
			mp.prepare();

			mp.setOnBufferingUpdateListener(this);
			mp.setOnCompletionListener(this);
			mp.setOnPreparedListener(this);
			mp.setOnVideoSizeChangedListener(this);
			

			mp.setAudioStreamType(AudioManager.STREAM_MUSIC);
			mp.start();

		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width,
			int height) {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void onSurfaceTextureUpdated(SurfaceTexture surface) {
		// TODO Auto-generated method stub

	}
	

	@Override
	public void onNewPositionCalculated(Position p) {
		// TODO handle it to the LocalBluetoothService further to send it to an possible BT-Client (Tablet to show positions)
		sendPositionOverBT(p);
	}

	@Override
	public void onBadFrameReceived() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onCalculationFinished(int numOfProcessedFrames) {
		// TODO Auto-generated method stub
		
	}
}
