package com.example.apriltagandroid.fragments;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Vector;

import org.apache.commons.io.FileUtils;
import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfDouble;
import org.opencv.core.Point;
import org.opencv.highgui.Highgui;
import org.opencv.imgproc.Imgproc;

import sjcam.util.SJCamUtil;
import sjcam.util.SJCamUtil.FileType;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.PowerManager;
import android.os.RemoteException;
import android.preference.PreferenceManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import at.fhooe.mcm.mbpt.view.util.SwipeDismissListViewTouchListener;
import at.fhooe.mcm.mbpt.view.util.SwipeDismissListViewTouchListener.DismissCallbacks;
import at.fhooe.mcm.mdpm.data.Marker;
import at.fhooe.mcm.mdpm.data.MarkerUtil;
import at.fhooe.mcm.mdpm.data.Position;
import at.fhooe.mcm.mdpm.data.calculation.CameraParameters;
import at.fhooe.mcm.mdpm.data.calculation.PositionCalculation;
import at.fhooe.mcm.mdpm.data.calculation.PositionCalculator;
import at.fhooe.mcm.mdpm.data.view.ListItem;
import at.fhooe.mcm.mdpm.data.view.PositionUpdateListener;
import at.fhooe.mcm.mdpm.services.LocalBluetoothService;

import com.example.apriltagandroid.MainActivity;
import com.example.apriltagandroid.PrefActivity;
import com.example.apriltagandroid.R;
import com.example.apriltagandroid.VideoPlayerActivity;
import com.example.apriltagandroid.VideoProcessingActivity;
import com.example.hellojni.HelloJni;
import com.google.gson.Gson;

/**
 * The Entry-Point-Fragment for the App. It is abstracted a little bit, to handle either
 * Photos or Videos.
 * The main functionalities of this fragment are:
 *  - show list of found Images/Videos
 *  - load file-List from an wifi-connected Action-Camera
 *  - download the files when the user wants one by one or simply all available on the camera
 *  - view images and videos
 *  - start processing of images (results will be shown in this fragment) and videos (Processing is an own activity)
 *  
 * @author Alexander
 *
 */
public class BaseFragment extends Fragment implements OnRefreshListener, PositionUpdateListener{

	/**
	 * The fragment argument representing the section number for this fragment.
	 */
	private static final String ARG_SECTION_NUMBER = "section_number";
	
//	private SparseArray<Marker> markerMap = MarkerUtil.getUserPreferredMarkerSetup(getActivity()); // TODO: delete if not used anymore
	private Context context;
	
	private int selectedCamera = -1; 	// as choosen in the settings-activity
	private SparseArray<Marker> selectedTrackSetup = null; // as choosen in the settings activity
	
	private PositionCalculator posCalculator = null;
	
	/**
	 * Returns a new instance of this fragment for the given section number.
	 */
	public static BaseFragment newInstance(int sectionNumber, Context context, FileType fileType) {
		BaseFragment fragment = new BaseFragment(context, fileType);
		Bundle args = new Bundle();
		args.putInt(ARG_SECTION_NUMBER, sectionNumber);
		fragment.setArguments(args);
		fragment.setHasOptionsMenu(true);
		return fragment;
	}

	public BaseFragment(Context context, FileType fileType) {
		this.context = context;
		this.fileType = fileType;
	}
	
	public BaseFragment() {
		super();
		context = getActivity();
	
	}
	
	private View rootView;
	
	protected ProgressDialog mProgressDialog;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		rootView = inflater.inflate(R.layout.activity_camera_file_list, container,
				false);
		
		super.onCreate(savedInstanceState);
		
		mLoaderCallback = new BaseLoaderCallback(getActivity().getApplicationContext()) {
			  @Override
		      public void onManagerConnected(int status) {
		          switch (status) {
		              case LoaderCallbackInterface.SUCCESS:
		              {            	  
		                  Log.i(TAG, "OpenCV loaded successfully");
		                  System.loadLibrary("marvigator_native");
		                  System.loadLibrary("mbpt-jni");
		                  opencvLoaded = true;   
		                  
		                  loadPreferences();	// set CameraParameter according to user-preference
		              } break;
		              default:
		              {
		                  super.onManagerConnected(status);
		              } break;
		          }
		      }
		  };
		  
		  
		
		OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_2_4_6, getActivity().getApplicationContext(), mLoaderCallback);
//		setContentView(R.layout.activity_camera_file_list);
		 
		// Retrieve and cache the system's default "short" animation time.
        mShortAnimationDuration = getResources().getInteger(
                android.R.integer.config_shortAnimTime);
        
		final ListView listview = (ListView) rootView.findViewById(R.id.listview);
		pos1X = (TextView)rootView.findViewById(R.id.txt_pos1_x);
		pos1Y = (TextView)rootView.findViewById(R.id.txt_pos1_y);
		pos1Z = (TextView)rootView.findViewById(R.id.txt_pos1_z);
		pos2X = (TextView)rootView.findViewById(R.id.txt_pos2_x);
		pos2Y = (TextView)rootView.findViewById(R.id.txt_pos2_y);
		pos2Z = (TextView)rootView.findViewById(R.id.txt_pos2_z);
		
		pos1AngleX = (TextView)rootView.findViewById(R.id.txt_pos1_angle_x);
		pos1AngleY = (TextView)rootView.findViewById(R.id.txt_pos1_angle_y);
		pos1AngleZ = (TextView)rootView.findViewById(R.id.txt_pos1_angle_z);		
		pos2AngleX = (TextView)rootView.findViewById(R.id.txt_pos2_angle_x);
		pos2AngleY = (TextView)rootView.findViewById(R.id.txt_pos2_angle_y);
		pos2AngleZ = (TextView)rootView.findViewById(R.id.txt_pos2_angle_z);
		
		selectedImage = (ImageView)rootView.findViewById(R.id.thumb_img_button);
		selectedImage.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				log("on selectedImage click -> zoom (make it bigger)");
				CharSequence contentDescription = selectedImage.getContentDescription();
				if(contentDescription != null) {
					String imgPath = contentDescription.toString();
					log("content-description: " + imgPath);
					zoomImageFromThumb(v, BitmapFactory.decodeFile(imgPath));					
				} else {
					log("content-description not set");
				}
			}
		});
		 adapter = new StableArrayAdapter(context,
					android.R.layout.simple_list_item_1, list);
		listview.setAdapter(adapter);
		listview.setClickable(true);
		
		SwipeDismissListViewTouchListener touchListener =
				          new SwipeDismissListViewTouchListener(
				        		  listview,
				                  new DismissCallbacks() {
				                      public void onDismiss(ListView listView, int[] reverseSortedPositions) {
				                          for (int position : reverseSortedPositions) {
				                              adapter.remove(adapter.getItem(position));
				                          }
				                          adapter.notifyDataSetChanged();
				                      }

									@Override
									public boolean canDismiss(int position) {
										// TODO Auto-generated method stub
										return true;
									}
				                  });
		listview.setOnTouchListener(touchListener);
		listview.setOnScrollListener(touchListener.makeScrollListener());
		
		listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, final View view,
					int position, long id) {
				final ListItem item = (ListItem) parent
						.getItemAtPosition(position);
				if(fileType == FileType.IMAGES){
					
					log("On item Clicke...");
					Toast.makeText(BaseFragment.this.getActivity(),
							"Start processing...", Toast.LENGTH_SHORT).show();
					if (!opencvLoaded)
						return;
					
					if(item.path != null && item.path != "") {
						pos1X.setText("x: ");
						pos1Y.setText("y: ");
						pos1Z.setText("z: ");
						
						pos1AngleX.setText("angle x: ");
						pos1AngleY.setText("angle y: ");
						pos1AngleZ.setText("angle z: ");
						
//					new ProcessImageThread(item.path).start();
						String fileName = item.imgPath;
						log("processing " + fileName);
						
						posCalculator.addImage(fileName, null, 0, 0);
						
						if (item.thumbPath != null && item.thumbPath != "") {
							selectedImage.setImageBitmap(BitmapFactory
									.decodeFile(item.thumbPath));
							selectedImage.setContentDescription(item.imgPath);
						}					
					}
				} else if (fileType == FileType.VIDEOS) {
					//TODO: add video processing
//					Toast.makeText(BaseFragment.this.getActivity(),
//							"Processing Video is currently not implemented, commint soon...", Toast.LENGTH_SHORT).show();
//				
					Intent intent = new Intent(context, VideoProcessingActivity.class);
					intent.putExtra(VideoProcessingActivity.INTENT_EXTRA_CAMERA, selectedCamera);
					intent.putExtra(VideoProcessingActivity.INTENT_EXTRA_FILE_PATH, item.imgPath);
					startActivity(intent);
				}
				
			}

		});		
		
		listview.setOnItemLongClickListener(new OnItemLongClickListener() {

			@Override
			public boolean onItemLongClick(AdapterView<?> parent, View view,
					int position, long id) {
				log("long Click...");
////				Toast.makeText(CameraFileListActivity.this, "show big image...", Toast.LENGTH_SHORT).show();
				ListItem item = (ListItem) parent.getItemAtPosition(position);
//				zoomImageFromThumb(view, BitmapFactory.decodeFile(item.imgPath));				

				// instantiate it within the onCreate method
				mProgressDialog = new ProgressDialog(getActivity());
				mProgressDialog.setMessage("Downloading: " + item.path);
				mProgressDialog.setIndeterminate(true);
				mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
				mProgressDialog.setCancelable(true);

				String urlToDownload = SJCamUtil.baseURL + item.path;
				String pathToSave = SJCamUtil.baseFolder + item.path;
				// execute this when the downloader must be fired
				final DownloadTask downloadTask = new DownloadTask(getActivity(), item);
				downloadTask.execute(urlToDownload, pathToSave);

				mProgressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
				    @Override
				    public void onCancel(DialogInterface dialog) {
				        downloadTask.cancel(true);
				    }
				});
				
				return true;
			}
			
		});		
		
		 mSwipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.container);
	     mSwipeRefreshLayout.setOnRefreshListener(this);
		mSwipeRefreshLayout.setColorSchemeResources(R.color.orange, R.color.blue, R.color.green, R.color.purple);
//		mSwipeRefreshLayout.setColorSchemeResources(arg0);
		//initially load existing files from SDCard which where already synced with the phone
//		new LoadFilesFromSDCardThread().start();
		new RestorStateThread().start();
		
		return rootView;
	}

	private void loadPreferences() {
		SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
		  selectedCamera = new Integer(sharedPreferences.getString(getString(R.string.pref_key_camera), "-1"));
		  selectedTrackSetup = MarkerUtil.getUserPreferredMarkerSetup(context);
		
		  
		  long startTime = System.currentTimeMillis();
		  
		  switch(selectedCamera) {
			case 1:
				posCalculator = new PositionCalculator(SJCamUtil.baseFolder + "/calculatedPositions_sjcam_"+startTime+".json",  SJCamUtil.baseFolder + "/calculatedMultiMarkerPositions_sjcam_"+startTime+".json", CameraParameters.createSJCamParameters(), this, selectedTrackSetup);
				break;
			case 2:
				posCalculator = new PositionCalculator(SJCamUtil.baseFolder + "/calculatedPositions_digicam_"+startTime+".json",SJCamUtil.baseFolder + "/calculatedMultiMarkerPositions_digicam_"+startTime+".json", CameraParameters.createDigiCamPhotoParameters(), this, selectedTrackSetup);
				break;
			case 3:
				posCalculator = new PositionCalculator(SJCamUtil.baseFolder + "/calculatedPositions_digicam16_9_"+startTime+".json", SJCamUtil.baseFolder + "/calculatedMultiMarkerPositions_digicam16_9_"+startTime+".json" , CameraParameters.createDigiCamVideoParameters(), this, selectedTrackSetup);
				break;
			case 4:
				posCalculator = new PositionCalculator(SJCamUtil.baseFolder + "/calculatedPositions_nexus5_"+startTime+".json" , SJCamUtil.baseFolder + "/calculatedMultiMarkerPositions_nexus5_"+startTime+".json", CameraParameters.createNexus5PhotoParameters(), this, selectedTrackSetup);
				break;
			}
		  
		  log("Slected Camera from preferences: " + selectedCamera);
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		((MainActivity) activity).onSectionAttached(getArguments().getInt(
				ARG_SECTION_NUMBER));
	}
	
	
	private static final String TAG = "CameraFileListActivity";
	
	final ArrayList<ListItem> list = new ArrayList<ListItem>();
	StableArrayAdapter adapter;
	
	int frameCount = 0;
	long summendTimesForProcessing = -1;
	boolean opencvLoaded = false;
	
	
	TextView pos1X, pos1Y, pos1Z,   pos2X, pos2Y, pos2Z;
	TextView pos1AngleX, pos1AngleY, pos1AngleZ,   pos2AngleX, pos2AngleY, pos2AngleZ;
	ImageView selectedImage;
	
	private BaseLoaderCallback mLoaderCallback;
  
  private SwipeRefreshLayout mSwipeRefreshLayout;

  private FileType fileType;
  
  
  /** Messenger for communicating with the service. */
  Messenger mService = null;

  /** Flag indicating whether we have called bind on the service. */
  boolean mBound;

  /**
   * Class for interacting with the main interface of the service.
   */
  private ServiceConnection mConnection = new ServiceConnection() {
      public void onServiceConnected(ComponentName className, IBinder service) {
          // This is called when the connection with the service has been
          // established, giving us the object we can use to
          // interact with the service.  We are communicating with the
          // service using a Messenger, so here we get a client-side
          // representation of that from the raw IBinder object.
          mService = new Messenger(service);
          mBound = true;
          Log.e(TAG, "Services is bound!");
          Message msg = Message.obtain(null, LocalBluetoothService.MSG_START_BT, 0, 0);
          try {
              mService.send(msg);
          } catch (RemoteException e) {
              e.printStackTrace();
          }
      }

      public void onServiceDisconnected(ComponentName className) {
          // This is called when the connection with the service has been
          // unexpectedly disconnected -- that is, its process crashed.
          mService = null;
          mBound = false;
          Log.e(TAG, "Services isnt bound anymore!");
      }
  };
  
  private Gson gson = new Gson();

  public void sendPositionOverBT(Position p) {
      if (!mBound) {
    	  Log.e(TAG, "Services isnt bound!");
    	  return;
      }
      // Create and send a message to the service, using a supported 'what' value
      String jsonString = gson.toJson(p);	
      Message msg = Message.obtain(null, LocalBluetoothService.MSG_WRITE_DATA, 0, 0);
      Bundle b = new Bundle();
      b.putString(LocalBluetoothService.KEY_WRITE_DATA, jsonString);
      msg.setData(b);
      try {
          mService.send(msg);
      } catch (RemoteException e) {
          e.printStackTrace();
      }
  }
  
  @Override
  public void onStart() {
      super.onStart();
      // Bind to the service
      getActivity().bindService(new Intent(getActivity(), LocalBluetoothService.class), mConnection,
          Context.BIND_AUTO_CREATE);     
      Log.e(TAG, "Services is now bound!");
  }
	
	@Override
	public void onStop() {
		SJCamUtil.storeState(list, fileType);
		log("onPause:: storeState");
		super.onStop();
		 // Unbind from the service
	      if (mBound) {
	          getActivity().unbindService(mConnection);
	          mBound = false;
	      }
	}
	
	@Override
	public void onResume() {
		if(opencvLoaded)	loadPreferences();
		super.onResume();
	}
	
	
	 @Override
	    public void onRefresh() {
	        Toast.makeText(context, "Loading File-List from Camera", Toast.LENGTH_SHORT).show();
	        new DownloadFileListThread(fileType).start();
	    }
	
	 @Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		 inflater.inflate(R.menu.menu_camera_file_list, menu);
	}	 
	
	
	@Override
	  public boolean onOptionsItemSelected(MenuItem item) {
	    switch (item.getItemId()) {
	    // action with ID action_refresh was selected
	    case R.id.action_download:
	      Toast.makeText(context, "Downloading Data from Camera...", Toast.LENGTH_SHORT)
	          .show();
	      new Thread(new Runnable() {
			
			@Override
			public void run() {
				
				String fileName;
				for (ListItem item : list) {
					fileName = item.path;
					while(currentRunningDownloadThreads>2) {
						try {
							Thread.sleep(100);
						} catch (InterruptedException e) {
							log("Error while sleeping. " + e.toString());
						}						
					}
					String t = new String(fileName);
					
					File f = new File(SJCamUtil.baseFolder + fileName);
					if (f.exists() || f.isDirectory()) {
						log("File does exist or is directory : " + f.getPath());
					} else {
						try {
							Bitmap thumbnail;
							currentRunningDownloadThreads++;
							FileUtils.copyURLToFile(new URL(SJCamUtil.baseURL + fileName), f,
									1000, 1000);
							currentRunningDownloadThreads--;
							log("saved File: " + f.getPath() + " and created thumbnail. " + item.thumbPath);
							
							item.thumbPath = SJCamUtil.extractThumbNail(fileName, fileType);
							item.imgPath = SJCamUtil.baseFolder + fileName;
							getActivity().runOnUiThread(new Runnable() {
								
								@Override
								public void run() {
									adapter.notifyDataSetChanged();						
								}
							});
						} catch (IOException e) {
							log("Error while saving File: " + fileName + " ex: " + e.toString());
						}
					}
					
//					new DownloadSingleFileThread(t, item).start();
					
				}
			}
		}).start();
	      break;
	    // action with ID action_settings was selected
	    case R.id.action_settings:
	      startActivity(new Intent(context, PrefActivity.class));
	      break;
	    case R.id.action_refresh:
	    	new DownloadFileListThread(fileType).start();
	    default:
	      break;
	    }

	    return true;
	  } 

	
	public static void log(String string) {
		Log.e(TAG, string);
	}
	
	//begin zoomStuff
	
	// Hold a reference to the current animator,
    // so that it can be canceled mid-way.
    private Animator mCurrentAnimator;

    // The system "short" animation time duration, in milliseconds. This
    // duration is ideal for subtle animations or animations that occur
    // very frequently.
    private int mShortAnimationDuration;

	
	
    
    private void zoomImageFromThumb(final View thumbView, Bitmap bitmap) {
        // If there's an animation in progress, cancel it
        // immediately and proceed with this one.
        if (mCurrentAnimator != null) {
            mCurrentAnimator.cancel();
        }

        // Load the high-resolution "zoomed-in" image.
        final ImageView expandedImageView = (ImageView) rootView.findViewById(
                R.id.expanded_image);
        expandedImageView.setImageBitmap(bitmap);

        // Calculate the starting and ending bounds for the zoomed-in image.
        // This step involves lots of math. Yay, math.
        final Rect startBounds = new Rect();
        final Rect finalBounds = new Rect();
        final android.graphics.Point globalOffset = new android.graphics.Point();

        // The start bounds are the global visible rectangle of the thumbnail,
        // and the final bounds are the global visible rectangle of the container
        // view. Also set the container view's offset as the origin for the
        // bounds, since that's the origin for the positioning animation
        // properties (X, Y).
       
        thumbView.getGlobalVisibleRect(startBounds);
        rootView.findViewById(R.id.zoom_container)
                .getGlobalVisibleRect(finalBounds, globalOffset);
        startBounds.offset(-globalOffset.x, -globalOffset.y);
        finalBounds.offset(-globalOffset.x, -globalOffset.y);

        // Adjust the start bounds to be the same aspect ratio as the final
        // bounds using the "center crop" technique. This prevents undesirable
        // stretching during the animation. Also calculate the start scaling
        // factor (the end scaling factor is always 1.0).
        float startScale;
        if ((float) finalBounds.width() / finalBounds.height()
                > (float) startBounds.width() / startBounds.height()) {
            // Extend start bounds horizontally
            startScale = (float) startBounds.height() / finalBounds.height();
            float startWidth = startScale * finalBounds.width();
            float deltaWidth = (startWidth - startBounds.width()) / 2;
            startBounds.left -= deltaWidth;
            startBounds.right += deltaWidth;
        } else {
            // Extend start bounds vertically
            startScale = (float) startBounds.width() / finalBounds.width();
            float startHeight = startScale * finalBounds.height();
            float deltaHeight = (startHeight - startBounds.height()) / 2;
            startBounds.top -= deltaHeight;
            startBounds.bottom += deltaHeight;
        }

        // Hide the thumbnail and show the zoomed-in view. When the animation
        // begins, it will position the zoomed-in view in the place of the
        // thumbnail.
//        thumbView.setAlpha(0f);
        expandedImageView.setVisibility(View.VISIBLE);

        // Set the pivot point for SCALE_X and SCALE_Y transformations
        // to the top-left corner of the zoomed-in view (the default
        // is the center of the view).
        expandedImageView.setPivotX(0f);
        expandedImageView.setPivotY(0f);

        // Construct and run the parallel animation of the four translation and
        // scale properties (X, Y, SCALE_X, and SCALE_Y).
        AnimatorSet set = new AnimatorSet();
        set
                .play(ObjectAnimator.ofFloat(expandedImageView, View.X,
                        startBounds.left, finalBounds.left))
                .with(ObjectAnimator.ofFloat(expandedImageView, View.Y,
                        startBounds.top, finalBounds.top))
                .with(ObjectAnimator.ofFloat(expandedImageView, View.SCALE_X,
                startScale, 1f)).with(ObjectAnimator.ofFloat(expandedImageView,
                        View.SCALE_Y, startScale, 1f));
        set.setDuration(mShortAnimationDuration);
        set.setInterpolator(new DecelerateInterpolator());
        set.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mCurrentAnimator = null;
            }

            @Override
            public void onAnimationCancel(Animator animation) {
                mCurrentAnimator = null;
            }
        });
        set.start();
        mCurrentAnimator = set;

        // Upon clicking the zoomed-in image, it should zoom back down
        // to the original bounds and show the thumbnail instead of
        // the expanded image.
        final float startScaleFinal = startScale;
        expandedImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mCurrentAnimator != null) {
                    mCurrentAnimator.cancel();
                }

                // Animate the four positioning/sizing properties in parallel,
                // back to their original values.
                AnimatorSet set = new AnimatorSet();
                set.play(ObjectAnimator
                            .ofFloat(expandedImageView, View.X, startBounds.left))
                            .with(ObjectAnimator
                                    .ofFloat(expandedImageView, 
                                            View.Y,startBounds.top))
                            .with(ObjectAnimator
                                    .ofFloat(expandedImageView, 
                                            View.SCALE_X, startScaleFinal))
                            .with(ObjectAnimator
                                    .ofFloat(expandedImageView, 
                                            View.SCALE_Y, startScaleFinal));
                set.setDuration(mShortAnimationDuration);
                set.setInterpolator(new DecelerateInterpolator());
                set.addListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        thumbView.setAlpha(1f);
                        expandedImageView.setVisibility(View.GONE);
                        mCurrentAnimator = null;
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {
                        thumbView.setAlpha(1f);
                        expandedImageView.setVisibility(View.GONE);
                        mCurrentAnimator = null;
                    }
                });
                set.start();
                mCurrentAnimator = set;
            }
        });
    }
	
	private class StableArrayAdapter extends ArrayAdapter<ListItem> {

		private final Context context;
//	    HashMap<String, Integer> mIdMap = new HashMap<String, Integer>();

	    public StableArrayAdapter(Context context, int textViewResourceId,
	        List<ListItem> objects) {
	      super(context, textViewResourceId, objects);
	      this.context = context;
//	      for (int i = 0; i < objects.size(); ++i) {
//	        mIdMap.put(objects.get(i), i);
//	      }
	    }
	    
	    @Override
	    public void add(ListItem object) {	    	
	    	super.add(object);
	    	
//	    	mIdMap.put(object, mIdMap.size());
	    }
	    
	    @Override
	    public View getView(int position, View convertView, ViewGroup parent) {
	    	ListItem item = getItem(position);
	      LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	      View rowView = inflater.inflate(R.layout.row_layout, parent, false);
	      TextView txtSecondLine = (TextView) rowView.findViewById(R.id.secondLine);
	      ImageView imageView = (ImageView) rowView.findViewById(R.id.thumb);
	      imageView.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				log("on image click");
				String imgPath = ((ImageView)v).getContentDescription().toString();
				log("content-description: " + imgPath);
				if(fileType == FileType.IMAGES) {
					zoomImageFromThumb(v, BitmapFactory.decodeFile(imgPath));					
				} else {
					Intent intent = new Intent(BaseFragment.this.context, VideoPlayerActivity.class);
					Bundle b = new Bundle();
					b.putString(VideoPlayerActivity.KEY_VIDEO_PATH, imgPath);
					intent.putExtras(b);
					startActivity(intent);
				}
			}
		});
//	      imageView.setClickable(false);
	      txtSecondLine.setText(item.path);
	      if(item.imgPath != null && item.imgPath != "") {
	    	  imageView.setContentDescription(item.imgPath);
	      }
	      if(item.thumbPath != null && item.thumbPath != "") {
	    	  imageView.setImageBitmap(BitmapFactory.decodeFile(item.thumbPath));	    	
	      }

	      return rowView;
	    }
	    
	    @Override
	    public boolean isEnabled(int position)
	    {
	        return true;
	    }

	  }
	/*
	class ProcessImageThread extends Thread {
		
		public String item;
		
		public ProcessImageThread(String item) {
			super();
			this.item = item;
		}

		@Override
		public void run() {
			String fileName = SJCamUtil.baseFolder + item;
			log("processing " + fileName);
			Mat mat = Highgui.imread(fileName);
			  
			long startTime = System.currentTimeMillis();
			Mat gray = new Mat(mat.rows(), mat.cols(), CvType.CV_8UC1);
			Imgproc.cvtColor(mat, gray, 11);
//			Imgproc.pyrDown(gray, gray);
			Log.e(TAG, "pyred gray: " + gray.rows() + " , " + gray.cols());
			byte[] imgBytes = new byte[(int) gray.total()];
			gray.get(0, 0, imgBytes);
			byte[] foundIds = new byte[21];
			for(int i=0; i<foundIds.length; i++) {
				foundIds[i] = Byte.MAX_VALUE;
			}
			double[] cornerPoints = new double[80];
			double[] H = new double[45];	//space for storing 5 Homographys (9 values = 3x3 matrices)
			log("jni findTags method: " + (new HelloJni().findTags(imgBytes, gray.cols(), gray.rows(), foundIds, cornerPoints, H)));
			long endTime = System.currentTimeMillis();
			log("Found TagIds: " + Arrays.toString(foundIds));
			log("CornerPoints: " + Arrays.toString(cornerPoints));
			log("Homography: " + Arrays.toString(H));
			log("Time to call JNI findTags method: " + (endTime-startTime));
			summendTimesForProcessing = summendTimesForProcessing + (endTime-startTime);
			frameCount++;
			Log.e(TAG, "Time to process Imag for finding Tags: " + (endTime-startTime));
			Log.e(TAG, "Current Summed Time for processing all Tags: " + summendTimesForProcessing + " frameCount: " + frameCount + "  avgProcTimePerFrame: " + summendTimesForProcessing/frameCount);
		
			if(foundIds[0]==(Byte.MAX_VALUE)) {
				return;
			}
			
			
			Mat cameraMatrix = new Mat(3,3,CvType.CV_32FC1);
			MatOfDouble distorsionMatrix = new MatOfDouble();
			
			//sjcam calib
			cameraMatrix.put(0, 0, 1001.7792, 0, 962.3616,
			  0, 1012.7232000000001, 561.27600000000007,
			  0, 0, 1);			
			
			double[] coeffArray = new double[]{-0.31331,
					0.12965,
			  0.00073,
			  -0.00022,
			  -0.02912};
			
			distorsionMatrix.fromArray(coeffArray);	
			
			Vector<Point> points = new Vector<Point>();
			points.add(new Point(cornerPoints[0] , cornerPoints[1]));		//1m
			points.add(new Point(cornerPoints[2] , cornerPoints[3]));
			points.add(new Point(cornerPoints[4] , cornerPoints[5]));
			points.add(new Point(cornerPoints[6] , cornerPoints[7]));
					
			float[] hf = new float[]{(float) H[0],(float) H[1],(float) H[2],(float) H[3],(float) H[4],(float) H[5], (float) H[6],(float) H[7],(float) H[8]};
			log("homo f: " + Arrays.toString(hf));
			PositionCalculation posCalc = new PositionCalculation(points);
			Marker m = markerMap.get(foundIds[0]);
			if(m==null) {
				log("Marker with id " + foundIds[0] + " couldnt be found in the MarkerMap");
				return;
			}
			posCalc.calculateExtrinsics(cameraMatrix, distorsionMatrix, m.size);
			final Position p1 = posCalc.calculateCameraPos();
			log("calculated Postion: -> " + p1.toString());
			
			posCalc = new PositionCalculation(hf);
			posCalc.calculateExtrinsics(cameraMatrix, distorsionMatrix, m.size);
			final Position p2 = posCalc.calculateCameraPos();
			log("calculated Postion: -> " + p2.toString());
			
			getActivity().runOnUiThread(new Runnable() {
				
				@Override
				public void run() {
					pos1X.setText("x: " + String.format("%.3f",p1.x));
					pos1Y.setText("y: " + String.format("%.3f",p1.y));
					pos1Z.setText("z: " + String.format("%.3f",p1.z));
					
					pos1AngleX.setText("angle x: " + p1.angleX + "�");
					pos1AngleY.setText("angle y: " + p1.angleY + "�");
					pos1AngleZ.setText("angle z: " + p1.angleZ + "�");
					
					pos2X.setText("x: " + String.format("%.3f",p2.x));
					pos2Y.setText("y: " + String.format("%.3f",p2.y));
					pos2Z.setText("z: " + String.format("%.3f",p2.z));
					
					pos2AngleX.setText("angle x: " + p2.angleX + "�");
					pos2AngleY.setText("angle y: " + p2.angleY + "�");
					pos2AngleZ.setText("angle z: " + p2.angleZ + "�");
					
					sendPositionOverBT(new Position(p1.x, p1.y, p1.z, 0));
				}
			});
			
		}
	}
	*/
	class DownloadFileListThread extends Thread {
		
		private FileType fileType;
		
		public DownloadFileListThread(FileType fileType) {
			this.fileType = fileType;
		}
		
		@Override
		public void run() {
			final Vector<String> fileNames = SJCamUtil.loadFileNamesFromSJCam(SJCamUtil.baseFolder,
					fileType);
			getActivity().runOnUiThread(new Runnable() {
				
				@Override
				public void run() {
					if(fileNames == null || fileNames.size()<1) {
						log("Couldnt read fileNames from camera.");
						logToast("Couldnt read fileNames from camera.");
						mSwipeRefreshLayout.setRefreshing(false);
						return;
					}
					for(String path: fileNames) {
						ListItem currentNewItem = new ListItem(path, null, null, null);		
						if(!list.contains(currentNewItem)) {
							list.add(currentNewItem);
						}
					}
					
					adapter.notifyDataSetChanged();
					mSwipeRefreshLayout.setRefreshing(false);
				}
			});
		}

	}
	
	class LoadFilesFromSDCardThread extends Thread {
		@Override
		public void run() {
			super.run();
			
			final Collection<String> files = SJCamUtil.loadExistingFilesFromStorage(FileType.IMAGES);
			
			if(files == null || files.size()<1) {
				log("Couldnt read fileList from SDCArd from camera.");
				return;
			}
			getActivity().runOnUiThread(new Runnable() {
				
				@Override
				public void run() {
					for(String path: files) {
						list.add(new ListItem(path, null, null, null));						
					}
					adapter.notifyDataSetChanged();
				}
			});
		}
	}
	
	class RestorStateThread extends Thread {
		@Override
		public void run() {
			super.run();
			
			final Vector<ListItem> tempVector = SJCamUtil.restoreState(fileType);
			
			getActivity().runOnUiThread(new Runnable() {
				
				@Override
				public void run() {
					list.addAll(tempVector);
					adapter.notifyDataSetChanged();
				}
			});
		}
	}
	
	
	static int currentRunningDownloadThreads = 0;
	
	class DownloadSingleFileThread extends Thread {
		
		private String fileName;
		private ListItem item;
		
		public DownloadSingleFileThread(String fileName, ListItem item) {
			this.fileName = fileName;
			this.item = item;
		}
		
		@Override
		public void run() {
			
		}
	}
	

	private void logToast(String logMessage) {
		Toast.makeText(context, logMessage, Toast.LENGTH_LONG).show();;
	}
	
	// usually, subclasses of AsyncTask are declared inside the activity class.
		// that way, you can easily modify the UI thread from here
		private class DownloadTask extends AsyncTask<String, Integer, String> {

		    private Context context;
		    private PowerManager.WakeLock mWakeLock;
		    private ListItem item;

		    public DownloadTask(Context context, ListItem item) {
		        this.context = context;
		        this.item = item;
		    }

		    @Override
		    protected String doInBackground(String... params) {
		        InputStream input = null;
		        OutputStream output = null;
		        HttpURLConnection connection = null;
		        try {
		            URL url = new URL(params[0]);
		            connection = (HttpURLConnection) url.openConnection();
		            connection.connect();

		            // expect HTTP 200 OK, so we don't mistakenly save error report
		            // instead of the file
		            if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
		                return "Server returned HTTP " + connection.getResponseCode()
		                        + " " + connection.getResponseMessage();
		            }

		            // this will be useful to display download percentage
		            // might be -1: server did not report the length
		            int fileLength = connection.getContentLength();

		            // download the file		          
		            String folderPath = "";
					if(fileType == FileType.IMAGES) folderPath = SJCamUtil.baseFolder + "/DCIM/PHOTO";
					else if(fileType == FileType.VIDEOS) folderPath = SJCamUtil.baseFolder + "/DCIM/MOVIE";
					File mFolder = new File(folderPath);
		            if (!mFolder.exists()) {
		                mFolder.mkdirs();
		            }
		            input = connection.getInputStream();
		            output = new FileOutputStream(params[1]);

		            
		            byte data[] = new byte[4096];
		            long total = 0;
		            int count;
		            while ((count = input.read(data)) != -1) {
		                // allow canceling with back button
		                if (isCancelled()) {
		                    input.close();
		                    return null;
		                }
		                total += count;
		                // publishing the progress....
		                if (fileLength > 0) // only if total length is known
		                    publishProgress((int) (total * 100 / fileLength));
		                output.write(data, 0, count);
		            }
		            
					
					item.thumbPath = SJCamUtil.extractThumbNail(item.path, fileType);
					item.imgPath = SJCamUtil.baseFolder + item.path;
					log("saved File: " + params[1] + " and created thumbnail. " + item.thumbPath);
		        } catch (Exception e) {
		            return e.toString();
		        } finally {
		            try {
		                if (output != null)
		                    output.close();
		                if (input != null)
		                    input.close();
		            } catch (IOException ignored) {
		            }

		            if (connection != null)
		                connection.disconnect();
		        }
		        return null;
		    }
		    
		    @Override
		    protected void onPreExecute() {
		        super.onPreExecute();
		        // take CPU lock to prevent CPU from going off if the user 
		        // presses the power button during download
		        PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
		        mWakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
		             getClass().getName());
		        mWakeLock.acquire();
		        mProgressDialog.show();
		    }

		    @Override
		    protected void onProgressUpdate(Integer... progress) {
		        super.onProgressUpdate(progress);
		        // if we get here, length is known, now set indeterminate to false
		        mProgressDialog.setIndeterminate(false);
		        mProgressDialog.setMax(100);
		        mProgressDialog.setProgress(progress[0]);
		    }

		    @Override
		    protected void onPostExecute(String result) {
		        mWakeLock.release();
		        mProgressDialog.dismiss();
		        if (result != null)
		            Toast.makeText(context,"Download error: "+result, Toast.LENGTH_LONG).show();
		        else
		            Toast.makeText(context,"File downloaded", Toast.LENGTH_SHORT).show();
		        adapter.notifyDataSetChanged();
		    }	    
		}


		@Override
		public void onNewPositionCalculated(Position p) {
			pos1X.setText("x: " + String.format("%.3f",p.x));
			pos1Y.setText("y: " + String.format("%.3f",p.y));
			pos1Z.setText("z: " + String.format("%.3f",p.z));
			
			pos1AngleX.setText("angle x: " + p.angleX + "�");
			pos1AngleY.setText("angle y: " + p.angleY + "�");
			pos1AngleZ.setText("angle z: " + p.angleZ + "�");
			
			
			
			sendPositionOverBT(new Position(p.x, p.y, p.z, 0));
		}

		@Override
		public void onCalculationFinished(int numOfProcessedFrames) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onBadFrameReceived() {
			pos1X.setText("x: nop");
			pos1Y.setText("y: nop");
			pos1Z.setText("z: nop");
		}
	
}
