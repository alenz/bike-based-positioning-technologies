package com.example.apriltagandroid;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.android.Utils;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.highgui.Highgui;
import org.opencv.highgui.VideoCapture;
import org.opencv.imgproc.Imgproc;

import sjcam.util.SJCamUtil;
import umich.cem.live.marvigator.Native;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.util.SparseArray;
import android.view.Menu;
import android.view.MenuItem;
import android.view.TextureView;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import at.fhooe.mcm.mdpm.data.Marker;
import at.fhooe.mcm.mdpm.data.MarkerUtil;
import at.fhooe.mcm.mdpm.data.Position;
import at.fhooe.mcm.mdpm.data.calculation.CameraParameters;
import at.fhooe.mcm.mdpm.data.calculation.PositionCalculator;
import at.fhooe.mcm.mdpm.data.view.PositionUpdateListener;
import at.fhooe.mcm.mdpm.services.LocalBluetoothService;
import at.fhooe.mcm.mdpm.videotest.ExtractMpegFrames;
import at.fhooe.mcm.mdpm.videotest.ExtractMpegFramesTest1_OLDVersion;

/**
 * {@link Activity} for Processing videos with the developed Marker-based-Positioning-Method.
 * It is responsible for setting up the whole process and start then an {@link ExtractMpegFrames}-Instance
 * with the proper {@link PositionCalculator}.
 * 
 * It gives the user some information of ..
 *  + the processed frameNr
 *  + how many positions have been successfully extracted
 *  + which video will be processed
 *  ...
 *  
 *  TODO: add binding to the {@link LocalBluetoothService} to send calculated Positions to tablet (BT-Client).
 * @author Alexander
 *
 */
public class VideoProcessingActivity extends Activity implements PositionUpdateListener{

	public static final String TAG = "TAG";
	
	public static final String INTENT_EXTRA_FILE_PATH = "INTENT_EXTRA_FILE_PATH";
	public static final String INTENT_EXTRA_CAMERA = "INTENT_EXTRA_CAMERA";
	
	private String filePath = null;
	private PositionCalculator posCalculator = null;
	private int camera = -1;
	private SparseArray<Marker> trackSetup = null;
	
	private TextView txtView, txtViewProgress, txtProgressFrames;
	private TextView txtCurrentPosX, txtCurrentPosY, txtCurrentPosZ;
	private ProgressBar progessbar;
	private Button btnSave;

	private int posCounter = 0;
	private int frameCounter = 0;
	
	private double duration = -1;
	private int numberOfFramesInTotal = -1;
	private int numberOfFramesToSkip = 1;		// every x frame will be taken to calc a position
	
	private int currentProcessedFrame = -1;
	private long currentProcessedTime = -1;
	
	private long startTime = 0;

    @Override
    public void onResume()
    {
        super.onResume();
        
    }
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main_old);
		txtView = (TextView) findViewById(R.id.txt_video_processing_text);
		txtViewProgress = (TextView) findViewById(R.id.txt_video_processing_Progress_text);
		txtProgressFrames = (TextView) findViewById(R.id.txt_video_processing_Progress_frames_text);
		txtCurrentPosX = (TextView) findViewById(R.id.txt_video_processing_current_position_x);
		txtCurrentPosY = (TextView) findViewById(R.id.txt_video_processing_current_position_y);
		txtCurrentPosZ = (TextView) findViewById(R.id.txt_video_processing_current_position_z);
		progessbar = (ProgressBar) findViewById(R.id.progressBar1_video_proecessing);
		btnSave = (Button) findViewById(R.id.btn_savePositions);
		btnSave.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// save to File
				posCalculator.savePositions();
				
				// send E-Mail
				String formattedTime = android.text.format.DateFormat.format("yyyy-MM-dd hh:mm:ss", new java.util.Date(startTime)).toString();
				
				
				Intent intent = new Intent(Intent.ACTION_SEND_MULTIPLE);
				intent.setType("text/plain");
				intent.putExtra(Intent.EXTRA_SUBJECT, "PosCalculations Results " + formattedTime + "  (" + filePath + ")");
				intent.putExtra(Intent.EXTRA_TEXT, "PosCalculations Results from file: " + filePath);
				intent.putExtra(Intent.EXTRA_EMAIL, new String[]{"ateamsolutions@gmx.at"});
				                                
				ArrayList<Uri> uris = new ArrayList<Uri>();
				uris.add(Uri.fromFile(new File(posCalculator.pathToSaveCalculatedPositions)));
				uris.add(Uri.fromFile(new File(posCalculator.pathToSaveMultiMarkerPositions)));

				intent.putParcelableArrayListExtra(Intent.EXTRA_STREAM, uris); 
				startActivity(Intent.createChooser(intent, "Send mail"));
			}
		});
		loadPreferences();
		
		filePath = getIntent().getStringExtra(INTENT_EXTRA_FILE_PATH);	
		
		MediaMetadataRetriever metaRetriever = new MediaMetadataRetriever();
        metaRetriever.setDataSource(filePath);
        duration = Double.parseDouble(metaRetriever.extractMetadata(android.media.MediaMetadataRetriever.METADATA_KEY_DURATION));
        numberOfFramesInTotal = (int) (duration*30d/1000d);
		log("Duration of the Video: " + duration/1000d + " * 30frames/sec = " + numberOfFramesInTotal + " frames total");
		
		
		progessbar.setMax(numberOfFramesInTotal);
        
		txtView.setText("Processing: " + filePath + "  with camera " + camera);
		txtProgressFrames.setText("Processed " + frameCounter*numberOfFramesToSkip + " of " + numberOfFramesInTotal + " frames in total.");
		
	}
	private void loadPreferences() {
		SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
		camera = new Integer(sharedPreferences.getString(getString(R.string.pref_key_camera), "-1"));
		trackSetup = MarkerUtil.getUserPreferredMarkerSetup(this);
		startTime = System.currentTimeMillis();
		switch(camera) {
		case 1:
			posCalculator = new PositionCalculator(SJCamUtil.baseFolder + "/calculatedPositions_movie_sjcam_"+startTime+".json",  SJCamUtil.baseFolder + "/calculatedMultiMarkerPositions_movie_sjcam_"+startTime+".json", CameraParameters.createSJCamParameters(), this, trackSetup);
			break;
		case 2:
			posCalculator = new PositionCalculator(SJCamUtil.baseFolder + "/calculatedPositions_movie_digicam_"+startTime+".json",SJCamUtil.baseFolder + "/calculatedMultiMarkerPositions_movie_digicam_"+startTime+".json", CameraParameters.createDigiCamPhotoParameters(), this, trackSetup);
			break;
		case 3:
			posCalculator = new PositionCalculator(SJCamUtil.baseFolder + "/calculatedPositions_movie_digicam16_9_"+startTime+".json", SJCamUtil.baseFolder + "/calculatedMultiMarkerPositions_movie_digicam16_9_"+startTime+".json" , CameraParameters.createDigiCamVideoParameters(), this, trackSetup);
			break;
		case 4:
			posCalculator = new PositionCalculator(SJCamUtil.baseFolder + "/calculatedPositions_movie_nexus5_"+startTime+".json" , SJCamUtil.baseFolder + "/calculatedMultiMarkerPositions_movie_nexus5_"+startTime+".json", CameraParameters.createNexus5PhotoParameters(), this, trackSetup);
			break;
		}
		
		log("Slected Camera from preferences: " + camera);
	}
	
	private void log(String string) {
		Log.e(TAG, string);
	}

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		 new Thread(new Runnable() {
 			
 			@Override
 			public void run() {
 				// TODO Auto-generated method stub
 				ExtractMpegFrames extractor = new ExtractMpegFrames();
 				try {
 					extractor.startExtractMpegFrames(posCalculator, filePath, numberOfFramesToSkip);
 				} catch (Throwable e) {
 					Log.e(TAG, "Exception:::" + e.toString());
 				}
 				
 			}
 		}).start();
	}

	@Override
	public void onNewPositionCalculated(Position p) {
		posCounter++;
		frameCounter++;
		
		txtCurrentPosX.setText("x: " + String.format("%.3f",p.x));
		txtCurrentPosY.setText("y: " + String.format("%.3f",p.y));
		txtCurrentPosZ.setText("z: " + String.format("%.3f",p.z));
		currentProcessedFrame = p.frameNr;
		currentProcessedTime = p.time;
		updateProgress();
	}

	@Override
	public void onCalculationFinished(int nrOfProcessedFrames) {
		progessbar.setProgress(nrOfProcessedFrames);
		btnSave.setVisibility(View.VISIBLE);
	}

	@Override
	public void onBadFrameReceived() {
		frameCounter++;
		updateProgress();
	}
	
	public void updateProgress() {
		progessbar.setProgress(frameCounter*numberOfFramesToSkip);
		txtViewProgress.setText("Calculated " + posCounter + " positions.");
		txtProgressFrames.setText("Processed " + frameCounter*numberOfFramesToSkip + " of " + numberOfFramesInTotal + " frames in total." + "\nCurrent processed FrameNr: " + currentProcessedFrame + " (" + currentProcessedTime + "ms)");
		
	}
	

	
	/*		try {
	// TODO Auto-generated method stub
	
	long t1 = System.currentTimeMillis();
	Mat mat = Utils.loadResource(MainActivity.this, R.drawable.m1);
	Mat gray = new Mat(mat.rows(), mat.cols(), CvType.CV_8UC1);
	Log.e(TAG, "gray: " + gray.rows() + " , " + gray.cols());
	Imgproc.cvtColor(mat, gray, 11);
	Imgproc.pyrDown(gray, gray);
	Log.e(TAG, "gray pyredDown: " + gray.rows() + " , " + gray.cols());

	Native.SetTagFamily(Native.TAG25H9);
//boolean flag=Highgui.imwrite("/sdcard/mat.jpg", mat); 
//flag=Highgui.imwrite("/sdcard/gray.jpg", gray); 
	long t2 = System.currentTimeMillis();
	Log.e(TAG, "Time for loading and graying the image: " + (t2-t1));
	
	long startTime = System.currentTimeMillis();
	Native.FindTag(gray.getNativeObjAddr(), H, ids);
	Native.FindTag(gray.getNativeObjAddr(), H, ids);
	Native.FindTag(gray.getNativeObjAddr(), H, ids);
	Native.FindTag(gray.getNativeObjAddr(), H, ids);
	Native.FindTag(gray.getNativeObjAddr(), H, ids);
	long endTime = System.currentTimeMillis();
	Log.e(TAG, "Time to process Imag for finding Tag-Homography: " + (endTime-startTime));
	
	Log.e(TAG, "H: " + Arrays.toString(H));
	Log.e(TAG, "Ids: " + Arrays.toString(ids));
	
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}*/
	
}
