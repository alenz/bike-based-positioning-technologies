package com.example.hellojni;

/**
 * Class for interacting with the native library for processing images to find AprilTag-Marker in them.
 * I ported the official C-library with JNI for using it in Java.
 * (Was extended from a simply HelloWorld for JNI-Programming so therefore the name ;) )
 * 
 * @author Alexander
 *
 */
public class HelloJni
{
	 public HelloJni()
	    {
	    }

	 
	 /**
	  * Method to get the Ids, cornerPoints and Homography of all found AprilTag-Marker in a given Image.
	  * The arrays must be created before the method call, the C-Part only fills the values in them.
	  * So they must be big enough to fit for multiple found markers.
	  * 
	  * @param arr - image data array
	  * @param width - width of the image
	  * @param height - height of the image
	  * @param foundIds - array of found ids 
	  * @param cornerPoints - corner points of all found tags (8 consecutive values always belongs to one marker - 4 points each with a x and y value)
	  * @param H - Homography arrays of all found tags
	  * @return - number of found ids
	  */
	 public native int findTags(byte[] arr, int width, int height, byte[] foundIds, double[] cornerPoints, double[] H);
	 
    
	 
	 
	 
	 
	 
	 
	 
	 
	 /* A native method that is implemented by the
     * 'hello-jni' native library, which is packaged
     * with this application.
     */
    public static native String  stringFromJNI();

    /* This is another native method declaration that is *not*
     * implemented by 'hello-jni'. This is simply to show that
     * you can declare as many native methods in your Java code
     * as you want, their implementation is searched in the
     * currently loaded native libraries only the first time
     * you call them.
     *
     * Trying to call this function will result in a
     * java.lang.UnsatisfiedLinkError exception !
     */
    public static native int  unimplementedStringFromJNI(byte[] arr);

    
    public static native int darken(byte[] arr, int width, int height);
    
    
    /* this is used to load the 'hello-jni' library on application
     * startup. The library has already been unpacked into
     * /data/data/com.example.hellojni/lib/libhello-jni.so at
     * installation time by the package manager.
     */
//    static {
//        System.loadLibrary("hello-jni");
//    }
}