package at.fhooe.mcm.mdpm.viewer.view;

/**
 * The Position class represents a 3D-Position with x,y,z-Coordinates
 * It includes also for further calculation and improvements of the recorded positions, the frameNr, time
 * and its angles (x,y,z) in respect to the Marker (should be probably in respect to the track-coordinate system).
 * @author Alexander
 *
 */
public class Position {

	public double x;
	public double y;
	public double z;
	
	public int frameNr;
	public long time;
	
	public int angleX, angleY, angleZ;	
	
	public Position() {
		x = -33;
		y = -33;
		z = -33;
		frameNr = -1;
		time = -1;
	}
	
//	public Position(int x, int y, int z) {
//		this.x = x;
//		this.y = y;
//		this.z = z;
//	}
//	
//	public Position(float x, float y, float z) {
//		this.x = x;
//		this.y = y;
//		this.z = z;
//	}

	public Position(double x, double y, double z, int frameNr) {
		this.x = x;
		this.y = y;
		this.z = z;
		this.frameNr = frameNr;
	}
	
	public String toStringShort() {
		return "Position:: x=" + x + " , y=" + y + " , z=" + z;
	}
	
	public String toString() {
		return "Position:: x=" + x + " , y=" + y + " , z=" + z + "  |||    AngleX=" + angleX + " , AngleY=" + angleY + " , AngleZ=" + angleZ;
	}
	
	public Position(double x, double y, double z, double angleX, double angleY, double angleZ) {
		this.x = x;
		this.y = y;
		this.z = z;
		this.angleX = (int) angleX;
		this.angleY = (int) angleY;
		this.angleZ = (int) angleZ;
	}	
	
	
	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}

	public double getZ() {
		return z;
	}

	public void setZ(double z) {
		this.z = z;
	}
}
