package de.tum.ei.vmi.bluetoothsrwc;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.UUID;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;
import at.fhooe.mcm.mdpm.viewer.view.Position;
import at.fhooe.mcm.mdpm.viewer.view.PositionView;

import com.google.gson.Gson;

public class MainActivity extends Activity {
	
	public static final UUID MY_UUID = UUID
			.fromString("f895eaf0-867f-11e3-baa7-0800200c9a66");
	
	private static final int SOCKET_CONNECTED = 1;
	private static final int DATA_RECEIVED = 2;
	
	private static final int REQEUEST_ENABLE_BT = 3;
	private static final int LIST_DEVICE = 4;

	private static final String TAG = "MainActivityPositionViewer";
	
	private TextView mTvStatus;
	private TextView mTvDeviceName;
	private TextView mTvData;
	private Button mBtnList;
	private Button mBtnMakeDiscoverable;
	
	private BluetoothAdapter mBluetoothAdapter = null;
	
	private ConnectionThread mBluetoothConnection;
	
	
	private PositionView posView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		mTvStatus = (TextView) findViewById(R.id.tvStatus);
		mTvDeviceName = (TextView) findViewById(R.id.tvDeviceName);
		mTvData = (TextView) findViewById(R.id.tvData);
		mBtnList = (Button) findViewById(R.id.btnList);
		mBtnMakeDiscoverable = (Button) findViewById(R.id.btnMakeDiscoverable);
		
		mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
		
		if (mBluetoothAdapter == null) {
			Toast.makeText(this, "Bluetooth is not available", Toast.LENGTH_LONG)
				.show();
			finish();
		} else {
			int btState = mBluetoothAdapter.getState();
			
			if (btState == BluetoothAdapter.STATE_OFF) {
				mTvStatus.setText("Bluetooth is off");
				if (!mBluetoothAdapter.isEnabled()) {
					Intent enableIntent = new Intent(
							BluetoothAdapter.ACTION_REQUEST_ENABLE);
					startActivityForResult(enableIntent, REQEUEST_ENABLE_BT);
				}
			} else if (btState == BluetoothAdapter.STATE_ON) {
				initializeBluetooth();
			}
		}
		
		mBtnMakeDiscoverable.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent discoverableIntent = new Intent(
						BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
				discoverableIntent.putExtra(
						BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION,
						300);
				startActivity(discoverableIntent);
			}
		});
		
		mBtnList.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent discoverintent = new Intent(
						MainActivity.this, ListActivity.class);
				startActivityForResult(discoverintent, LIST_DEVICE);
			}
		});
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode,
			Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		
		switch (requestCode) {
		case REQEUEST_ENABLE_BT:
			if (resultCode == RESULT_OK) {
				initializeBluetooth();
			} else if (resultCode == RESULT_CANCELED) {
				Toast.makeText(this, "Bluetooth is required", Toast.LENGTH_LONG)
					.show();
				finish();
			}
			break;
		case LIST_DEVICE:
			if (resultCode == RESULT_OK) {
				String address = data.getStringExtra(
						ListActivity.EXTRA_DEVICE_ADDRESS);
				Toast.makeText(this, "Connect to " + address, Toast.LENGTH_SHORT)
				.show();
				new ConnectThread(address, mHandler).start();
			}
			break;
		}
	}

	private void initializeBluetooth() {
		mTvStatus.setText("Bluetooth is on");
		mTvDeviceName.setText("My device name: " +
					mBluetoothAdapter.getName() + " (" +
					mBluetoothAdapter.getAddress() + ")");
		new AcceptThread(mHandler).start();
	}
	
	private Gson gson = new Gson();
	
	private Button btnChangeAxisBtn;
	private Button btnClearHistory;
	private Button btnZoomIn, btnZoomOut;
	
	private Handler mHandler = new Handler(new Handler.Callback() {
		
		@Override
		public boolean handleMessage(Message msg) {
			switch (msg.what) {
			case SOCKET_CONNECTED:
				mBluetoothConnection = (ConnectionThread) msg.obj;
				BluetoothDevice device = mBluetoothConnection.mmBluetoothSocket
						.getRemoteDevice();
				mTvStatus.setText("Connected to " + device.getName() +
						" (" + device.getAddress() + ")");
				String hello = "Hello from " + mBluetoothAdapter.getName();
				
				FrameLayout frameLayout = new FrameLayout(MainActivity.this);
				LayoutInflater inflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			    View buttonView = inflater.inflate(R.layout.position_view_buttons_layout, null);
				btnChangeAxisBtn = (Button) buttonView.findViewById(R.id.btn_changeShownAxis);
				btnChangeAxisBtn.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						int currentAxisState = MainActivity.this.posView.changeAxisDrawingMode();
						if(currentAxisState == PositionView.AXIS_DRAWING_MODE_X_Y) {
							MainActivity.this.btnChangeAxisBtn.setText("Axis x, y");							
						} else {
							MainActivity.this.btnChangeAxisBtn.setText("Axis x, z");	
						}
					}
				});
				btnClearHistory = (Button) buttonView.findViewById(R.id.btn_clearPositionHistory);
			    btnClearHistory.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						MainActivity.this.posView.clearPositionHistory();
					}
				});			    
			    btnZoomIn = (Button) buttonView.findViewById(R.id.btn_zoomIn);
			    btnZoomIn.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						MainActivity.this.posView.zoomIn();
					}
				});		
			    btnZoomOut = (Button) buttonView.findViewById(R.id.btn_zoomOut);
			    btnZoomOut.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						MainActivity.this.posView.zoomOut();
					}
				});	
				posView = new PositionView(MainActivity.this, null);
				
				
				frameLayout.addView(posView);
				frameLayout.addView(buttonView);
				setContentView(frameLayout);
				mBluetoothConnection.write(hello.getBytes());
				addNewMessage(mBluetoothAdapter.getName(), hello);
				break;
			case DATA_RECEIVED:
				String data = (String) msg.obj;
				
				Position pos = gson.fromJson(data, Position.class);
				posView.addCurrentPosition(pos);
				addNewMessage(mBluetoothConnection.mmBluetoothSocket
						.getRemoteDevice().getName()
						, data);
			}
			return true;
		}
	});
	
	private void addNewMessage(String name, String message) {
		Log.e(TAG, name + ": " + message);
		mTvData.append(name + ": " + message + "\n");
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	class AcceptThread extends Thread {
		private final Handler mmHandler;
		private BluetoothServerSocket mmServerSocket;
		private BluetoothSocket mmSocket = null;
		
		public AcceptThread(Handler handler) {
			mmHandler = handler;
			try {
				mmServerSocket = mBluetoothAdapter
						.listenUsingRfcommWithServiceRecord
						("Bluetooth demo", MY_UUID);
			} catch (IOException e) {
				Log.e(TAG, "Error in AcceptThreadConstr: " + e.toString());
			}
		}
		
		public void run() {
			while (true) {
				try {
					mmSocket = mmServerSocket.accept();
					ConnectionThread conn = new ConnectionThread
							(mmSocket, mmHandler);
					mmHandler.obtainMessage(SOCKET_CONNECTED, conn)
						.sendToTarget();
					conn.start();
					mmServerSocket.close();
					break;
				} catch (IOException e) {
					Log.e(TAG, "Error in AcceptThread: " + e.toString());
				}
			}
		}
	}
	
	public class ConnectionThread extends Thread {
		BluetoothSocket mmBluetoothSocket;
		private final Handler mmHandler;
		private InputStream mmInStream;
		private OutputStream mmOutStream;
		
		public ConnectionThread(BluetoothSocket socket, Handler handler) {
			super();
			mmBluetoothSocket = socket;
			mmHandler = handler;
			try {
				mmInStream = mmBluetoothSocket.getInputStream();
				mmOutStream = mmBluetoothSocket.getOutputStream();
			} catch (IOException e) {
				Log.e(TAG, "Error in ConnectionThreadConstr: " + e.toString());
			}
		}
		
		@Override
		public void run() {
			byte[] buffer = new byte[1024];
			int len;
			while (true) {
				try {
					len = mmInStream.read(buffer);
					String data = new String(buffer, 0, len);
					mmHandler.obtainMessage(DATA_RECEIVED, data)
						.sendToTarget();
				} catch (IOException e) {
					Log.e(TAG, "Error in ConnectionThread.Receiving: " + e.toString());
				}
			}
		}
		
		public void write(byte[] bytes) {
			try {
				mmOutStream.write(bytes);
			} catch (IOException e) {
				Log.e(TAG, "Error in ConnectionThread-Writing: " + e.toString());
			}
		}
	}
	
	public class ConnectThread extends Thread {
		private BluetoothSocket mmBluetoothSocket;
		private final BluetoothDevice mmDevice;
		private final Handler mmHandler;
		
		public ConnectThread(String address, Handler handler) {
			mmHandler = handler;
			mmDevice = mBluetoothAdapter.getRemoteDevice(address);
			try {
				mmBluetoothSocket = mmDevice
						.createRfcommSocketToServiceRecord(MY_UUID);
			} catch (IOException e) {
				Log.e(TAG, "Error in ConnectThreadConstr: " + e.toString());
			}
		}
		
		public void run() {
			mBluetoothAdapter.cancelDiscovery();
			try {
				mmBluetoothSocket.connect();
				ConnectionThread conn = new ConnectionThread(
						mmBluetoothSocket, mmHandler);
				mmHandler.obtainMessage(SOCKET_CONNECTED, conn)
						.sendToTarget();
				conn.start();
			} catch (IOException e) {
				try {
					mmBluetoothSocket.close();
				} catch (IOException e1) {
					Log.e(TAG, "Error in ConnectThread-closing: " + e1.toString());
				}
				Log.e(TAG, "Error in ConnectThread: " + e.toString());
			}
		}
	}

}
