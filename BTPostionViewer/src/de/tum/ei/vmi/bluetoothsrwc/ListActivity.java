package de.tum.ei.vmi.bluetoothsrwc;

import java.util.Set;

import android.os.Bundle;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

public class ListActivity extends Activity {
	
	public static String EXTRA_DEVICE_ADDRESS = "device_address";
	
	private BluetoothAdapter mBluetoothAdapter;
	private ArrayAdapter<String> mPairedDevicesAdapter;
	private ArrayAdapter<String> mNewDevicesAdapter;
	
	private Button mBtnStartDiscovery;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
		setContentView(R.layout.activity_list);
		
		setResult(Activity.RESULT_CANCELED);
		
		mBtnStartDiscovery = (Button) findViewById(R.id.btnStartDiscovery);
		
		mPairedDevicesAdapter = new ArrayAdapter<String>(this, 
				R.layout.device_entry);
		mNewDevicesAdapter = new ArrayAdapter<String>(this, 
				R.layout.device_entry);
		
		ListView lvPaired = (ListView) findViewById(R.id.lvPairedDevices);
		ListView lvNew = (ListView) findViewById(R.id.lvNewDevices);
		lvPaired.setAdapter(mPairedDevicesAdapter);
		lvNew.setAdapter(mNewDevicesAdapter);
		
		lvPaired.setOnItemClickListener(mDeviceClickListener);
		lvNew.setOnItemClickListener(mDeviceClickListener);
		
		mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
		
		Set<BluetoothDevice> pairedDevices = mBluetoothAdapter
							.getBondedDevices();
		if (pairedDevices.size() > 0) {
			for (BluetoothDevice device : pairedDevices) {
				mPairedDevicesAdapter.add(device.getName() + "\n" +
						device.getAddress());
			}
		} else {
			mPairedDevicesAdapter.add("No devices found");
		}
		
		IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
		this.registerReceiver(mReceiver, filter);
		filter = new IntentFilter(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
		this.registerReceiver(mReceiver, filter);
		
		mBtnStartDiscovery.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				mNewDevicesAdapter.clear();
				
				setProgressBarIndeterminateVisibility(true);
				setTitle("Scanning");
				
				if (mBluetoothAdapter.isDiscovering()) {
					mBluetoothAdapter.cancelDiscovery();
				}
				
				mBluetoothAdapter.startDiscovery();
			}
		});
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		
		if (mBluetoothAdapter != null) {
			mBluetoothAdapter.cancelDiscovery();
		}
		
		this.unregisterReceiver(mReceiver);
	}
	
	private OnItemClickListener mDeviceClickListener = new OnItemClickListener() {

		@Override
		public void onItemClick(AdapterView<?> av, View v, int arg2,
				long arg3) {
			mBluetoothAdapter.cancelDiscovery();
			
			String info = ((TextView) v).getText().toString();
			String address = info.substring(info.length() - 17);
			
			Intent intent = new Intent();
			intent.putExtra(EXTRA_DEVICE_ADDRESS, address);
			
			setResult(Activity.RESULT_OK, intent);
			finish();
		}
		
	};

	private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
		
		@Override
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();
			
			if (BluetoothDevice.ACTION_FOUND.equals(action)) {
				BluetoothDevice device = intent.
						getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
				if (device.getBondState() != BluetoothDevice.BOND_BONDED) {
					mNewDevicesAdapter.add(device.getName() + "\n" +
							device.getAddress());
				}
			} else if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED
					.equals(action)) {
				setProgressBarIndeterminateVisibility(false);
				setTitle("Select a partner device");
				if (mNewDevicesAdapter.getCount() == 0) {
					mNewDevicesAdapter.add("No device was found");
				}
			}
		}
	};

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.list, menu);
		return true;
	}

}
