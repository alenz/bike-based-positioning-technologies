package de.tum.ei.vmi.bluetoothsrwc;

import android.app.Activity;
import android.os.Bundle;
import android.view.ViewGroup.LayoutParams;
import at.fhooe.mcm.mdpm.viewer.view.PositionView;

public class PositionViewActivity extends Activity {

	private PositionView posView;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		posView = new PositionView(this, null);
		setContentView(posView, new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
		
	}
	
	
}
