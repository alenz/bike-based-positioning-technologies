# Copyright (C) 2009 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)
LOCAL_CFLAGS += -std=gnu99
LOCAL_MODULE    := mbpt-jni
LOCAL_SRC_FILES := hello-jni.c \
apriltag.c \
apriltag_quad_thresh.c \
tag16h5.c \
tag25h7.c \
tag25h9.c \
tag36h10.c \
tag36h11.c \
tag36artoolkit.c \
g2d.c \
zarray.c \
zhash.c \
zmaxheap.c \
unionfind.c \
matd.c \
image_u8.c \
pnm.c \
image_f32.c \
image_u32.c \
workerpool.c \
time_util.c \
svd22.c \
homography.c \




include $(BUILD_SHARED_LIBRARY)
