/*
 * Copyright (C) 2009 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#include <string.h>
#include <jni.h>
#include <math_util.h>
#include <stdio.h>
#include <image_u8.h>
#include <zarray.h>
#include <apriltag.h>
#include <tag25h9.h>


/* This is a trivial JNI example where we use a native method
 * to return a new VM String. See the corresponding Java source
 * file located at:
 *
 *   apps/samples/hello-jni/project/src/com/example/hellojni/HelloJni.java
 */
jstring
Java_com_example_hellojni_HelloJni_stringFromJNI( JNIEnv* env,
                                                  jobject thiz )
{
#if defined(__arm__)
  #if defined(__ARM_ARCH_7A__)
    #if defined(__ARM_NEON__)
      #if defined(__ARM_PCS_VFP)
        #define ABI "armeabi-v7a/NEON (hard-float)"
      #else
        #define ABI "armeabi-v7a/NEON"
      #endif
    #else
      #if defined(__ARM_PCS_VFP)
        #define ABI "armeabi-v7a (hard-float)"
      #else
        #define ABI "armeabi-v7a"
      #endif
    #endif
  #else
   #define ABI "armeabi"
  #endif
#elif defined(__i386__)
   #define ABI "x86"
#elif defined(__x86_64__)
   #define ABI "x86_64"
#elif defined(__mips64)  /* mips64el-* toolchain defines __mips__ too */
   #define ABI "mips64"
#elif defined(__mips__)
   #define ABI "mips"
#elif defined(__aarch64__)
   #define ABI "arm64-v8a"
#else
   #define ABI "unknown"
#endif
	float f = randf();
    return (*env)->NewStringUTF(env, "Hello from JNI !  Compiled with ABI " ABI ".");
}

jint
Java_com_example_hellojni_HelloJni_unimplementedStringFromJNI( JNIEnv* env,
                                                  jobject thiz, jbyteArray arr )
{
	jsize len = (*env)->GetArrayLength(env, arr);
	int i, sum = 0;
	jbyte *body = (*env)->GetByteArrayElements(env, arr, 0);
	for(i=0; i<len; i++) {
		sum += body[i];
		body[i] = body[i]/2;
	}
	(*env)->ReleaseByteArrayElements(env, arr, body, 0);
	return sum;	
}

jint
Java_com_example_hellojni_HelloJni_darken( JNIEnv* env,
                                                  jobject thiz, jbyteArray arr, jint width, jint height )
{

	jsize len = (*env)->GetArrayLength(env, arr);
	jbyte *body = (*env)->GetByteArrayElements(env, arr, 0);

	image_u8_t *im = image_u8_create_from_byteArray((uint8_t*)body, width, height);
	image_u8_darken(im);
//	image_u8_write_pnm(im, "/sdcard/bbpm/g1jni.jpg");
	(*env)->ReleaseByteArrayElements(env, arr, body, 0);
	return 200;	
}

jint
Java_com_example_hellojni_HelloJni_findTags( JNIEnv* env,
                                                  jobject thiz, jbyteArray arr, jint width, jint height, jbyteArray foundIds, jdoubleArray cornerPoints, jdoubleArray homography)
{

	jsize len = (*env)->GetArrayLength(env, arr);
	jbyte *body = (*env)->GetByteArrayElements(env, arr, 0);
	jbyte *ids = (*env)->GetByteArrayElements(env, foundIds, 0);
	jdouble *points = (*env)->GetDoubleArrayElements(env, cornerPoints, 0);
	jdouble *h = (*env)->GetDoubleArrayElements(env, homography, 0);

	image_u8_t *im = image_u8_create_from_byteArray((uint8_t*)body, width, height);
	
	apriltag_family_t *tf = tag25h9_create();	
	apriltag_detector_t *td = apriltag_detector_create();
	td->nthreads = 10;
    	apriltag_detector_add_family(td, tf);

	zarray_t *detections = apriltag_detector_detect(td, im);
	int lastFound = 66;
	const int sizeOf4Points = 8; // 4*2 needed of the one-dimensional array back to java
	//go through detections
	int numDetections = zarray_size(detections);
	for (int i = 0; i < numDetections; i++) {
	  apriltag_detection_t *det;
	  zarray_get(detections, i, &det);
	  lastFound = det->id;
	  ids[i] = det->id;
	  points[(sizeOf4Points*i) + 0] = det->p[0][0];
	  points[(sizeOf4Points*i) + 1] = det->p[0][1];
	  points[(sizeOf4Points*i) + 2] = det->p[1][0];
	  points[(sizeOf4Points*i) + 3] = det->p[1][1];
	  points[(sizeOf4Points*i) + 4] = det->p[2][0];
	  points[(sizeOf4Points*i) + 5] = det->p[2][1];
	  points[(sizeOf4Points*i) + 6] = det->p[3][0];
	  points[(sizeOf4Points*i) + 7] = det->p[3][1];
	  if(i<5) {
		h[(9*i)+0] = det->H->data[0];
		h[(9*i)+1] = det->H->data[1];
		h[(9*i)+2] = det->H->data[2];
	  	h[(9*i)+3] = det->H->data[3];
		h[(9*i)+4] = det->H->data[4];
		h[(9*i)+5] = det->H->data[5];
		h[(9*i)+6] = det->H->data[6];
	  	h[(9*i)+7] = det->H->data[7];
		h[(9*i)+8] = det->H->data[8];
	  }
	 // printf("detection %3d: id %4d, hamming %d, goodness %f\n", i, det->id, det->hamming, det->goodness);
	  apriltag_detection_destroy(det);

	}

	zarray_destroy(detections);
	apriltag_detector_destroy(td);
	tag25h9_destroy(tf);
	image_u8_destroy(im);

// dont forget to release all allocated memory
//	image_u8_write_pnm(im, "/sdcard/bbpm/g1jni.jpg");
	(*env)->ReleaseByteArrayElements(env, arr, body, 0);
	(*env)->ReleaseByteArrayElements(env, foundIds, ids, 0);
	(*env)->ReleaseDoubleArrayElements(env, cornerPoints, points, 0);
	(*env)->ReleaseDoubleArrayElements(env, homography, h, 0);
	return numDetections;	
}
