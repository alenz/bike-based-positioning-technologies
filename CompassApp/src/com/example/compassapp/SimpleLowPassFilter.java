package com.example.compassapp;




public class SimpleLowPassFilter extends Filter {

	private static final String FILTER_NAME = "SimpleLowPassFilter";
	
	private float smoothingHardness;
	private float smoothedValue;
	
	public SimpleLowPassFilter(float smoothingHardness) {
		this.smoothingHardness = smoothingHardness;
		smoothedValue = Float.NaN;
	}
	
	@Override
	public String getFilterName() {
		return FILTER_NAME;
	}

	@Override
	public float filterNewValue(float newValue) {
		if(smoothedValue == Float.NaN) {
			smoothedValue = newValue;
		}
		smoothedValue += (newValue - smoothedValue) / smoothingHardness;
		return smoothedValue;
	}

}
