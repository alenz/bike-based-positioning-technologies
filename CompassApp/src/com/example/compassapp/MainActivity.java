package com.example.compassapp;

import android.app.Activity;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.util.Log;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends Activity implements SensorEventListener {

	private static final String TAG = "SensorTestActivity";

	// define the display assembly compass picture
	private ImageView image;

	// record the compass picture angle turned
	private float currentDegree = 0f;

	// device sensor manager
	private SensorManager mSensorManager;

	TextView tvHeading;
	
	float[] rotMatFromVector = new float[9];
	float[] rotMat = new float[9];
	private float[] orientationVals = new float[3];

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		// our compass image
		image = (ImageView) findViewById(R.id.imageViewCompass);

		// TextView that will tell the user what degree is he heading
		tvHeading = (TextView) findViewById(R.id.tvHeading);

		// initialize your android device sensor capabilities
		mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
	}

	@Override
	protected void onResume() {
		super.onResume();
		
		// for the system's orientation sensor registered listeners
		mSensorManager.registerListener(this, mSensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR),
				SensorManager.SENSOR_DELAY_GAME);
		
//		mSensorManager.registerListener(this, mSensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE), SensorManager.SENSOR_DELAY_GAME);
	}

	@Override
	protected void onPause() {
		super.onPause();
		
		// to stop the listener and save battery
		mSensorManager.unregisterListener(this);
	}
	
//	@Override
//	public void onSensorChanged(SensorEvent event)
//	{
//	    // It is good practice to check that we received the proper sensor event
//	    if (event.sensor.getType() == Sensor.TYPE_ROTATION_VECTOR)
//	    {
//	        // Convert the rotation-vector to a 4x4 matrix.
//	        SensorManager.getRotationMatrixFromVector(rotMatFromVector,
//	                event.values);
//	        SensorManager
//	                .remapCoordinateSystem(rotMatFromVector,
//	                        SensorManager.AXIS_X, SensorManager.AXIS_Z,
//	                        rotMat);
//	        SensorManager.getOrientation(rotMat, orientationVals);
//
//	        // Optionally convert the result from radians to degrees
//	        orientationVals[0] = (float) Math.toDegrees(orientationVals[0]);
//	        orientationVals[1] = (float) Math.toDegrees(orientationVals[1]);
//	        orientationVals[2] = (float) Math.toDegrees(orientationVals[2]);
//
////	        tv.setText(" Yaw: " + orientationVals[0] + "\n Pitch: "
////	                + orientationVals[1] + "\n Roll (not used): "
////	                + orientationVals[2]);
//	        
//	        
//	     // calculate th rotation matrix
//	     			SensorManager.getRotationMatrixFromVector( rMat, event.values );
//	     			// get the azimuth value (orientation[0]) in degree
//	     			int mAzimuth = (int) ( Math.toDegrees( SensorManager.getOrientation( rMat, orientation )[0] ) + 360 ) % 360;
//	     			tvHeading.setText("Heading: " + mAzimuth + " degrees");
//	     
//	    }
//	}
	
	float[] orientation = new float[3];
	float[] rMat = new float[9];

//	public void onAccuracyChanged( Sensor sensor, int accuracy ) {}

	@Override
	public void onSensorChanged( SensorEvent event ) {
		if( event.sensor.getType() == Sensor.TYPE_ROTATION_VECTOR ){
			// calculate th rotation matrix
			SensorManager.getRotationMatrixFromVector( rMat, event.values );
			// get the azimuth value (orientation[0]) in degree
			int mAzimuth = (int) ( Math.toDegrees( SensorManager.getOrientation( rMat, orientation )[0] ) + 360 ) % 360;
			tvHeading.setText("Heading: " + mAzimuth + " degrees");
		}
	}

//	@Override
//	public void onSensorChanged(SensorEvent event) {
//		if(event.sensor.getType() == Sensor.TYPE_GYROSCOPE) {
//			Log.e(TAG,"Gyro-value 0: " + event.values[0]);
//		} else if(event.sensor.getType() == Sensor.TYPE_ORIENTATION) {
//			// get the angle around the z-axis rotated
//			float degree = Math.round(event.values[0]);
//			
//			tvHeading.setText("Heading: " + Float.toString(degree) + " degrees");
//			
//			// create a rotation animation (reverse turn degree degrees)
//			RotateAnimation ra = new RotateAnimation(
//					currentDegree, 
//					-degree,
//					Animation.RELATIVE_TO_SELF, 0.5f, 
//					Animation.RELATIVE_TO_SELF,
//					0.5f);
//			
//			// how long the animation will take place
//			ra.setDuration(210);
//			
//			// set the animation after the end of the reservation status
//			ra.setFillAfter(true);
//			
//			// Start the animation
//			image.startAnimation(ra);
//			currentDegree = -degree;
//		}
//		
//
//	}

	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
		// not in use
	}
}
