package com.example.compassapp;


public abstract class Filter {
	
	/**
	 * Returns the Name (or a short description of this specific filter)
	 * @return the Name (or a short description of this specific filter)
	 */
	public abstract String getFilterName();

	/**
	 * Worker-method which applies the filter to a new sensor-value and returns an filtered value
	 * @param newValue
	 * @return the filtered newValue
	 */
	public abstract float filterNewValue(float newValue);
}
