filenameX = 'positionsForMatlabX.data';
filenameY = 'positionsForMatlabZ.data';

[X,delimiterOut]=importdata(filenameX)
[Y,delimiterOut]=importdata(filenameY)


nrOfElementsPerBoundingBox = 50;
boxElementsX = ones(1,nrOfElementsPerBoundingBox) * -1000;
boxElementsY = ones(1,nrOfElementsPerBoundingBox) * -1000;

figure

plot(X,Y,'xr')

hold on;

for i = 1:size(X) 
    i
    idxInBoundingBox = mod(i,nrOfElementsPerBoundingBox);
    if(idxInBoundingBox == 0) 
        boxElementsX(nrOfElementsPerBoundingBox) = X(i);
        boxElementsY(nrOfElementsPerBoundingBox) = Y(i);
        
        [qx,qy] = minboundrect(boxElementsX,boxElementsY); %quad
        plot(boxElementsX,boxElementsY,'ro',qx,qy,'b-')
        hold on;
    else
        boxElementsX(idxInBoundingBox) = X(i);
        boxElementsY(idxInBoundingBox) = Y(i);
        
    end
   %fprintf('value of x(%d): %d\n', i, X(i));
end