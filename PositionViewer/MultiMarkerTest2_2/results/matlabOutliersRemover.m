%filenameX = 'positionsForMatlabX_multi.data';
%filenameY = 'positionsForMatlabZ_multi.data';

%[X,delimiterOut]=importdata(filenameX)
%[Y,delimiterOut]=importdata(filenameY)


s = RandStream('mt19937ar','seed',1966);
%RandStream.setDefaultStream(s);
RandStream.setGlobalStream(s);

% Create a vector of X values
clear all
clc
hold off

X = 1:100;
X = X';

for a = 1:100 
    X(a) = 0
   fprintf('value of a: %d\n', a);
end

% Create a noise vector
noise = 10 * randn(100,1);

% Create a second noise value where sigma is much larger
noise2 = 50 * randn(100,1);

% Substitute noise2 for noise1 at obs# (11, 31, 51, 71, 91)
% Many of these points will have an undue influence on the model 

noise(11:30:91) = noise2(11:30:91);
X = X + noise;
% Specify Y = F(X)
Y = 1:100;

% Cook's Distance for a given data point measures the extent to 
% which a regression model would change if this data point 
% were excluded from the regression. Cook's Distance is 
% sometimes used to suggest whether a given data point might be an outlier.

% Use regstats to calculate Cook's Distance
stats = regstats(X,Y,'linear');

% if Cook's Distance > n/4 is a typical treshold that is used to suggest
% the presence of an outlier

%potential_outlier = stats.cookd > 4/length(X);
potential_outlier = stats.cookd > 0.02;


figure
% Display the index of potential outliers and graph the results
X(potential_outlier)
scatter(X,Y, 'b.')
hold on
scatter(X(potential_outlier),Y(potential_outlier), 'r.')

