filenameX = 'positionsForMatlabX_multi1.data';
filenameY = 'positionsForMatlabZ_multi1.data';

[X,delimiterOut]=importdata(filenameX)
[Y,delimiterOut]=importdata(filenameY)

markerPosX = [-0.79, 0, 0.80];
markerPosY = [0, 0, 0];

figure
plot(X,Y,'xr',markerPosX, markerPosY, 'db')
title('Combined Test 1 (5m straigt)');
xlabel('x [cm]');
ylabel('y [cm]');
legend('Inertial-Only Positions','Marker-Pos');
grid on