package at.fhooe.mcm.pdpm.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;


import java.util.List;
import java.util.Vector;








import at.fhooe.mcm.pdpm.data.InertialData;
import at.fhooe.mcm.pdpm.data.Position;

import com.google.gson.Gson;

public class FileUtils {
	
	public static List<InertialData> importInertialData(String path) throws IOException {
		List<InertialData> inertialData = new Vector<InertialData>();
		// import Inertial Data sentences		
		Gson gson = new Gson();
		String gsonString = org.apache.commons.io.FileUtils.readFileToString(new File(path));
		InertialData[]  inertialDataArray = gson.fromJson(gsonString, InertialData[].class);
		inertialData.addAll(Arrays.asList(inertialDataArray));	
		
		return inertialData;
	}
	
	public static List<Position> importPositionData(String path) throws IOException {
		List<Position> positions = new Vector<Position>();
		// import Positions
		Gson gson = new Gson();
		String gsonString = org.apache.commons.io.FileUtils.readFileToString(new File(path));
		Position[]  positionDataArray = gson.fromJson(gsonString, Position[].class);
		positions.addAll(Arrays.asList(positionDataArray));		
				
		return positions;
	}
	
	public static void writePositionsToMatlabExportFiles(List<Position> positions, String pathX, String pathY, String pathZ) throws IOException {
		FileWriter fwX = new FileWriter(pathX);
		FileWriter fwY = new FileWriter(pathY);
		FileWriter fwZ = new FileWriter(pathZ);
		
		for(Position p: positions) {
			fwX.write(p.x + "\n");
			fwY.write(p.y + "\n");
			fwZ.write(p.z + "\n");
		}
		
		fwX.flush();
		fwX.close();
		fwY.flush();
		fwY.close();
		fwZ.flush();
		fwZ.close();
	}
	
	public static List<Position> importMatlabSmoothedPositions(String pathBaseFolder) throws IOException{
		List<Position> positions = new Vector<Position>();
		BufferedReader brX=null, brY=null, brTime=null;
		String lineX = "";
		String lineY, lineTime;

		try {

			brX = new BufferedReader(new FileReader(pathBaseFolder + "/" + "MatlabSmoothedMarkerBasedPositionsX.txt"));
			brY = new BufferedReader(new FileReader(pathBaseFolder + "/" + "MatlabSmoothedMarkerBasedPositionsY.txt"));
			brTime = new BufferedReader(new FileReader(pathBaseFolder + "/" + "MatlabSmoothedMarkerBasedPositionsTime.txt"));
			while ((lineX = brX.readLine()) != null) {
			  lineTime = brTime.readLine();
			  lineY = brY.readLine();
			  float x = new Float(lineX);
			  float z = new Float(lineY);
			  long time = new Long(lineTime);
			  Position p = new Position(x, 0, z, -1);
			  p.time = time;
			  positions.add(p);

			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (brX != null) {
				try {
					brX.close();
					brY.close();
					brTime.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		
		return positions;
	}
	
	public static void main(String[] args) throws IOException {
		List<Position> importedPositions = importMatlabSmoothedPositions(Tests.BigSTestPlus_23_10__3.baseFolder);
		System.out.println("Imported " + importedPositions.size());
	}
 }
