package at.fhooe.mcm.pdpm.util;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;

import at.fhooe.mcm.pdpm.data.InertialSessionSensorEvent;

import com.google.gson.Gson;

public class SensorEventsViewer {
	
	public static void main(String[]args) throws IOException {
		String fileName = Tests.CombTest_aroundNussbaum_1.baseFolder + "/" + "sensorEventsOrienations_1445428404303";
		Gson gson = new Gson();
		String gsonString = FileUtils.readFileToString(new File(fileName));  //  rawOrientation_1442415082634
		InertialSessionSensorEvent[]  dataArray = gson.fromJson(gsonString, InertialSessionSensorEvent[].class);
		int i=0;
		for(InertialSessionSensorEvent data: dataArray) {
			System.out.println(i + ": " +data.toString());
			i++;
		}
	}

}
