package at.fhooe.mcm.pdpm.util;

public class Tests {

	public String baseFolder;
	public String fileNameRawMarkerBasedPositions;
	public String fileNameInertialData;
	public String fileNameRawOrientations;
	public String resultsFolder;
	
	public Tests(String baseFolder, String rawMarkers, String inertialData, String resultsFolder, String rawOrientations) {
		this.baseFolder = baseFolder;
		this.fileNameRawMarkerBasedPositions = rawMarkers;
		this.fileNameInertialData = inertialData;
		this.resultsFolder = resultsFolder;
		this.fileNameRawOrientations = rawOrientations;
		
	}
	
	public static Tests MultiMarkerTest2_2 = new Tests("MultiMarkerTest2_2", "calculatedPositions_movie_sjcam_1443192064794.json", null, "results", null);
	public static Tests BigSTest = new Tests("big_s_test", "calculatedPositions_movie_sjcam_1427419999180.json", null, "results", null);
	public static Tests CombTest_aroundNussbaum_1 = new Tests("CombTest_aroundNussBaum_1", "calculatedPositions_movie_sjcam_1445349485049.json", "inertialData_1445335310941", "results", null);
	public static Tests BigSTestPlus = new Tests("big_s_test_plus", null, "inertialData_1445523542631", "results", "rawOrientation_1445523542631");
	public static Tests BigSTestPlus_23_10__3 = new Tests("Big_S_Plus_23_10___3", "calculatedPositions_movie_sjcam_1445682277846.json", "inertialData_1445615292319", "results", "rawOrientation_1445615292319");
	
	public static Tests BigSTestPlus_23_10__4 = new Tests("Big_S_Plus_23_10___4", "calculatedPositions_movie_sjcam__TODO", "inertialData_1445615430560", "results", "rawOrientation_1445615430560");

}
