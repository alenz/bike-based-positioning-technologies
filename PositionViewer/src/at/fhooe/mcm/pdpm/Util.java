package at.fhooe.mcm.pdpm;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.apache.commons.io.FileUtils;

import at.fhooe.mcm.pdpm.data.Position;

import com.google.gson.Gson;

public class Util {

	public static void log(String tag, String log) {
		System.out.println(tag + ": " + log);
	}
	
	public static void savePositionsAsJsonFile(List<Position> positions, String path) {
		log("Util", "Saved " + positions.size() + " Positions as JSON-Array to sdcard. " + path);
		Gson gson = new Gson();
		Position[] array = new Position[positions.size()];
		positions.toArray(array);
		String str = gson.toJson(array, Position[].class);
		try {
			FileUtils.writeStringToFile(new File(path), str);
		} catch (IOException e) {
			log("Util", "Error while saving calculated positions: " + e.toString());
		}
	}
}
