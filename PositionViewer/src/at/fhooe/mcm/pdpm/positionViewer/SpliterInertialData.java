
package at.fhooe.mcm.pdpm.positionViewer;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import org.apache.commons.io.FileUtils;

import at.fhooe.mcm.pdpm.data.InertialData;
import at.fhooe.mcm.pdpm.util.Tests;

import com.google.gson.Gson;

public class SpliterInertialData {

	
	static final String BASE_FOLDER = Tests.BigSTestPlus_23_10__4.baseFolder;
	static final String FILE_NAME_INERTIAL_DATA = Tests.BigSTestPlus_23_10__4.fileNameInertialData; //Tests.CombTest_aroundNussbaum_1.fileNameInertialData;
	static final String FILE_NAME_RAW_ORIENTATION_DATA = Tests.BigSTestPlus_23_10__4.fileNameRawOrientations; // "rawOrientation_1445335310941";
	static final String RESULTS_FOLDER = Tests.BigSTestPlus_23_10__4.resultsFolder;
	
	static final String FILE_NAME_NEW_INERTIAL_DATA = "splitted_" + FILE_NAME_INERTIAL_DATA;
	
	public static void main(String[] args) throws IOException {
		
		//import InertialData
		Gson gson = new Gson();
		String gsonString = FileUtils.readFileToString(new File(BASE_FOLDER + "/" + FILE_NAME_INERTIAL_DATA));
		InertialData[]  inertialDataArray = gson.fromJson(gsonString, InertialData[].class);
		ArrayList<InertialData> inertialData = new ArrayList<InertialData>();
		inertialData.addAll(Arrays.asList(inertialDataArray));		
		System.out.println("Imported " + inertialData.size() + " InertialData-sentences");
		
		// import rawOrientations
		gsonString = FileUtils.readFileToString(new File(BASE_FOLDER + "/" + FILE_NAME_RAW_ORIENTATION_DATA));
		InertialData[]  rawOrientationsArray = gson.fromJson(gsonString, InertialData[].class);
		ArrayList<InertialData> rawOrientationData = new ArrayList<InertialData>();
		rawOrientationData.addAll(Arrays.asList(rawOrientationsArray));		
		System.out.println("Imported " + rawOrientationData.size() + " RawOrientations-sentences");
		
		//split InertialData up
		ArrayList<InertialData> splittedInertialData = splitUpInertialDate(inertialData, rawOrientationData);
		System.out.println("Splitted the Inertial Data up to " + splittedInertialData.size() + " elements.");
		
		//save new splitted InertialData
		InertialData[] inertialDataArray1 = new InertialData[splittedInertialData.size()];
		splittedInertialData.toArray(inertialDataArray1);
		String str = gson.toJson(inertialDataArray1, InertialData[].class);
		FileUtils.writeStringToFile(new File(BASE_FOLDER + "/" + FILE_NAME_NEW_INERTIAL_DATA), str);
		
		float sumDistanceOldInertialData = 0;
		for(InertialData i1: inertialData) {
			sumDistanceOldInertialData+=i1.distance;
		}
		
		float sumDistanceSplittedInertialData = 0;
		for(InertialData i2: splittedInertialData) {
			sumDistanceSplittedInertialData+=i2.distance;
		}
		System.out.println("Old Inert Data: " + sumDistanceOldInertialData + " and splitted Inert Data: " + sumDistanceSplittedInertialData);
		
	}

	public static final int NUM_OF_SPLITTED_ELEMENTS = 10;
	
	public static ArrayList<InertialData> splitUpInertialDate(ArrayList<InertialData> oldInertialData, ArrayList<InertialData> rawOrientations) {
		ArrayList<InertialData> newInertialData = new ArrayList<InertialData>();
		int idxRawOrientation = 0;
		InertialData currentRawOrienationData;
		long currentInertialDataStartTime = 0;
		long currentInertialDataEndTime = 0;
		long currentSegmentStartTime;
		int segmentCounter = 0;
		for(InertialData inertialData: oldInertialData) {
			currentInertialDataEndTime = inertialData.sessionTime;
			long segmentDuration = (inertialData.sessionTime-currentInertialDataStartTime)/NUM_OF_SPLITTED_ELEMENTS;
			currentSegmentStartTime = currentInertialDataStartTime;
			segmentCounter = 0;
			while(currentSegmentStartTime < currentInertialDataEndTime && (segmentDuration > NUM_OF_SPLITTED_ELEMENTS)) {
				long currentSegmentEndTime = currentSegmentStartTime + segmentDuration;
				currentRawOrienationData = rawOrientations.get(idxRawOrientation);
				
				float sumSegmentOrientations = 0;
				float ctrSegmentOrientations = 0;
				while(currentRawOrienationData.sessionTime < currentSegmentEndTime && (segmentCounter < NUM_OF_SPLITTED_ELEMENTS)) {
					sumSegmentOrientations += currentRawOrienationData.angle;
					ctrSegmentOrientations++;
					
					idxRawOrientation++;
					currentRawOrienationData = rawOrientations.get(idxRawOrientation);
				}
				
				if(ctrSegmentOrientations > 0) {
					segmentCounter++;
					newInertialData.add(new InertialData(sumSegmentOrientations/ctrSegmentOrientations, inertialData.distance/NUM_OF_SPLITTED_ELEMENTS, -1, currentSegmentEndTime));					
				}
				currentSegmentStartTime = currentSegmentEndTime;
			}
//			System.out.println("newInertialData-Size: " + newInertialData.size());	
				
			currentInertialDataStartTime = inertialData.sessionTime;
		}
		
		return newInertialData;
	}
}
