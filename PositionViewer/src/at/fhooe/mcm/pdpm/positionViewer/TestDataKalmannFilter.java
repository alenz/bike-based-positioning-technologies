package at.fhooe.mcm.pdpm.positionViewer;

import java.util.List;
import java.util.Vector;

import at.fhooe.mcm.pdpm.data.InertialData;
import at.fhooe.mcm.pdpm.data.Position;

public class TestDataKalmannFilter {

	public static List<Position> getTestDataFromMarkerPositioning() {
		Vector<Position> positionList = new Vector<Position>();
		
		positionList.add(new Position(5d, 1d, 1d, 1));
		positionList.add(new Position(5.3d, 1d, 1.1d, 2));
		positionList.add(new Position(4.6d, 1d, 1.2d, 3));
		positionList.add(new Position(4.5d, 1d, 1.28d, 4));
		positionList.add(new Position(5.1d, 1d, 1.43d, 5));
		positionList.add(new Position(5.0d, 1d, 1.51d, 6));
		positionList.add(new Position(4.3d, 1d, 1.58d, 7));
		positionList.add(new Position(5.1d, 1d, 1.71d, 8));
		positionList.add(new Position(5.2d, 1d, 1.8d, 9));
		positionList.add(new Position(4.9d, 1d, 1.92d, 10));
		positionList.add(new Position(5.0d, 1d, 2.02d, 11));
		// TODO add here all the other positions with some noisy character ....
		
		
		return positionList;
	}
	
	
	public static List<InertialData> getTestDataFromInertialPositioning() {
		Vector<InertialData> inertialDataList = new Vector<InertialData>();
		
		// the offset between the track-coordinate-system (orientation) and the real north must be considered in the kalman-filter-implementation
		
		inertialDataList.add(new InertialData(90f, 0.1f, 0, 0));
		inertialDataList.add(new InertialData(89f, 0.1f, 0, 0));	
		inertialDataList.add(new InertialData(90f, 0.1f, 0, 0));
		inertialDataList.add(new InertialData(91f, 0.1f, 0, 0));	
		inertialDataList.add(new InertialData(91f, 0.1f, 0, 0));	
		inertialDataList.add(new InertialData(89f, 0.1f, 0, 0));	
		inertialDataList.add(new InertialData(88f, 0.1f, 0, 0));	
		inertialDataList.add(new InertialData(90f, 0.1f, 0, 0));
		inertialDataList.add(new InertialData(90f, 0.1f, 0, 0));	
		inertialDataList.add(new InertialData(91f, 0.1f, 0, 0));	
		inertialDataList.add(new InertialData(90f, 0.1f, 0, 0));	
		// TODO add here all the other inertial-Sensor-values with some noisy character ....
		
		
		return inertialDataList;
	}
}
