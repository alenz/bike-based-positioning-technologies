package at.fhooe.mcm.pdpm.positionViewer;

import java.awt.Color;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Vector;

import javax.swing.JFrame;

import org.apache.commons.io.FileUtils;

import at.fhooe.mcm.pdpm.data.InertialData;
import at.fhooe.mcm.pdpm.data.Marker;
import at.fhooe.mcm.pdpm.data.Position;
import at.fhooe.mcm.pdpm.data.PositionVector;
import at.fhooe.mcm.pdpm.data.TrackSetup;
import at.fhooe.mcm.pdpm.util.Tests;

import com.google.gson.Gson;

public class InertialOnlyPositionViewer {

	private static ArrayList<PositionVector> positionVectors = new ArrayList<PositionVector>();
	private static ArrayList<Position> inertialOnlyPositions = new ArrayList<Position>();
	private static ArrayList<InertialData> inertialData = new ArrayList<InertialData>();
//	private static Position lastPosition = new Position(100.0f, 0.0f, 0f, 0);
//	private static Position lastPosition = new Position(5.0f, 7.95f, 0f, 0);
	private static Position lastPosition = new Position(8.83f, 19.14f, 0f, 0);
	private static float startOrientation = Float.NaN;
	
	static final String BASE_FOLDER = Tests.BigSTestPlus_23_10__4.baseFolder;
//	static final String FILE_NAME_INERTIAL_DATA = "splitted_" + Tests.CombTest_aroundNussbaum_1.fileNameInertialData;
	static final String FILE_NAME_INERTIAL_DATA =  "splitted_" + Tests.BigSTestPlus_23_10__4.fileNameInertialData; //"splitted_inertialData_1445428404303_slow_going";
//	static final String FILE_NAME_INERTIAL_DATA = "inertialData_1445428404303_slow_going";
	
	static final String RESULTS_FOLDER = Tests.BigSTestPlus_23_10__4.resultsFolder;
	
//	static final String FILE_NAME_RESULTED_MATLAB_X 11= "positionsForMatlabX_splitted.data";
//	static final String FILE_NAME_RESULTED_MATLAB_Y = "positionsForMatlabY_splitted.data";
//	static final String FILE_NAME_RESULTED_MATLAB_Z = "positionsForMatlabZ_splitted.data";
	
	static final String FILE_NAME_RESULTED_MATLAB_X = "positionsForMatlabX_inertSpl.data"; //_inertSpl
	static final String FILE_NAME_RESULTED_MATLAB_Y = "positionsForMatlabY_inertSpl.data";
	static final String FILE_NAME_RESULTED_MATLAB_Z = "positionsForMatlabZ_inertSpl.data";
	
	private static final float fixedStartDirection = 180.0f; // 90
	
	public static void main(String[] args) throws IOException {
		
		// import Inertial Data sentences
		Gson gson = new Gson();
//		String gsonString = FileUtils.readFileToString(new File("combinedTest1/inertialData_1441817326886_test1.json"));
		String gsonString = FileUtils.readFileToString(new File(BASE_FOLDER + "/" + FILE_NAME_INERTIAL_DATA));
		InertialData[]  inertialDataArray = gson.fromJson(gsonString, InertialData[].class);
		inertialData.addAll(Arrays.asList(inertialDataArray));		
		System.out.println("Imported " + inertialData.size() + " InertialData-sentences");
		
		// calc Positions
		PositionVector currentPositionVector;
		float transformedOrientation; // means in the direction of the StartPoint on the Test-Track (90� is along the y-aches up)
		inertialOnlyPositions.add(lastPosition); // add already the starting positions
		
		for(InertialData in: inertialData) {
			if(Float.isNaN(startOrientation)) {
				startOrientation = in.angle;
			}
			transformedOrientation =(fixedStartDirection + (startOrientation - in.angle));
			currentPositionVector = new PositionVector(lastPosition, in.distance*1.15f, transformedOrientation);
			
			lastPosition = currentPositionVector.endPosition;
			positionVectors.add(currentPositionVector);
			inertialOnlyPositions.add(currentPositionVector.endPosition);
		}
		
		//write positions to txt for matlab
		FileWriter fwX = new FileWriter(new File(BASE_FOLDER + "/" + RESULTS_FOLDER + "/" + FILE_NAME_RESULTED_MATLAB_X));
		FileWriter fwY = new FileWriter(BASE_FOLDER + "/" + RESULTS_FOLDER + "/" + FILE_NAME_RESULTED_MATLAB_Y);
		FileWriter fwZ = new FileWriter(BASE_FOLDER + "/" + RESULTS_FOLDER + "/" + FILE_NAME_RESULTED_MATLAB_Z);
		
		for(Position p: inertialOnlyPositions) {			
			fwX.write(p.x + "\n");
			fwY.write(p.y + "\n");
			fwZ.write(p.z + "\n");
		}
		
		fwX.flush();
		fwX.close();
		fwY.flush();
		fwY.close();
		fwZ.flush();
		fwZ.close();
		
		// show Results
		TrackSetup trackSetup = new TrackSetup();
		trackSetup.width = 200.f;
		trackSetup.height = 600.f;
		Marker m = new Marker(1, new Position(0d, 0d, 0d, 0), 1.50f, 0, 0, 0);
		Vector<Marker> markers = new Vector<Marker>();
		markers.addElement(m);
		trackSetup.markers = markers;
		
		List<Position> positionLists[] = new List[1];
		positionLists[0] = inertialOnlyPositions;
//		positionLists[1] = markerBasedPositions;
		
		Color[] positionColors = new Color[1];
		positionColors[0] = Color.RED;
//		positionColors[1] = Color.BLACK;
		
		// TODO Auto-generated method stub
		 JFrame f = new JFrame("Swing Paint Demo");
	        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	        f.add(new PositionViewPanel(positionLists, positionColors, trackSetup));
	        f.pack();
	        f.setVisible(true);
	}
}
