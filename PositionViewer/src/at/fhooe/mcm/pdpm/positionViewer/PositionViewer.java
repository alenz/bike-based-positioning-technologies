package at.fhooe.mcm.pdpm.positionViewer;

import static java.lang.Math.pow;

import java.awt.Color;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import javax.swing.JFrame;

import kalman.KalmanFilter;

import org.apache.commons.collections4.queue.CircularFifoQueue;
import org.apache.commons.io.FileUtils;

import Jama.Matrix;
import at.fhooe.mcm.pdpm.data.Marker;
import at.fhooe.mcm.pdpm.data.Position;
import at.fhooe.mcm.pdpm.data.TrackSetup;
import at.fhooe.mcm.pdpm.util.Tests;

import com.google.gson.Gson;

/**
 * The {@link PositionViewer}-Project is/was for trying different ways/algorithmns to smooth the raw Point-Cloud
 * which is the result of the Marker-based-positioning-approach. 
 * Also the visualization is part of this.
 * @author Alexander
 *
 */
public class PositionViewer extends JFrame {
	
	private static final long serialVersionUID = -7360347248702036117L;
	
	static final int NUM_AVG_ELEMENTS = 10;
	static final double SMOOTHING_HARDNESS = 5;
	static CircularFifoQueue<Position> fifoQue = new CircularFifoQueue<Position>(NUM_AVG_ELEMENTS);
	
	public static Position calcRunningAvgPosition() {
		Iterator<Position> iterator = fifoQue.iterator();
		Position p;
		double x=0d , y=0d, z=0d;
		while(iterator.hasNext()) {
			p = iterator.next();
			x += p.x;
			y += p.y;
			z += p.z;
		}
		return new Position(x/NUM_AVG_ELEMENTS, y/NUM_AVG_ELEMENTS, z/NUM_AVG_ELEMENTS, 0);	// TODO - frameTime
	}
	
	static final String BASE_FOLDER = Tests.BigSTestPlus_23_10__3.baseFolder;
	static final String FILE_NAME_RAW_MARKER_BASED_POSITIONS = Tests.BigSTestPlus_23_10__3.fileNameRawMarkerBasedPositions;
	static final String RESULTS_FOLDER = Tests.BigSTestPlus_23_10__3.resultsFolder;
	
	public static void main(String[] args) throws IOException {
		Vector<Position> positions = new Vector<Position>();
//		positions.add(new Position(-0.1d, 0, 0.3));
		Gson gson = new Gson();
//		String gsonString = FileUtils.readFileToString(new File("snake_hockey_curve/calculatedPositions_movie_sjcam_1426804222970.json"));
//		String gsonString = FileUtils.readFileToString(new File("quadrat_test_nussbaum_1/outside_circle_test_digicam16_9_1427119802937.json"));
//		String gsonString = FileUtils.readFileToString(new File("u_test/digicam16_9_1427273515844.json"));
//		String gsonString = FileUtils.readFileToString(new File("big_s_test/calculatedPositions_movie_sjcam_1427419999180.json"));
		String gsonString = FileUtils.readFileToString(new File(BASE_FOLDER + "/" + FILE_NAME_RAW_MARKER_BASED_POSITIONS));
		System.out.println("Read from File: " + BASE_FOLDER);
		Position[]  posArray = gson.fromJson(gsonString, Position[].class);
		positions.addAll(Arrays.asList(posArray));
		
		Vector<Position> newPositions = new Vector<Position>();
		Position currentNewPosition;
		
		FileWriter fwX = new FileWriter(BASE_FOLDER + "/" + RESULTS_FOLDER + "/" + "positionsForMatlabX_marker_based.data");
		FileWriter fwY = new FileWriter(BASE_FOLDER + "/" + RESULTS_FOLDER + "/" + "positionsForMatlabY_marker_based.data");
		FileWriter fwZ = new FileWriter(BASE_FOLDER + "/" + RESULTS_FOLDER + "/" + "positionsForMatlabZ_marker_based.data");
		FileWriter fwTime = new FileWriter(BASE_FOLDER + "/" + RESULTS_FOLDER + "/" + "time.data");
		
		double sumX=0d, sumY=0d, sumZ=0d;
		int posNr = 1;
		
		initKalmanFilter(positions.get(0));	//init the KF
		System.out.println(positions.size());
		
		double smoothedX, smoothedY, smoothedZ;
		Position firstPosition = positions.get(0);
		smoothedX = firstPosition.x;
		smoothedY = firstPosition.y;
		smoothedZ = firstPosition.z;
		
		int nullCounter = 0;
		
		for(Position p: positions) {
//			if(p.x > 8 && (p.z<25) && (p.z) > 17) {			// for big s test, to remove a group of strange outlieers
//				continue;
//			}
			if(p == null) {
				nullCounter++;
				continue;
			}
			fwX.write(p.x + "\n");
			fwY.write(p.y + "\n");
			fwZ.write(p.z + "\n");
			fwTime.write(p.time + "\n");
			fifoQue.add(p);
//			
//			sumX += p.x;
//			sumY += p.y;
//			sumZ += p.z;
//			if(posNr % NUM_AVG_ELEMENTS == 0) {
//				currentNewPosition = new Position(sumX/NUM_AVG_ELEMENTS, sumY/NUM_AVG_ELEMENTS, sumZ/NUM_AVG_ELEMENTS);
//				newPositions.add(currentNewPosition);
//				System.out.println(p.toString() + " -> " + currentNewPosition.toString());
//				sumX = 0;
//				sumY = 0;
//				sumZ = 0;
//			}
			posNr++;
			
			//kalman example
//			currentNewPosition = calcNewKalmanFilteredPosition(p);
//			newPositions.add(currentNewPosition);
//			System.out.println(p.toString() + " -> " + currentNewPosition.toString());
			// end kalman example
			
			// begin moving avging
//			currentNewPosition = calcRunningAvgPosition();
//			newPositions.add(currentNewPosition);
////			System.out.println(p.toString() + " -> " + currentNewPosition.toString());
//			System.out.println("frameNr: " + p.frameNr + " time: " + p.time + " | " + p.toString());
			// end moving avging
			
//			currentNewPosition.y = p.y;
//			currentNewPosition.z = p.z;
			
			//begin smoothing with a low pass filter
			// If you have a fixed frame rate
			smoothedX += (p.x - smoothedX) / SMOOTHING_HARDNESS;
			smoothedY += (p.y - smoothedY) / SMOOTHING_HARDNESS;
			smoothedZ += (p.z - smoothedZ) / SMOOTHING_HARDNESS;
//			smoothedZ = (p.z); //only for brake Test
			currentNewPosition = new Position(smoothedX, smoothedY, smoothedZ, p.frameNr);
			currentNewPosition.time = p.time;
			newPositions.add(currentNewPosition);
			System.out.println("frameNr: " + currentNewPosition.frameNr + " time: " + currentNewPosition.time + " | " + currentNewPosition.toString());
			//end smoothin with low pass filter
			
		}
		
		calcDistance(newPositions);
		
		System.out.println("Found: " + nullCounter + " null-positions.");
		
		fwX.flush();
		fwX.close();
		fwY.flush();
		fwY.close();
		fwZ.flush();
		fwZ.close();
		fwTime.flush();
		fwTime.close();
		
		TrackSetup trackSetup = new TrackSetup();
		trackSetup.width = 200.f;
		trackSetup.height = 550.f;
		Marker m = new Marker(1, new Position(50d, 0d, 500d, 0), 100f, 0, 0, 0);
		Vector<Marker> markers = new Vector<Marker>();
		markers.addElement(m);
		trackSetup.markers = markers;
		
		// TODO Auto-generated method stub
		 JFrame f = new JFrame("Swing Paint Demo");
	        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	        f.add(new PositionViewPanel(new List[]{newPositions}, new Color[]{Color.RED}, trackSetup));
	        f.pack();
	        f.setVisible(true);
//		 SwingUtilities.invokeLater(doRun);
	}
	
//	private static Position smoothNewPosition(Position new)
	
	private static KalmanFilter KF;
	
	private static void initKalmanFilter(Position firstPosition) {
		//process parameters
        double dt =	50d;	// 50d;
        double processNoiseStdev = 3d;	//3;
        double measurementNoiseStdev = 5000;
        double m = 0;

        //init filter
        KF = KalmanFilter.buildKF(0, 0, dt, pow(processNoiseStdev, 2) / 2, pow(measurementNoiseStdev, 2));
        KF.setX(new Matrix(new double[][]{{firstPosition.x}, {0}, {firstPosition.z}, {0}}));
	}
	
	private static Position calcNewKalmanFilteredPosition(Position p) {
		//filter update
        KF.predict();
        KF.correct(new Matrix(new double[][]{{p.x}, {p.z}}));
        
        return new Position(KF.getX().get(0, 0), p.y, KF.getX().get(1, 0), 0);	//TODO - frame Time
	}
	
	public static double calcDistance(Vector<Position> positions) {
		int segmentLength = 5;
		Position lastSegmentEndPosition = positions.get(0);
		double lastSegmentSpeed = 0;
		Position currentSegmentEndPosition = null;
		double currentSegmentLength = -1;
		double currentSegmentDuration = 0d;
		double currentSpeed = 0; //in m/s
		double sumSegmentLengths = 0;
		double acc = 0;
		for(int i=1; i<positions.size(); i++) {
			if(i%segmentLength == 0) {
				currentSegmentEndPosition = positions.get(i);
				currentSegmentLength = calcDistanceBetweenPoints(currentSegmentEndPosition, lastSegmentEndPosition);
				currentSegmentDuration = currentSegmentEndPosition.time - lastSegmentEndPosition.time;
				currentSpeed = currentSegmentLength/(currentSegmentDuration/1000);
				acc = (currentSpeed-lastSegmentSpeed)/(currentSegmentDuration/1000);
				System.out.println(currentSegmentLength + " Duration: " + currentSegmentDuration + "  Speed: " + currentSpeed + " Acc: " + acc + "  - LastSegmentPos: " + lastSegmentEndPosition);
				
				sumSegmentLengths += currentSegmentLength;
				lastSegmentEndPosition = currentSegmentEndPosition;
				lastSegmentSpeed = currentSpeed;
			}
		}
		System.out.println("Sum Segment length: " + sumSegmentLengths);
		return sumSegmentLengths;
	}
	
	public static double calcDistanceBetweenPoints(Position p1, Position p2) {
		double xDelta = p2.x - p1.x;
		double zDelta = p2.z - p1.z;
		return Math.sqrt((xDelta*xDelta) + (zDelta*zDelta));
	}

}
