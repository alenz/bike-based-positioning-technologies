package at.fhooe.mcm.pdpm.positionViewer;

import java.awt.Color;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import javax.swing.JFrame;

import org.apache.commons.collections4.queue.CircularFifoQueue;
import org.apache.commons.io.FileUtils;
import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.linear.RealMatrix;

import robotutils.filter.KalmanFilterRobotUtil;
import at.fhooe.mcm.pdpm.Util;
import at.fhooe.mcm.pdpm.data.InertialData;
import at.fhooe.mcm.pdpm.data.Marker;
import at.fhooe.mcm.pdpm.data.Position;
import at.fhooe.mcm.pdpm.data.TrackSetup;

import com.google.gson.Gson;

/**
 * The {@link KalmanPositionViewer} combines the filtered Marker based Positions with some inertial sensor values
 * via a Kalman Filter Implementation
 * @author Alexander
 *
 */
public class KalmanPositionViewer extends JFrame {
	
	private static final long serialVersionUID = -7360347248702036117L;
	
	static final int NUM_AVG_ELEMENTS = 10;
	static final double SMOOTHING_HARDNESS = 5;
	static CircularFifoQueue<Position> fifoQue = new CircularFifoQueue<Position>(NUM_AVG_ELEMENTS);
	
	
	static final double UPPER_BOUND_FOR_CALCULATING_R1 = 0.5; // m�
	static final double LOWER_BOUND_FOR_CALCULATING_R1 = 0.1; // m�
	
	static final double UPPER_BOUND_FOR_CALCULATING_R2 = 1.0; // m
	static final double LOWER_BOUND_FOR_CALCULATING_R2 = 0.3; // m
	
	static final double R1_WEIGHT_PERCENTAGE_BIG = 0.05; // %/100
	static final double R1_WEIGHT_PERCENTAGE_MEDIUM = 0.15; // %/100
	static final double R1_WEIGHT_PERCENTAGE_LOW = 0.35; // %/100
	
	static final double R2_WEIGHT_PERCENTAGE_BIG = 0.05; // %/100
	static final double R2_WEIGHT_PERCENTAGE_MEDIUM = 0.15; // %/100
	static final double R2_WEIGHT_PERCENTAGE_LOW = 0.35; // %/100
	
	static final int NUM_ELEMENTS_FOR_CALCULATNG_R = 100;
	static CircularFifoQueue<Position> measurmentErrorCovarianceRfifoQueue = new CircularFifoQueue<Position>(NUM_ELEMENTS_FOR_CALCULATNG_R);
	
	static final int NUM_ELEMENTS_FOR_CALCULATING_DISTANCES_BETWEEN_LAST_POSITIONS = 100; // probably set to more like 3,4,5
	static CircularFifoQueue<Position> distanceCalcOfLastPositionsFifoQueue = new CircularFifoQueue<Position>(NUM_ELEMENTS_FOR_CALCULATING_DISTANCES_BETWEEN_LAST_POSITIONS);
	
	public static Position calcRunningAvgPosition() {
		Iterator<Position> iterator = fifoQue.iterator();
		Position p;
		double x=0d , y=0d, z=0d;
		while(iterator.hasNext()) {
			p = iterator.next();
			x += p.x;
			y += p.y;
			z += p.z;
		}
		return new Position(x/NUM_AVG_ELEMENTS, y/NUM_AVG_ELEMENTS, z/NUM_AVG_ELEMENTS, 0);	// TODO - frameTime
	}
	
	public static double calculateDistanceBetweenLastPositions() {
		Iterator<Position> iterator = distanceCalcOfLastPositionsFifoQueue.iterator();
		
		Position p;
		
		double minX=0d, minY=0d, minZ=0d;
		double maxX=0d, maxY=0d, maxZ=0d;
		if(iterator.hasNext()) {
			p = iterator.next();
			minX = p.x;
			maxX = p.x;
			minY = p.y;
			maxY = p.y;
			minZ = p.z;
			maxZ = p.z;
		}
		while(iterator.hasNext()) {
			p = iterator.next();
			if(p.x < minX) minX = p.x;
			if(p.y < minY) minY = p.y;
			if(p.z < minZ) minZ = p.z;
			
			if(p.x > maxX) maxX = p.x;
			if(p.y > maxY) maxY = p.y;
			if(p.z > maxZ) maxZ = p.z;			
		}
		double deltaX = maxX - minX;
		double deltaY = maxY - minY;
		
		return Math.sqrt((deltaX * deltaX) + (deltaY * deltaY));
	}
	
	/**
	 * Method calculated the Measurement Error Covariance Value R using the last couple of measured positions
	 * In our Kalman-Filter-Implementation this value is also the Kalman-Gain K stating the weighting of the measurments and
	 * predictions. (its simply a percentage value of how much we trust in the measurements.
	 * @return
	 */
	public static double calcMeasurementErrorCovarianceRValue() {
		Iterator<Position> iterator = measurmentErrorCovarianceRfifoQueue.iterator();
		Position p;
		
		double minX=0d, minY=0d, minZ=0d;
		double maxX=0d, maxY=0d, maxZ=0d;
		if(iterator.hasNext()) {
			p = iterator.next();
			minX = p.x;
			maxX = p.x;
			minY = p.y;
			maxY = p.y;
			minZ = p.z;
			maxZ = p.z;
		}
		while(iterator.hasNext()) {
			p = iterator.next();
			if(p.x < minX) minX = p.x;
			if(p.y < minY) minY = p.y;
			if(p.z < minZ) minZ = p.z;
			
			if(p.x > maxX) maxX = p.x;
			if(p.y > maxY) maxY = p.y;
			if(p.z > maxZ) maxZ = p.z;			
		}
		
		
		double R1 = 0;
		double R2 = 0;
		
		double area = (maxX - minX) * (maxY - minY);
		if(area >= UPPER_BOUND_FOR_CALCULATING_R1) {
			R1 = R1_WEIGHT_PERCENTAGE_BIG;
		} else if((area > LOWER_BOUND_FOR_CALCULATING_R1) && (area < UPPER_BOUND_FOR_CALCULATING_R1)) {
			R1 = R1_WEIGHT_PERCENTAGE_MEDIUM;
		} else if(area <= LOWER_BOUND_FOR_CALCULATING_R1) {
			R1 = R1_WEIGHT_PERCENTAGE_LOW;
		}
		
		double distanceBetweenLastPositions = calculateDistanceBetweenLastPositions();
		if(distanceBetweenLastPositions >= UPPER_BOUND_FOR_CALCULATING_R2) {
			R2 = R2_WEIGHT_PERCENTAGE_BIG;
		} else if((distanceBetweenLastPositions > LOWER_BOUND_FOR_CALCULATING_R2) && (distanceBetweenLastPositions < UPPER_BOUND_FOR_CALCULATING_R2)) {
			R2 = R2_WEIGHT_PERCENTAGE_MEDIUM;
		} else if(distanceBetweenLastPositions < LOWER_BOUND_FOR_CALCULATING_R2) {
			R2 = R2_WEIGHT_PERCENTAGE_LOW;
		}
		
		return R1 + R2;
	}
	
	static final String pathToInertialData = "combinedTest1/inertialData_1441817326886_test1.json";
	static final String pathToFilteredMarkerBasedPositions = "combinedTest1/results/filteredMarkerBasedPositionsTimeBasedAvgExtended.json";
	static final String pathToRawMarkerBasedPositions = "combinedTest1/calculatedPositions_movie_sjcam_1441922998860.json";

	private static final String TAG = "KalmanPositionViewer:";
	
	private static Position startPosition = new Position(100.0f, 0.0f, 0.0f, 0);
	
	public static void main(String[] args) throws IOException {
//		List<Position> markerBasedPositions = TestDataKalmannFilter.getTestDataFromMarkerPositioning();
//		List<InertialData> inertialPositioningData = TestDataKalmannFilter.getTestDataFromInertialPositioning();
		
		List<Position> rawMarkerBasedPositions = new Vector<Position>();
		List<Position> filteredMarkerBasedPositions = new Vector<Position>();
		List<InertialData> inertialPositioningData = new Vector<InertialData>();
		
		// import Inertial Data sentences		
		Gson gson = new Gson();
		String gsonString = FileUtils.readFileToString(new File(pathToInertialData));
		InertialData[]  inertialDataArray = gson.fromJson(gsonString, InertialData[].class);
		inertialPositioningData.addAll(Arrays.asList(inertialDataArray));		
		System.out.println("Imported " + inertialPositioningData.size() + " InertialData-sentences");
		
		// import rawMarkerBasedPositions
		gsonString = FileUtils.readFileToString(new File(pathToRawMarkerBasedPositions));
		Position[]  positionDataArray = gson.fromJson(gsonString, Position[].class);
		rawMarkerBasedPositions.addAll(Arrays.asList(positionDataArray));		
		System.out.println("Imported " + rawMarkerBasedPositions.size() + " RawMarkerBasedPositions");
		
		//import filteredMarkerBasedPositions
		gsonString = FileUtils.readFileToString(new File(pathToFilteredMarkerBasedPositions));
		Position[]  filteredPositionDataArray = gson.fromJson(gsonString, Position[].class);
		filteredMarkerBasedPositions.addAll(Arrays.asList(filteredPositionDataArray));		
		System.out.println("Imported " + filteredMarkerBasedPositions.size() + " FilterdMarkerBasedPositions");
		
		Vector<Position> newPositions = new Vector<Position>();
		Position currentNewPosition;
		
		double sumX=0d, sumY=0d, sumZ=0d;
		int posNr = 1;
		
		initKalmanFilter(startPosition);	//init the KF
		System.out.println(filteredMarkerBasedPositions.size());
		
//		double smoothedX, smoothedY, smoothedZ;
//		Position firstPosition = markerBasedPositions.get(0);
//		smoothedX = firstPosition.x;
//		smoothedY = firstPosition.y;
//		smoothedZ = firstPosition.z;
		
		
		InertialData inertialData = null;
		long currentInertialDataTime = 0;
		
		int idxMarkerPositions = 0;
		long currentMarkerPositionTime = 0;
		
		for(InertialData currentInertialData: inertialPositioningData) {			
			distanceCalcOfLastPositionsFifoQueue.clear();
			measurmentErrorCovarianceRfifoQueue.clear();
			
			currentInertialDataTime = currentInertialData.sessionTime;
			// get (interpolated) filtered and raw Marker based Position
			Position currentRawMarkerBasedPosition = null;
			do {
				currentRawMarkerBasedPosition = rawMarkerBasedPositions.get(idxMarkerPositions);
				distanceCalcOfLastPositionsFifoQueue.add(currentRawMarkerBasedPosition);
				measurmentErrorCovarianceRfifoQueue.add(currentRawMarkerBasedPosition);
				currentMarkerPositionTime = filteredMarkerBasedPositions.get(idxMarkerPositions).time;
				idxMarkerPositions++;
			} while((currentMarkerPositionTime < currentInertialDataTime) && (idxMarkerPositions<filteredMarkerBasedPositions.size()));
			Position filteredMarkerbasedPosition1 = filteredMarkerBasedPositions.get(idxMarkerPositions-2);
			Position filteredMarkerBasedPosition2 = filteredMarkerBasedPositions.get(idxMarkerPositions-1);
			float interpolationPercentage = ((float)(currentInertialDataTime-filteredMarkerbasedPosition1.time)) / ((float)(filteredMarkerBasedPosition2.time-filteredMarkerbasedPosition1.time));
			Util.log(TAG, "InterpolationPercentage: " + interpolationPercentage);
			
			double interpolatedX = filteredMarkerbasedPosition1.x + ((filteredMarkerBasedPosition2.x - filteredMarkerbasedPosition1.x) * interpolationPercentage);
			double interpolatedY = filteredMarkerbasedPosition1.y + ((filteredMarkerBasedPosition2.y - filteredMarkerbasedPosition1.y) * interpolationPercentage);
			double interpolatedZ = filteredMarkerbasedPosition1.z + ((filteredMarkerBasedPosition2.z - filteredMarkerbasedPosition1.z) * interpolationPercentage);
			
			Position interpolatedFilteredPosition = new Position(interpolatedX, interpolatedY, interpolatedZ, -1);
			interpolatedFilteredPosition.time = currentInertialDataTime;
					
			currentNewPosition = calcNewKalmanFilteredPosition(interpolatedFilteredPosition, currentInertialData);
			currentNewPosition.time = currentInertialDataTime;
			newPositions.add(currentNewPosition);
			System.out.println("KalmannFilteredPosition:" + " frameNr: " + currentNewPosition.frameNr + " time: " + currentNewPosition.time + " | " + currentNewPosition.toString());
			//end smoothin with low pass filter
			
		}
			
	
		
		TrackSetup trackSetup = new TrackSetup();
		trackSetup.width = 200.f;
		trackSetup.height = 550.f;
		Marker m = new Marker(1, new Position(0d, 0d, 0d, 0), 1.50f, 0, 0, 0);
		Vector<Marker> markers = new Vector<Marker>();
		markers.addElement(m);
		trackSetup.markers = markers;
		
		List<Position> positionLists[] = new List[2];
		positionLists[0] = newPositions;
		positionLists[1] = filteredMarkerBasedPositions;
		
		Color[] positionColors = new Color[2];
		positionColors[0] = Color.RED;
		positionColors[1] = Color.BLACK;
		
		// TODO Auto-generated method stub
		 JFrame f = new JFrame("Swing Paint Demo");
	        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	        f.add(new PositionViewPanel(positionLists, positionColors, trackSetup));
	        f.pack();
	        f.setVisible(true);
//		 SwingUtilities.invokeLater(doRun);
	}
	
	
	private static KalmanFilterRobotUtil KF;
	
	private static void initKalmanFilter(Position startPosition) {
	

        double initialValueX = startPosition.x;
        double initialValueY = startPosition.z;
        RealMatrix x0 = MatrixUtils.createRealMatrix(new double[][]{{initialValueX}, {initialValueY}, {1}});
        RealMatrix P0 = MatrixUtils.createRealMatrix(new double[][]{{1, 0, 0}, {0, 1, 0}, {0, 0, 1}});
        
        //init filter
        KF = new KalmanFilterRobotUtil(x0, P0);
	}
	
	private static RealMatrix Q = MatrixUtils.createRealMatrix(new double[][]{{1, 0, 0}, {0, 1, 0}, {0, 0, 1}});
	private static RealMatrix H = MatrixUtils.createRealMatrix(new double[][]{{1, 0, 0}, {0, 1, 0}, {0, 0, 1}}); // no transformation between measurement vector-space and state-vector-space
	
	private static RealMatrix X;
	
	private static float startOrientation = Float.NaN;
	
	private static Position calcNewKalmanFilteredPosition(Position markerBasedPosition, InertialData inertialData) {
		//filter update
		System.out.println("-------------------");
		if(Float.isNaN(startOrientation)) {
			startOrientation = inertialData.angle;
		}
		float transformedOrientation = (90 + (inertialData.angle - startOrientation)) % 360;
		inertialData.angle = transformedOrientation;
		RealMatrix F = PredictionModel.updatePredictionModel(inertialData);
		
		double kalmanGain = calcMeasurementErrorCovarianceRValue() / 2;
		System.out.println("K=" + kalmanGain);
		RealMatrix K = MatrixUtils.createRealMatrix(new double[][]{{kalmanGain, 0, 0}, {0, kalmanGain, 0}, {0, 0, kalmanGain}});
		RealMatrix z = MatrixUtils.createRealMatrix(new double[][]{{markerBasedPosition.x}, {markerBasedPosition.z}, {1}});
        KF.predictWithoutQ(F);
        System.out.println("F=" + F.toString());
        System.out.println("X-State after prediction: " + KF.getState());
        KF.updateWithModifiedKalmanGainCalc(K, H, z);
        X = KF.getState();
        System.out.println("X-State after measurement-update: " + KF.getState());
        return new Position(X.getEntry(0, 0), markerBasedPosition.y, X.getEntry(1, 0), 0);	//TODO - frame Time
	}
	
	
	
	public static double calcDistanceBetweenPoints(Position p1, Position p2) {
		double xDelta = p2.x - p1.x;
		double zDelta = p2.z - p1.z;
		return Math.sqrt((xDelta*xDelta) + (zDelta*zDelta));
	}

}
