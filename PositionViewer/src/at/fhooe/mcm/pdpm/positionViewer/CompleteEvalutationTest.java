package at.fhooe.mcm.pdpm.positionViewer;

import java.io.IOException;
import java.util.List;
import java.util.Vector;

import at.fhooe.mcm.pdpm.data.InertialData;
import at.fhooe.mcm.pdpm.data.Position;
import at.fhooe.mcm.pdpm.util.FileUtils;
import at.fhooe.mcm.pdpm.util.Tests;

public class CompleteEvalutationTest {
	
	static final Tests TEST = Tests.BigSTestPlus_23_10__3;
	
	static final String BASE_FOLDER = TEST.baseFolder;
	static final String RESULTS_FOLDER = TEST.resultsFolder;
	static final String pathToInertialData = BASE_FOLDER + "/" + "splitted_" + TEST.fileNameInertialData;
	static final String pathToFilteredMarkerBasedPositions = BASE_FOLDER + "/" + TEST.resultsFolder + "/" + "filteredMarkerBasedPositionsTimeBasedAvgExtended.json";
	static final String pathToRawMarkerBasedPositions = BASE_FOLDER + "/" + TEST.fileNameRawMarkerBasedPositions;
	
	static final String PATH_MATLAB_EXPORT_X = BASE_FOLDER + "/" + RESULTS_FOLDER + "/" + "CustomKalmanFilteredX.data";
	static final String PATH_MATLAB_EXPORT_Y = BASE_FOLDER + "/" + RESULTS_FOLDER + "/" + "CustomKalmanFilteredY.data";
	static final String PATH_MATLAB_EXPORT_Z = BASE_FOLDER + "/" + RESULTS_FOLDER + "/" + "CustomKalmanFilteredZ.data";

	static final float startDirection = 180;
	static final Position startPosition = new Position(8.83f, 0f,  19.14f, 0);
	
	public static void main(String[] args) throws IOException {
		List<Position> rawMarkerBasedPositions = FileUtils.importPositionData(pathToRawMarkerBasedPositions);
//		List<Position> filteredMarkerBasedPositions = FileUtils.importPositionData(pathToFilteredMarkerBasedPositions);
		List<Position> filteredMarkerBasedPositions = FileUtils.importMatlabSmoothedPositions(BASE_FOLDER);
		List<InertialData> inertialPositioningData = FileUtils.importInertialData(pathToInertialData);
		
		CustomKalmanImplementation customKalmanFilter = new CustomKalmanImplementation(startDirection, startPosition, rawMarkerBasedPositions, filteredMarkerBasedPositions, inertialPositioningData);
		
		List<Position> newPositions = customKalmanFilter.processData();
		FileUtils.writePositionsToMatlabExportFiles(newPositions, PATH_MATLAB_EXPORT_X, PATH_MATLAB_EXPORT_Y, PATH_MATLAB_EXPORT_Z);
		System.out.println("CustomKalmanImplementation created " + newPositions.size() + " new KalmanFiltered Positions.");
		
		
	}
}
