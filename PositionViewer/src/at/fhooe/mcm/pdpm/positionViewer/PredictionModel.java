package at.fhooe.mcm.pdpm.positionViewer;

import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.linear.RealMatrix;

import at.fhooe.mcm.pdpm.data.InertialData;

public class PredictionModel {

	public static RealMatrix updatePredictionModel(InertialData data) {
		double deltaX = data.distance * Math.cos(Math.toRadians(data.angle));
		double deltaY = data.distance * Math.sin(Math.toRadians(data.angle));
//		System.out.println(data.toString() + " | DeltaX: " +  deltaX + " , DeltaY: " + deltaY);
		
		return MatrixUtils.createRealMatrix(new double[][] {{1, 0, data.distance * Math.cos(Math.toRadians(data.angle))},
															{0, 1, data.distance * Math.sin(Math.toRadians(data.angle))},
															{0, 0, 1}});
	}
}
