package at.fhooe.mcm.pdpm.positionViewer;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.util.List;

import javax.swing.JPanel;

import at.fhooe.mcm.pdpm.data.Marker;
import at.fhooe.mcm.pdpm.data.Position;
import at.fhooe.mcm.pdpm.data.TrackSetup;

/**
 * {@link JPanel} for visualizing a list of positions in a test-track.
 * @author Alexander
 *
 */
public class PositionViewPanel extends JPanel implements ComponentListener{
	
	private static final long serialVersionUID = 3740781455780768274L;
	
	public List<Position>[] positionLists;
	public Color[] colors;
	public TrackSetup trackSetup;		//include marker-positions, orientations and size of the track were the positions should be displayed
	
	public Dimension panelSize;
	
	public float pixelPerMeter = 1;
	public int maxPixelHeight = -1;
	public boolean heightAttached = true;
	boolean zeorXIsInTheMiddel = false;
	int offsetX = 0;
	
	public PositionViewPanel(List<Position>[] positionlists, Color[] colors, TrackSetup setup) {
		this.positionLists = positionlists;
		this.colors = colors;
		this.trackSetup = setup;
//		setBorder(BorderFactory.createLineBorder(Color.black));
		addComponentListener(this);
	}
	
	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.setColor(Color.BLACK);
		
		g.drawRect(0, 0, (int)(trackSetup.width*pixelPerMeter), (int)(trackSetup.height*pixelPerMeter));
		
		g.drawLine(offsetX, 0, offsetX, (int)(trackSetup.height*pixelPerMeter));
		
		for(int i=0; i<trackSetup.height; i=i+10) {
			g.drawLine(0, (int)(i*pixelPerMeter), (int)(trackSetup.width*pixelPerMeter), (int)(i*pixelPerMeter));
		}
		
		for(int j=0; j<trackSetup.width; j=j+10) {
			g.drawLine((int)(j*pixelPerMeter), 0, (int)(j*pixelPerMeter), (int)(trackSetup.height*pixelPerMeter));
		}
		
		//draw Markers
		g.setColor(Color.GREEN);
		int mCenterX, mCenterY, mHalfSize;
		for(Marker m: trackSetup.markers) {
			mCenterX = (int) (m.pos.x * pixelPerMeter);
			mCenterY = (int) (maxPixelHeight-m.pos.z * pixelPerMeter);
			mHalfSize = (int) (m.size * pixelPerMeter / 2);
			
			g.drawLine(offsetX + (mCenterX - mHalfSize), mCenterY, offsetX + (mCenterX + mHalfSize), mCenterY);
			g.drawString("M"+ m.id, offsetX + (mCenterX + mHalfSize + 1), mCenterY);
		}
		
		//drawPositions
		g.setColor(Color.RED);
		int x, y;
		int i=0;
		for(List<Position> positionList: positionLists) {
			g.setColor(colors[i]);
			i++;
			for(Position p: positionList) {
				x = (int)(offsetX + (p.x * pixelPerMeter) - 1);				//with corret marker-setup
				y = (int)((maxPixelHeight-p.z * pixelPerMeter) - 1);			/// !!!!! important change here when a swap of the y- and the z-axis should be
				
//			x = (int) (offsetX + (p.x*pixelPerMeter));
//			y = (int) (-p.z*pixelPerMeter);
				
				g.fillOval(x, y, 6, 6);
//			System.out.println("x: " +  x + " , y: " +y);
			}
			
		}
		
	}

	@Override
	public void componentResized(ComponentEvent e) {
		Dimension size = getSize();
		panelSize = new Dimension(size.width, size.height);
		double screenRatio = panelSize.getWidth()/panelSize.getHeight();
		double trackRatio = trackSetup.width / trackSetup.height;
	
		if(screenRatio > trackRatio) {
			heightAttached = true;
			pixelPerMeter = (float) (panelSize.height / trackSetup.height);
		} else {
			pixelPerMeter = (float) (panelSize.width / trackSetup.width);
		}
		int trackWidthPixels = (int) (pixelPerMeter * trackSetup.width);
		if(zeorXIsInTheMiddel) {
			offsetX = trackWidthPixels/2;
		}
		
		maxPixelHeight = (int) (pixelPerMeter*trackSetup.height);
		System.out.println(panelSize + "heightAttached: " + heightAttached + " pixelPerMeter: " + pixelPerMeter);
		System.out.println("height in Pixels: " + pixelPerMeter*trackSetup.height + "(in m = " +  trackSetup.height + ")");
		repaint();
	}

	@Override
	public void componentMoved(ComponentEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void componentShown(ComponentEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void componentHidden(ComponentEvent e) {
		// TODO Auto-generated method stub
		
	}
	
	

}
