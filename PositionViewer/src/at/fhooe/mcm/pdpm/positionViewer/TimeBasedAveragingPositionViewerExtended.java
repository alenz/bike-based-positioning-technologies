package at.fhooe.mcm.pdpm.positionViewer;

import static java.lang.Math.pow;

import java.awt.Color;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Vector;

import javax.swing.JFrame;

import kalman.KalmanFilter;

import org.apache.commons.io.FileUtils;

import Jama.Matrix;
import at.fhooe.mcm.pdpm.Util;
import at.fhooe.mcm.pdpm.data.Marker;
import at.fhooe.mcm.pdpm.data.Position;
import at.fhooe.mcm.pdpm.data.TrackSetup;
import at.fhooe.mcm.pdpm.util.Tests;

import com.google.gson.Gson;

/**
 * The {@link TimeBasedAveragingPositionViewerExtended}-Project is/was for trying different ways/algorithmns to smooth the raw Point-Cloud
 * which is the result of the Marker-based-positioning-approach. 
 * Also the visualization is part of this.
 * @author Alexander
 *
 */
public class TimeBasedAveragingPositionViewerExtended extends JFrame {
	
	private static final long serialVersionUID = -7360347248702036117L;
	
	static final String BASE_FOLDER = Tests.BigSTestPlus_23_10__3.baseFolder;
	static final String FILE_NAME_RAW_MARKER_BASED_POSITIONS = Tests.BigSTestPlus_23_10__3.fileNameRawMarkerBasedPositions;
	static final String RESULTS_FOLDER = Tests.BigSTestPlus_23_10__3.resultsFolder;
	
	static final String MATLAB_FILE_POSTFIX = "_avgExt1";
	static final String FILE_NAME_RESULTED_MATLAB_X = "positionsForMatlabX" + MATLAB_FILE_POSTFIX + ".data";
	static final String FILE_NAME_RESULTED_MATLAB_Y = "positionsForMatlabY" + MATLAB_FILE_POSTFIX + ".data";
	static final String FILE_NAME_RESULTED_MATLAB_Z = "positionsForMatlabZ" + MATLAB_FILE_POSTFIX + ".data";
	
	
	private static final String TAG = null;
	
	public static void main(String[] args) throws IOException {
		Vector<Position> positions = new Vector<Position>();
		Gson gson = new Gson();
		String gsonString = FileUtils.readFileToString(new File(BASE_FOLDER + "/" + FILE_NAME_RAW_MARKER_BASED_POSITIONS));
		Position[]  posArray = gson.fromJson(gsonString, Position[].class);
		positions.addAll(Arrays.asList(posArray));
		
		Vector<Position> newPositions = new Vector<Position>();
		Position currentNewPosition;
		
		FileWriter fwX = new FileWriter(BASE_FOLDER + "/" + RESULTS_FOLDER + "/" + FILE_NAME_RESULTED_MATLAB_X);
		FileWriter fwY = new FileWriter(BASE_FOLDER + "/" + RESULTS_FOLDER + "/" + FILE_NAME_RESULTED_MATLAB_Y);
		FileWriter fwZ = new FileWriter(BASE_FOLDER + "/" + RESULTS_FOLDER + "/" + FILE_NAME_RESULTED_MATLAB_Z);
		
		double sumX=0d, sumY=0d, sumZ=0d;
		
		System.out.println(positions.size());		
		
		
		final int timePeriod = 100; // all positions in this period of time will be averaged.
		long startTime = positions.get(0).time;
		long lastTime = startTime;
		long currentTime;
		int posAvgCounter = 0;
		int posIdx = -1;
		int posIdxUp = 0;
		int posIdxDown;
		
		for(Position p: positions) {
			posIdx++;
			
			
			
//			if(p.time < timePeriod) {				
//				System.out.println("++");
//				continue;
//			}
			if(p == null) {
				continue;
			}
			
			currentTime = p.time;
			startTime = p.time;
			lastTime = p.time;
			posIdxUp = posIdx;
			posAvgCounter = 0;
			sumX = sumY = sumZ = 0;
			Position currentUpPosition = positions.get(posIdxUp);
			System.out.println(posIdxUp);
			while((currentTime-startTime) < timePeriod ) {
				if(currentUpPosition != null) {
					sumX = sumX + currentUpPosition.x;
					sumY = sumY + currentUpPosition.y;
					sumZ = sumZ + currentUpPosition.z;
					lastTime = currentUpPosition.time;					
					posAvgCounter++;
				}
				posIdxUp++;
				if(posIdxUp >= positions.size()) {
					System.out.println(posIdxUp);
					break;
				}
				currentUpPosition = positions.get(posIdxUp);
				if(currentUpPosition != null) {
					currentTime = currentUpPosition.time;
				} else {
					System.out.println("CurrentUpPosition is Null ");					
				}
			}
			
			long overallEndTime = lastTime;
			
			posIdxDown = posIdx - 1;
//			Util.log(TAG, "posIdxDown: " + posIdxDown);
			if(posIdxDown >= 0) {
				Position currentDownPosition = positions.get(posIdxDown);
				currentTime = currentDownPosition.time;
				startTime = currentDownPosition.time;
				lastTime = currentDownPosition.time;
				
				while((startTime-currentTime) < timePeriod && (posIdxDown >= 0)) {
					sumX = sumX + currentDownPosition.x;
					sumY = sumY + currentDownPosition.y;
					sumZ = sumZ + currentDownPosition.z;
					lastTime = currentDownPosition.time;
					posIdxDown--;
					posAvgCounter++;
					if(posIdxDown < 0) {
						break;
					}
					currentDownPosition = positions.get(posIdxDown);
					currentTime = currentDownPosition.time;
				} 
				
				long overallStartTime = lastTime;
			}
			
			
			currentNewPosition = new Position(sumX/posAvgCounter, sumY/posAvgCounter, sumZ/posAvgCounter, -1);
			currentNewPosition.time = p.time;
			newPositions.add(currentNewPosition);
			fwX.write(currentNewPosition.x + "\n");
			fwY.write(currentNewPosition.y + "\n");
			fwZ.write(currentNewPosition.z + "\n");
		//	System.out.println(" time: " + currentNewPosition.time + " | " + currentNewPosition.toString());
						
			
		}
		
		Util.savePositionsAsJsonFile(newPositions, BASE_FOLDER + "/" + RESULTS_FOLDER + "/" + "filteredMarkerBasedPositionsTimeBasedAvgExtended.json");
		
		
//		calcDistance(newPositions);
		
		fwX.flush();
		fwX.close();
		fwY.flush();
		fwY.close();
		fwZ.flush();
		fwZ.close();
		
		TrackSetup trackSetup = new TrackSetup();
		trackSetup.width = 200.f;
		trackSetup.height = 550.f;
		Marker m = new Marker(1, new Position(50d, 0d, 500d, 0), 100f, 0, 0, 0);
		Vector<Marker> markers = new Vector<Marker>();
		markers.addElement(m);
		trackSetup.markers = markers;
		
		// TODO Auto-generated method stub
		 JFrame f = new JFrame("Swing Paint Demo");
	        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	        f.add(new PositionViewPanel(new List[]{newPositions}, new Color[]{Color.RED}, trackSetup));
	        f.pack();
	        f.setVisible(true);
//		 SwingUtilities.invokeLater(doRun);
	}
	
//	private static Position smoothNewPosition(Position new)
	
	private static KalmanFilter KF;
	
	private static void initKalmanFilter(Position firstPosition) {
		//process parameters
        double dt =	50d;	// 50d;
        double processNoiseStdev = 3d;	//3;
        double measurementNoiseStdev = 5000;
        double m = 0;

        //init filter
        KF = KalmanFilter.buildKF(0, 0, dt, pow(processNoiseStdev, 2) / 2, pow(measurementNoiseStdev, 2));
        KF.setX(new Matrix(new double[][]{{firstPosition.x}, {0}, {firstPosition.z}, {0}}));
	}
	
	private static Position calcNewKalmanFilteredPosition(Position p) {
		//filter update
        KF.predict();
        KF.correct(new Matrix(new double[][]{{p.x}, {p.z}}));
        
        return new Position(KF.getX().get(0, 0), p.y, KF.getX().get(1, 0), 0);	//TODO - frame Time
	}
	
	public static double calcDistance(Vector<Position> positions) {
		int segmentLength = 5;
		Position lastSegmentEndPosition = positions.get(0);
		double lastSegmentSpeed = 0;
		Position currentSegmentEndPosition = null;
		double currentSegmentLength = -1;
		double currentSegmentDuration = 0d;
		double currentSpeed = 0; //in m/s
		double sumSegmentLengths = 0;
		double acc = 0;
		for(int i=1; i<positions.size(); i++) {
			if(i%segmentLength == 0) {
				currentSegmentEndPosition = positions.get(i);
				currentSegmentLength = calcDistanceBetweenPoints(currentSegmentEndPosition, lastSegmentEndPosition);
				currentSegmentDuration = currentSegmentEndPosition.time - lastSegmentEndPosition.time;
				currentSpeed = currentSegmentLength/(currentSegmentDuration/1000);
				acc = (currentSpeed-lastSegmentSpeed)/(currentSegmentDuration/1000);
				System.out.println(currentSegmentLength + " Duration: " + currentSegmentDuration + "  Speed: " + currentSpeed + " Acc: " + acc + "  - LastSegmentPos: " + lastSegmentEndPosition);
				
				sumSegmentLengths += currentSegmentLength;
				lastSegmentEndPosition = currentSegmentEndPosition;
				lastSegmentSpeed = currentSpeed;
			}
		}
		System.out.println("Sum Segment length: " + sumSegmentLengths);
		return sumSegmentLengths;
	}
	
	public static double calcDistanceBetweenPoints(Position p1, Position p2) {
		double xDelta = p2.x - p1.x;
		double zDelta = p2.z - p1.z;
		return Math.sqrt((xDelta*xDelta) + (zDelta*zDelta));
	}

}
