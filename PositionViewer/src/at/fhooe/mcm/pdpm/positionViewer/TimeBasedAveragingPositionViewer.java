package at.fhooe.mcm.pdpm.positionViewer;

import static java.lang.Math.pow;

import java.awt.Color;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Vector;

import javax.swing.JFrame;

import kalman.KalmanFilter;

import org.apache.commons.io.FileUtils;

import Jama.Matrix;
import at.fhooe.mcm.pdpm.Util;
import at.fhooe.mcm.pdpm.data.Marker;
import at.fhooe.mcm.pdpm.data.Position;
import at.fhooe.mcm.pdpm.data.TrackSetup;

import com.google.gson.Gson;

/**
 * The {@link TimeBasedAveragingPositionViewer}-Project is/was for trying different ways/algorithmns to smooth the raw Point-Cloud
 * which is the result of the Marker-based-positioning-approach. 
 * Also the visualization is part of this.
 * @author Alexander
 *
 */
public class TimeBasedAveragingPositionViewer extends JFrame {
	
	private static final long serialVersionUID = -7360347248702036117L;	
	
	static final String BASE_FOLDER = "combinedTest1";
	static final String FILE_NAME_RAW_MARKER_BASED_POSITIONS = "calculatedPositions_movie_sjcam_1441922998860.json";
	static final String RESULTS_FOLDER = "results";
	
	public static void main(String[] args) throws IOException {
		Vector<Position> positions = new Vector<Position>();
		Gson gson = new Gson();
		String gsonString = FileUtils.readFileToString(new File(BASE_FOLDER + "/" + FILE_NAME_RAW_MARKER_BASED_POSITIONS));
		Position[]  posArray = gson.fromJson(gsonString, Position[].class);
		positions.addAll(Arrays.asList(posArray));
		
		Vector<Position> newPositions = new Vector<Position>();
		Position currentNewPosition;
		
		FileWriter fwX = new FileWriter(BASE_FOLDER + "/" + RESULTS_FOLDER + "/" + "positionsForMatlabX.data");
		FileWriter fwY = new FileWriter(BASE_FOLDER + "/" + RESULTS_FOLDER + "/" + "positionsForMatlabY.data");
		FileWriter fwZ = new FileWriter(BASE_FOLDER + "/" + RESULTS_FOLDER + "/" + "positionsForMatlabZ.data");
		
		double sumX=0d, sumY=0d, sumZ=0d;
		
		System.out.println(positions.size());		
		
		
		final int timePeriod = 1000; // all positions in this period of time will be averaged.
		long startTime = positions.get(0).time;
		long lastTime = startTime;
		long currentTime;
		int posAvgCounter = 0;
		for(Position p: positions) {
			
			fwX.write(p.x + "\n");
			fwY.write(p.y + "\n");
			fwZ.write(p.z + "\n");
			currentTime = p.time;
			
			if((currentTime-startTime) < timePeriod) {
				posAvgCounter++;
				sumX = sumX + p.x;
				sumY = sumY + p.y;
				sumZ = sumZ + p.z;
				lastTime = p.time;
			} else {
				currentNewPosition = new Position(sumX/posAvgCounter, sumY/posAvgCounter, sumZ/posAvgCounter, -1);
				currentNewPosition.time = startTime + ((lastTime-startTime)/2);
				newPositions.add(currentNewPosition);
				System.out.println(" time: " + currentNewPosition.time + " | " + currentNewPosition.toString());
				startTime = p.time;
				lastTime = p.time;
				posAvgCounter = 1;
				sumX = p.x;
				sumY = p.y;
				sumZ = p.z;
			}


		}
		
		Util.savePositionsAsJsonFile(newPositions, BASE_FOLDER + "/" + RESULTS_FOLDER + "/" + "filteredMarkerBasedPositionsTimeBasedAvg.json");
		
//		calcDistance(newPositions);
		
		fwX.flush();
		fwX.close();
		fwY.flush();
		fwY.close();
		fwZ.flush();
		fwZ.close();
		
		TrackSetup trackSetup = new TrackSetup();
		trackSetup.width = 200.f;
		trackSetup.height = 550.f;
		Marker m = new Marker(1, new Position(50d, 0d, 500d, 0), 100f, 0, 0, 0);
		Vector<Marker> markers = new Vector<Marker>();
		markers.addElement(m);
		trackSetup.markers = markers;
		
		// TODO Auto-generated method stub
		 JFrame f = new JFrame("Swing Paint Demo");
	        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	        f.add(new PositionViewPanel(new List[]{newPositions}, new Color[]{Color.RED}, trackSetup));
	        f.pack();
	        f.setVisible(true);
//		 SwingUtilities.invokeLater(doRun);
	}
	
//	private static Position smoothNewPosition(Position new)
	
	private static KalmanFilter KF;
	
	private static void initKalmanFilter(Position firstPosition) {
		//process parameters
        double dt =	50d;	// 50d;
        double processNoiseStdev = 3d;	//3;
        double measurementNoiseStdev = 5000;
        double m = 0;

        //init filter
        KF = KalmanFilter.buildKF(0, 0, dt, pow(processNoiseStdev, 2) / 2, pow(measurementNoiseStdev, 2));
        KF.setX(new Matrix(new double[][]{{firstPosition.x}, {0}, {firstPosition.z}, {0}}));
	}
	
	private static Position calcNewKalmanFilteredPosition(Position p) {
		//filter update
        KF.predict();
        KF.correct(new Matrix(new double[][]{{p.x}, {p.z}}));
        
        return new Position(KF.getX().get(0, 0), p.y, KF.getX().get(1, 0), 0);	//TODO - frame Time
	}
	
	public static double calcDistance(Vector<Position> positions) {
		int segmentLength = 5;
		Position lastSegmentEndPosition = positions.get(0);
		double lastSegmentSpeed = 0;
		Position currentSegmentEndPosition = null;
		double currentSegmentLength = -1;
		double currentSegmentDuration = 0d;
		double currentSpeed = 0; //in m/s
		double sumSegmentLengths = 0;
		double acc = 0;
		for(int i=1; i<positions.size(); i++) {
			if(i%segmentLength == 0) {
				currentSegmentEndPosition = positions.get(i);
				currentSegmentLength = calcDistanceBetweenPoints(currentSegmentEndPosition, lastSegmentEndPosition);
				currentSegmentDuration = currentSegmentEndPosition.time - lastSegmentEndPosition.time;
				currentSpeed = currentSegmentLength/(currentSegmentDuration/1000);
				acc = (currentSpeed-lastSegmentSpeed)/(currentSegmentDuration/1000);
				System.out.println(currentSegmentLength + " Duration: " + currentSegmentDuration + "  Speed: " + currentSpeed + " Acc: " + acc + "  - LastSegmentPos: " + lastSegmentEndPosition);
				
				sumSegmentLengths += currentSegmentLength;
				lastSegmentEndPosition = currentSegmentEndPosition;
				lastSegmentSpeed = currentSpeed;
			}
		}
		System.out.println("Sum Segment length: " + sumSegmentLengths);
		return sumSegmentLengths;
	}
	
	public static double calcDistanceBetweenPoints(Position p1, Position p2) {
		double xDelta = p2.x - p1.x;
		double zDelta = p2.z - p1.z;
		return Math.sqrt((xDelta*xDelta) + (zDelta*zDelta));
	}

}
