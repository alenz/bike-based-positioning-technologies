package at.fhooe.mcm.pdpm.positionViewer;

import java.util.List;
import java.util.Vector;

import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.linear.RealMatrix;

import robotutils.filter.KalmanFilterRobotUtil;
import at.fhooe.mcm.pdpm.Util;
import at.fhooe.mcm.pdpm.data.InertialData;
import at.fhooe.mcm.pdpm.data.Position;

public class CustomKalmanImplementation {

	private List<Position> rawMarkerBasedPositions = new Vector<Position>();
	private List<Position> filteredMarkerBasedPositions = new Vector<Position>();
	private List<InertialData> inertialPositioningData = new Vector<InertialData>();
	
	private KalmanFilterRobotUtil KF;
	
	private Position startPosition;
	private float fixedStartDirection = 180.0f; // 90
	
	private double kgDistance; //
	private float kgPercentage;
	
	public CustomKalmanImplementation(float startDirection, Position startPosition, List<Position> rawMarkerBasedPositions, List<Position> filteredMarkerBasedPositions, List<InertialData> inertialPositioningData) {
		this.rawMarkerBasedPositions = rawMarkerBasedPositions;
		this.filteredMarkerBasedPositions = filteredMarkerBasedPositions;
		this.inertialPositioningData = inertialPositioningData;
		this.startPosition = startPosition;
		this.fixedStartDirection = startDirection;
	}
	
	public List<Position> processData() {
		Vector<Position> newPositions = new Vector<Position>();
		Position currentNewPosition;
		
		initKalmanFilter(startPosition);	//init the KF
		
		long currentInertialDataTime = 0;
		
		int idxMarkerPositions = 0;
		long currentMarkerPositionTime = 0;
		
		for(InertialData currentInertialData: inertialPositioningData) {			
//			distanceCalcOfLastPositionsFifoQueue.clear();
//			measurmentErrorCovarianceRfifoQueue.clear();
			
			currentInertialDataTime = currentInertialData.sessionTime;
			// get (interpolated) filtered and raw Marker based Position
			Position currentRawMarkerBasedPosition = null;
			idxMarkerPositions = 0;
			do {
				currentRawMarkerBasedPosition = rawMarkerBasedPositions.get(idxMarkerPositions);
//				distanceCalcOfLastPositionsFifoQueue.add(currentRawMarkerBasedPosition);
//				measurmentErrorCovarianceRfifoQueue.add(currentRawMarkerBasedPosition);
				currentMarkerPositionTime = filteredMarkerBasedPositions.get(idxMarkerPositions).time;
				idxMarkerPositions++;
			} while((currentMarkerPositionTime < currentInertialDataTime) && (idxMarkerPositions<filteredMarkerBasedPositions.size()));
			Position filteredMarkerbasedPosition1, filteredMarkerBasedPosition2;
			if(idxMarkerPositions < 2) {
				filteredMarkerbasedPosition1 = startPosition;
				filteredMarkerBasedPosition2 = filteredMarkerBasedPositions.get(idxMarkerPositions-1);
			} else {
				filteredMarkerbasedPosition1 = filteredMarkerBasedPositions.get(idxMarkerPositions-2);
				filteredMarkerBasedPosition2 = filteredMarkerBasedPositions.get(idxMarkerPositions-1);
			}
			kgDistance = calcDistanceBetweenPoints(filteredMarkerbasedPosition1, filteredMarkerBasedPosition2);
			float interpolationPercentage = ((float)(currentInertialDataTime-filteredMarkerbasedPosition1.time)) / ((float)(filteredMarkerBasedPosition2.time-filteredMarkerbasedPosition1.time));
			kgPercentage = interpolationPercentage;
//			Util.log(TAG, "InterpolationPercentage: " + interpolationPercentage);
			if(interpolationPercentage > 1) {
				System.out.println("InterpolatedPercentage is > 1 -> Error");
			}
			
			
			double interpolatedX = filteredMarkerbasedPosition1.x + ((filteredMarkerBasedPosition2.x - filteredMarkerbasedPosition1.x) * interpolationPercentage);
			double interpolatedY = filteredMarkerbasedPosition1.y + ((filteredMarkerBasedPosition2.y - filteredMarkerbasedPosition1.y) * interpolationPercentage);
			double interpolatedZ = filteredMarkerbasedPosition1.z + ((filteredMarkerBasedPosition2.z - filteredMarkerbasedPosition1.z) * interpolationPercentage);
			
			Position interpolatedFilteredPosition = new Position(interpolatedX, interpolatedY, interpolatedZ, -1);
			interpolatedFilteredPosition.time = currentInertialDataTime;
			
			if(interpolatedZ > 20) {
				System.out.println("InterpolatedZ: " + interpolatedZ + " at time " + interpolatedFilteredPosition.time);
			}
					
			currentNewPosition = calcNewKalmanFilteredPosition(interpolatedFilteredPosition, currentInertialData);
			currentNewPosition.time = currentInertialDataTime;
			newPositions.add(currentNewPosition);
//			System.out.println("KalmannFilteredPosition:" + " frameNr: " + currentNewPosition.frameNr + " time: " + currentNewPosition.time + " | " + currentNewPosition.toString());
			//end smoothin with low pass filter
			
		}
		
		return newPositions;
	}
	
	
	
	
	private void initKalmanFilter(Position startPosition) {
	

        double initialValueX = startPosition.x;
        double initialValueY = startPosition.z;
        RealMatrix x0 = MatrixUtils.createRealMatrix(new double[][]{{initialValueX}, {initialValueY}, {1}});
        RealMatrix P0 = MatrixUtils.createRealMatrix(new double[][]{{1, 0, 0}, {0, 1, 0}, {0, 0, 1}});
        
        //init filter
        KF = new KalmanFilterRobotUtil(x0, P0);
	}
	
	private static RealMatrix Q = MatrixUtils.createRealMatrix(new double[][]{{1, 0, 0}, {0, 1, 0}, {0, 0, 1}});
	private static RealMatrix H = MatrixUtils.createRealMatrix(new double[][]{{1, 0, 0}, {0, 1, 0}, {0, 0, 1}}); // no transformation between measurement vector-space and state-vector-space
	
	private static RealMatrix X;
	
	private float startOrientation = Float.NaN;
	
	
	private Position calcNewKalmanFilteredPosition(Position markerBasedPosition, InertialData inertialData) {
		//filter update
//		System.out.println("-------------------");
		if(Float.isNaN(startOrientation)) {
			startOrientation = inertialData.angle;
		}
		float transformedOrientation = 	(fixedStartDirection + (startOrientation - inertialData.angle));				//(90 + (inertialData.angle - startOrientation)) % 360;
		
		inertialData.angle = transformedOrientation;
//		inertialData.distance = inertialData.distance * 1.15f;
		RealMatrix F = PredictionModel.updatePredictionModel(inertialData);
		
		double kalmanGain = calcKalmanGainValue(); 
//		System.out.println("K=" + kalmanGain);
		RealMatrix K = MatrixUtils.createRealMatrix(new double[][]{{kalmanGain, 0, 0}, {0, kalmanGain, 0}, {0, 0, kalmanGain}});
		RealMatrix z = MatrixUtils.createRealMatrix(new double[][]{{markerBasedPosition.x}, {markerBasedPosition.z}, {1}});
        KF.predictWithoutQ(F);
//        System.out.println("F=" + F.toString());
//        System.out.println("X-State after prediction: " + KF.getState());
        KF.updateWithModifiedKalmanGainCalc(K, H, z);
        X = KF.getState();
//        System.out.println("X-State after measurement-update: " + KF.getState());
        return new Position(X.getEntry(0, 0), markerBasedPosition.y, X.getEntry(1, 0), 0);	//TODO - frame Time
	}
	
	private double calcKalmanGainValue() { /// TODO rethink of calculation
//		return 0.5;
		if(kgDistance > 1.0) {
//			return 0.01;
			return (kgPercentage * 0.2);
		} else {
			return 0.8;
		}
	}
	
	public static double calcDistanceBetweenPoints(Position p1, Position p2) {
		double xDelta = p2.x - p1.x;
		double zDelta = p2.z - p1.z;
		return Math.sqrt((xDelta*xDelta) + (zDelta*zDelta));
	}
}
