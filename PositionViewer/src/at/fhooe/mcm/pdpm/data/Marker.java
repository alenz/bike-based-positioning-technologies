package at.fhooe.mcm.pdpm.data;

public class Marker {

	public int id;
	public Position pos;
	public float size;	//one side of the marker-square
	public int angleX;
	public int angleY;
	public int angleZ;
	
	public Marker(int id, Position pos, float size, int angleX, int angleY, int angleZ) {
		this.id = id;
		this.pos = pos;
		this.size = size;
		this.angleX = angleX;
		this.angleY = angleY;
		this.angleZ = angleZ;
	}
}
