package at.fhooe.mcm.pdpm.data;

import java.util.Arrays;

public class SensorEvent{

	public int sensorType;		// the hardware-sensor which can gather a bunch of different sensor-values (sensorData), e.g: BLEBikeSensor
	public int sensorDataType;	// one specific value which senses a hardware-sensor, e.g: Speed, Heartrate, Cadence,...
	
	public float[] values;
	public long timestamp;
	public int accuracy;
	
	public SensorEvent(int sensorDataType, int sensorType, float[] values, long timestamp, int accuracy) {
		this.sensorDataType = sensorDataType;
		this.sensorType = sensorType;
		this.values = values;
		this.timestamp = timestamp;
		this.accuracy = accuracy;
	}
	
	@Override
	public String toString() {
		return "sensordatatype=" + sensorDataType + ", values=" +  Arrays.toString(values);
	}
}
