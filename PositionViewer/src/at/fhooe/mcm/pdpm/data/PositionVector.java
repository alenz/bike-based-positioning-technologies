package at.fhooe.mcm.pdpm.data;

import at.fhooe.mcm.pdpm.Util;

public class PositionVector {
	
	private static final String TAG = "PositionVector";
	
	public Position startPosition, endPosition;
	
	public float distance;	// in cm
	public float orientation; // in �
	
	public PositionVector(Position startPosition, float distance, float orientation) {
		this.startPosition = startPosition;
		this.distance = distance;
		this.orientation = orientation;
		this.endPosition = calcEndPosition(startPosition, distance, orientation);
	}
	
	/**
	 * Method to calculate a new Position out of a startPosition, a distance and an orientation
	 * @param startPosition
	 * @param distance
	 * @param orientation // in degrees � -> will be internally transformed to radians which is neccesary for the cos-/sin-calc
	 * @return new Position calculated from a startPosition, a distance and an orientation
	 */
	public static Position calcEndPosition(Position startPosition, float distance, float orientation) {
		double deltaX = Math.cos(Math.toRadians((double)orientation)) * distance;
		double deltaY = Math.sin(Math.toRadians((double)orientation)) * distance;
		Util.log(TAG, "dist: " + distance + "  | orient: " + orientation + "  | deltaX: " + deltaX + "  | deltaY: " + deltaY);
		// TODO: !!!!!! important !!!!!!!!!  check if y and z are changed (for MarkerDetectionPosCalc they are currently changed)
		return new Position(startPosition.x + deltaX, startPosition.y + deltaY, startPosition.z, -1);
	}
}
