package at.fhooe.mcm.pdpm.data;

public class InertialSessionSensorEvent extends SensorEvent{

	public long sessionTime;
	
	public InertialSessionSensorEvent(SensorEvent sensorEvent, long sessionTime) {
		super(sensorEvent.sensorDataType, sensorEvent.sensorType, sensorEvent.values, sensorEvent.timestamp, sensorEvent.accuracy);
		this.sessionTime = sessionTime;
	}
	
	public InertialSessionSensorEvent(int sensorDataType, int sensorType,
			float[] values, long timestamp, int accuracy) {
		super(sensorDataType, sensorType, values, timestamp, accuracy);
		sessionTime = -1;
	}
	
	@Override
	public String toString() {
		return "<SessionSensorEvent: sessionTime=" + sessionTime + ", " + super.toString() + " >";
	}
}
