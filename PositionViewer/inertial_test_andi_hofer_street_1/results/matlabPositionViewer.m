filenameX = 'positionsForMatlabX.data';
filenameY = 'positionsForMatlabY.data';

filenameXsplitted = 'positionsForMatlabX_splitted.data';
filenameYsplitted = 'positionsForMatlabY_splitted.data';

[X,delimiterOut]=importdata(filenameX)
[Y,delimiterOut]=importdata(filenameY)

[Xsplitted,delimiterOut]=importdata(filenameXsplitted)
[Ysplitted,delimiterOut]=importdata(filenameYsplitted)

markerPosX = [100, 0, 200];
markerPosY = [500, 0, 0];

figure
plot(X,Y,'xr',Xsplitted, Ysplitted, 'xb', markerPosX, markerPosY, 'db')
title('Inertial Street Block 3 with Splitted Data comparision');
xlabel('x [cm]');
ylabel('y [cm]');
legend('Inertial-Only Positions', 'Inertial-Only Positions (splitted)','Marker-Pos');
grid on