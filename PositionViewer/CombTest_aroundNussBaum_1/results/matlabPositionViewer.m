%filenameX = 'positionsForMatlabX_avgExt1.data';
%filenameY = 'positionsForMatlabZ_avgExt1.data';

filenameX = 'positionsForMatlabX_inertSlow.data';
filenameY = 'positionsForMatlabY_inertSlow.data';


[X,delimiterOut]=importdata(filenameX)
[Y,delimiterOut]=importdata(filenameY)

markerPosX = [-0.79, 0, 0.80];
markerPosY = [0, 0, 0];

figure
plot(X,Y,'xr',markerPosX, markerPosY, 'db')
title('Combined Test 1 (5m straigt)');
xlabel('x [cm]');
ylabel('y [cm]');
legend('Inertial-Only Positions','Marker-Pos');
grid on, axis equal

%figure
%t = linspace( 0, 1, numel(X) ); % define the parameter t
% fitX = fit( t, X, 'cubicinterp'); % fit x as a function of parameter t
% fitY = fit( t, Y, 'cubicinterp'); % fit y as a function of parameter t
% plot( fitX, fitY, 'r-' ); % plot the parametric curve