%!!!!!!!!  axis([0 10 0 10])     !!!!!!! to set what should be shown on the
%diagram


%filenameXFilteredAvgExt1 = 'positionsForMatlabX_avgExt1.data';
%filenameYFilteredAvgExt1 = 'positionsForMatlabZ_avgExt1.data';

filenameXFilteredAvgExt1 = 'MatlabSmoothedMarkerBasedPositionsX.txt';
filenameYFilteredAvgExt1 = 'MatlabSmoothedMarkerBasedPositionsY.txt';

filenameX = 'positionsForMatlabX_marker_based.data';
filenameY = 'positionsForMatlabZ_marker_based.data';


filenameXreferencePoints = 'matlab_x_reference_points_bigsplus.csv';
filenameYreferencePoints = 'matlab_y_reference_points_bigsplus.csv';

filenameXInertSpl = 'positionsForMatlabX_inertSpl.data';
filenameYInertSpl = 'positionsForMatlabY_inertSpl.data';

filenameXKalman = 'CustomKalmanFilteredX.data';
filenameYKalman = 'CustomKalmanFilteredZ.data';

[X,delimiterOut]=importdata(filenameX);
[Y,delimiterOut]=importdata(filenameY);

[Xfiltered,delimiterOut]=importdata(filenameXFilteredAvgExt1);
[Yfiltered,delimiterOut]=importdata(filenameYFilteredAvgExt1);

[XreferencePoints,delimiterOut]=importdata(filenameXreferencePoints);
[YreferencePoints,delimiterOut]=importdata(filenameYreferencePoints);

[XinertSplit,delimiterOut]=importdata(filenameXInertSpl)
[YinertSplit,delimiterOut]=importdata(filenameYInertSpl)

[Xkalman,delimiterOut]=importdata(filenameXKalman);
[Ykalman,delimiterOut]=importdata(filenameYKalman);

markerPosX = [-0.79, 0, 0.80];
markerPosY = [0, 0, 0];

figure
plot(X,Y,'xr', XreferencePoints, YreferencePoints, 's', Xfiltered, Yfiltered, 's', XinertSplit, YinertSplit, 'h', Xkalman, Ykalman, '-')
title('Combined Test 1 (5m straigt)');
xlabel('x [cm]');
ylabel('y [cm]');
legend('raw marker based positions','Reference Points', 'smoothed marker based positions', 'Dead Reckoning Data', 'Kalman filtered Positions');
grid minor, grid on, axis equal,  axis([-1 11 -7 20]) ;

accuracies = zeros(size(Xkalman));

% calc accuricies
for idxCalcPoints = 1:numel(Xkalman)
    minDistance = 9999.9;
    currentCalcPointX = Xkalman(idxCalcPoints);
    currentCalcPointY = Ykalman(idxCalcPoints);
    for idxReferencePoints = 2:numel(XreferencePoints)
        p1x = XreferencePoints(idxReferencePoints-1);
        p1y = YreferencePoints(idxReferencePoints-1);
        p2x = XreferencePoints(idxReferencePoints);
        p2y = YreferencePoints(idxReferencePoints);
        
        currentDistanceFromPointToLineSegment = distancePointToLineFunction(p1x, p1y, p2x, p2y, currentCalcPointX, currentCalcPointY);
        if currentDistanceFromPointToLineSegment < minDistance
            minDistance = currentDistanceFromPointToLineSegment;
        end
    end;
    
    accuracies(idxCalcPoints) = minDistance;
end;

avgAccuracy = mean(accuracies);
figure
hist(accuracies, 10);
title('Histogram of the position errors');
xlabel('Error [m]');
ylabel('Nr. of samples');

[fAccuracyEcdf,xEsj] = ecdf(accuracies);
figure()
plot(xEsj,fAccuracyEcdf, 'g')
hold on
%plot(xEdigi, fErrorDigiEcdf, 'b');
%plot(xEnex, fErrorNexusEcdf, 'c');
%legend('SJCAM', 'DigiCam', 'Nexus 5');
title('Cumulative Error Distribution');
xlabel('Error (Accuracy) [m]');
ylabel('Cumulative Error Distribution Function');
grid on
hold off

%figure
%t = linspace( 0, 1, numel(X) ); % define the parameter t
% fitX = fit( t, X, 'cubicinterp'); % fit x as a function of parameter t
% fitY = fit( t, Y, 'cubicinterp'); % fit y as a function of parameter t
% plot( fitX, fitY, 'r-' ); % plot the parametric curve