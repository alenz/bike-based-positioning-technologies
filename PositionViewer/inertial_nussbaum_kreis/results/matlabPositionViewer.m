filenameX = 'positionsForMatlabX.data';
filenameY = 'positionsForMatlabY.data';

[X,delimiterOut]=importdata(filenameX)
[Y,delimiterOut]=importdata(filenameY)

markerPosX = [100, 0, 200];
markerPosY = [500, 0, 0];

figure
plot(X,Y,'xr',markerPosX, markerPosY, 'db')
title('Inertial Only Street Block 3');
xlabel('x [cm]');
ylabel('y [cm]');
legend('Inertial-Only Positions','Marker-Pos');
grid on