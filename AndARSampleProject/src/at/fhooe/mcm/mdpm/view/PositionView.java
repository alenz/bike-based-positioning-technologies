package at.fhooe.mcm.mdpm.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import at.fhooe.mcm.mdpm.data.Position;

import java.util.ArrayList;

public class PositionView extends View {
	private Paint linePaint;
	private Paint textPaint;

	private Position currentPosition;

	private float[] points = new float[1000];
	private Path path = new Path();
	private int numberOfStoredPoints = 0;
	private int currentIdx = 0;
	
	private int centerScreenX = 0;
	private int centerScreenY = 0;

	public PositionView(Context context, AttributeSet attrs) {
		super(context, attrs);

		linePaint = new Paint();
		linePaint.setAntiAlias(true);
		linePaint.setStrokeWidth(10f);
		linePaint.setColor(Color.BLACK);
		linePaint.setStyle(Paint.Style.STROKE);
		linePaint.setStrokeJoin(Paint.Join.ROUND);
		
		textPaint = new Paint();
		textPaint.setAntiAlias(true);
		textPaint.setStrokeWidth(1f);
		textPaint.setColor(Color.RED);
		textPaint.setStyle(Paint.Style.FILL_AND_STROKE);
		textPaint.setStrokeJoin(Paint.Join.ROUND);
		textPaint.setTextSize(40);
		
		numberOfStoredPoints = 0;
		currentIdx = 0;
		points = new float[1000];
		path = new Path();
		currentPosition = new Position(centerScreenX, centerScreenY, 0);
		centerScreenX = 500;
		centerScreenY = 300;
		setLayoutParams(new LayoutParams(1000, 600));
		path.moveTo(centerScreenX, centerScreenY);
	}

	@Override
	protected void onDraw(Canvas canvas) {

//		canvas.drawPath(path, linePaint);
//		canvas.drawCircle(centerScreenX+(currentPosition.x), centerScreenY+(currentPosition.y), 15, textPaint);
		canvas.drawText("Pos.: " + currentPosition.x + " , " + currentPosition.y + " , " + currentPosition.z, 50, 50, textPaint);
//		canvas.drawText("")
	}

	public void addCurrentPosition(Position currentPos) {
		currentPosition = currentPos;
		path.lineTo(centerScreenX+(currentPos.x), centerScreenY+(currentPos.y));
		postInvalidate();
	}
	
//	public void setCurrentTransMatrix
	
	private void moveToScreenCenter(Point p) {
		
	}
}