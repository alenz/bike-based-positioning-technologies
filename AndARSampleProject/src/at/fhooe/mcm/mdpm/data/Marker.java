package at.fhooe.mcm.mdpm.data;

import java.nio.FloatBuffer;
import java.util.Arrays;

import javax.microedition.khronos.opengles.GL10;

import Jama.Matrix;
import android.graphics.Point;
import android.renderscript.Matrix4f;
import android.util.Log;

import edu.dhbw.andar.ARObject;
import edu.dhbw.andar.ARToolkit;
import edu.dhbw.andar.pub.SimpleBox;
import edu.dhbw.andar.util.GraphicsUtil;

/**
 * The Custom-Marker-Class which calculates the current camera position 
 * with the markers current Transition-Matrix
 */
public class Marker extends ARObject {

	private Position markerPosition;
	
	public Marker(String name, String patternName, double markerWidth,
			double[] markerCenter, Position position) {
		
		super(name, patternName, markerWidth, markerCenter);
		markerPosition = position;
		float mat_ambientf[] = { 0f, 1.0f, 0f, 1.0f };
		float mat_flashf[] = { 0f, 1.0f, 0f, 1.0f };
		float mat_diffusef[] = { 0f, 1.0f, 0f, 1.0f };
		float mat_flash_shinyf[] = { 50.0f };

		mat_ambient = GraphicsUtil.makeFloatBuffer(mat_ambientf);
		mat_flash = GraphicsUtil.makeFloatBuffer(mat_flashf);
		mat_flash_shiny = GraphicsUtil.makeFloatBuffer(mat_flash_shinyf);
		mat_diffuse = GraphicsUtil.makeFloatBuffer(mat_diffusef);

	}

	public Marker(String name, String patternName, double markerWidth,
			double[] markerCenter, float[] customColor, Position position) {
		
		super(name, patternName, markerWidth, markerCenter);
		markerPosition = position;
		float mat_flash_shinyf[] = { 50.0f };

		mat_ambient = GraphicsUtil.makeFloatBuffer(customColor);
		mat_flash = GraphicsUtil.makeFloatBuffer(customColor);
		mat_flash_shiny = GraphicsUtil.makeFloatBuffer(mat_flash_shinyf);
		mat_diffuse = GraphicsUtil.makeFloatBuffer(customColor);

	}

	/**
	 * Just a box, imported from the AndAR project.
	 */
	private SimpleBox box = new SimpleBox();
	private FloatBuffer mat_flash;
	private FloatBuffer mat_ambient;
	private FloatBuffer mat_flash_shiny;
	private FloatBuffer mat_diffuse;

	private double[] invTrans = new double[12];

	/**
	 * Everything drawn here will be drawn directly onto the marker, as the
	 * corresponding translation matrix will already be applied.
	 */
	@Override
	public final void draw(GL10 gl) {
		super.draw(gl);

		gl.glMaterialfv(GL10.GL_FRONT_AND_BACK, GL10.GL_SPECULAR, mat_flash);
		gl.glMaterialfv(GL10.GL_FRONT_AND_BACK, GL10.GL_SHININESS,
				mat_flash_shiny);
		gl.glMaterialfv(GL10.GL_FRONT_AND_BACK, GL10.GL_DIFFUSE, mat_diffuse);
		gl.glMaterialfv(GL10.GL_FRONT_AND_BACK, GL10.GL_AMBIENT, mat_ambient);

		// draw cube
		gl.glColor4f(0, 1.0f, 0, 1.0f);
		gl.glTranslatef(0.0f, 0.0f, 12.5f);

		// draw the box
		box.draw(gl);
	}

	@Override
	public void init(GL10 gl) {

	}

	private Matrix transMat;
	private Matrix zeroPosMat = new Matrix(new double[][] {{0},{0},{0},{1}});
	private Matrix currentPosMat;
	
	public synchronized Position getUserPosition() {
		if (isVisible()) {			
			double[] transmat = getTransMatrix();
			transMat = new Matrix(new double[][]{	{transmat[0], transmat[1], transmat[2], transmat[3]},
													{transmat[4], transmat[5], transmat[6], transmat[7]},
													{transmat[8], transmat[9], transmat[10], transmat[11]},
													{0,0,0,1}});
			
			currentPosMat = (transMat.inverse()).times(zeroPosMat);
			
			// ARToolkit.arUtilMatInv(transmat, invTrans);
			double marker_x = transmat[3];
			double marker_y = transmat[7];
			double marker_z = transmat[11];			
			
			Log.i("Marker_Pos", "x: " + marker_x + " , y: " + marker_y
					+ " , z: " + marker_z);

			Log.i("Trans_Mat", "TransMat: " + Arrays.toString(transmat));
//			return new Position(currentPosMat.get(0, 0), currentPosMat.get(1, 0), currentPosMat.get(2, 0));
			return new Position(markerPosition.x - marker_x, marker_y - markerPosition.y, marker_z + markerPosition.z);
		} else {
			return null;
		}

	}
}
