package at.fhooe.mcm.mdpm.threads;

import java.util.Vector;

import android.os.Handler;
import at.fhooe.mcm.mdpm.data.Marker;
import at.fhooe.mcm.mdpm.data.Position;

public class PositionCalculator {

	private Vector<Marker> marker;
	private Position currentUserPosition;
	
	
	public PositionCalculator() {
		marker = new Vector<Marker>();
	}
	
	public void addPositionMarker(Marker m) {
		marker.add(m);
	}
	
	public Position calcCurrentUserPosition() {
		int sumX = 0, sumY = 0, sumZ = 0;
		int numberOfVisibleMarkers = 0;
		Position currentUserPositionOfMarker = null;
		for (Marker m: marker) {
			if (m.isVisible()) {
				currentUserPositionOfMarker = m.getUserPosition();
				if(currentUserPositionOfMarker != null) {
					sumX = sumX + currentUserPositionOfMarker.x;
					sumY = sumY + currentUserPositionOfMarker.y;
					sumZ = sumZ + currentUserPositionOfMarker.z;
					numberOfVisibleMarkers++;
				}
			}
		}
		if(numberOfVisibleMarkers < 1) {
			return null;
		}
		
		Position calculatedAvgPosition = new Position(sumX / numberOfVisibleMarkers, sumY / numberOfVisibleMarkers, sumZ / numberOfVisibleMarkers);
		return calculatedAvgPosition;
	}
	
 
}
