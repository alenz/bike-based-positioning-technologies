package at.fhooe.mcm.mdpm.threads;

import edu.dhbw.andar.sample.CustomActivity;
import android.net.MailTo;
import android.os.Handler;
import android.util.Log;
import at.fhooe.mcm.mdpm.data.Position;
import at.fhooe.mcm.mdpm.view.PositionView;

public class UpdateThread extends Thread {

	private PositionCalculator posCalculator;
	private boolean running = true;
	private PositionView positionView;
	private Handler btHandler;
	
	public UpdateThread(PositionCalculator calculator, PositionView posView, Handler handler) {
		this.posCalculator = calculator;
		this.positionView = posView;
		this.btHandler = handler;
		setDaemon(true);
		start();
	}
	
	@Override
	public void run() {
		Position p = new Position(50, 50, 50);
		
		super.run();
		while(running) {
			
			p = posCalculator.calcCurrentUserPosition();
			if(p != null) {
				btHandler.obtainMessage(CustomActivity.WRITE_POS_DATA, p).sendToTarget();
				positionView.addCurrentPosition(p);
			}
			
			
			try {
				sleep(300);
			} catch (InterruptedException e) {
				Log.e("UpdateThread::", e.toString());
			}
		}
	}

	public void setRunning(boolean b) {
		running = b;
	}
}
