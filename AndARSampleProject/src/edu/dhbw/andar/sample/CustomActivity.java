package edu.dhbw.andar.sample;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.UUID;

import com.google.gson.Gson;


import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.widget.FrameLayout;
import android.widget.Toast;
import at.fhooe.mcm.mdpm.data.Marker;
import at.fhooe.mcm.mdpm.data.Position;
import at.fhooe.mcm.mdpm.threads.PositionCalculator;
import at.fhooe.mcm.mdpm.threads.UpdateThread;
import at.fhooe.mcm.mdpm.view.PositionView;
import edu.dhbw.andar.ARToolkit;
import edu.dhbw.andar.AndARActivity;
import edu.dhbw.andar.exceptions.AndARException;

/**
 * Example of an application that makes use of the AndAR toolkit.
 * @author Tobi
 *
 */
public class CustomActivity extends AndARActivity{

	public static final String TAG = "MDPM::";
	
	Marker hiroObject;
	Marker kanjiObject;
	ARToolkit artoolkit;
	
	UpdateThread updateThread;
	PositionCalculator posCalculator;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		
		FrameLayout layout = getBaseFrameLayout();
		
		PositionView positionView = new PositionView(this, null);
		layout.addView(positionView);
		
		CustomRenderer renderer = new CustomRenderer();//optional, may be set to null
		super.setNonARRenderer(renderer);//or might be omited
		
		// BT Begin
		
		mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
		
		if (mBluetoothAdapter == null) {
			Toast.makeText(this, "Bluetooth is not available", Toast.LENGTH_LONG)
				.show();
			finish();
		} else {
			int btState = mBluetoothAdapter.getState();
			
			if (btState == BluetoothAdapter.STATE_OFF) {
				Log.i(TAG, "Bluetooth is off");
			} else if (btState == BluetoothAdapter.STATE_ON) {
				initializeBluetooth();
			}
		}	
		
		// BT End
		
		
		try {
			//register a object for each marker type
			artoolkit = super.getArtoolkit();
			
			kanjiObject = new Marker
					("kanji_pattern", "kanji.pat", 170.0, new double[]{0,0}, new Position(300, 0, 250));
				artoolkit.registerARObject(kanjiObject);
			
				
			// width = 90 for small marker
			hiroObject = new Marker
				("hiro_pattern", "patt.hiro", 170.0, new double[]{0,0}, new Position(-300, 0, -250));
			artoolkit.registerARObject(hiroObject);
			
//			someObject = new CustomObject
//			("test", "android.patt", 80.0, new double[]{0,0});
//			artoolkit.registerARObject(someObject);
//			someObject = new CustomObject
//			("test", "barcode.patt", 80.0, new double[]{0,0});
//			artoolkit.registerARObject(someObject);
			
//			ARToolkit.arUtilMatMul(new double[]{1,1,1,1,1,1,1,1,1,1,1,1}, new double[]{1,1,1,1,1,1,1,1,1,1,1,1}, new double[]{1,1,1,1,1,1,1,1,1,1,1,1});
			
			posCalculator = new PositionCalculator();
			posCalculator.addPositionMarker(kanjiObject);
			posCalculator.addPositionMarker(hiroObject);
			
				
			updateThread = new UpdateThread(posCalculator, positionView, btHandler);
			
		} catch (AndARException ex){
			//handle the exception, that means: show the user what happened
			System.out.println("");
		}		
		startPreview();
	}

	/**
	 * Inform the user about exceptions that occurred in background threads.
	 * This exception is rather severe and can not be recovered from.
	 * TODO Inform the user and shut down the application.
	 */
	@Override
	public void uncaughtException(Thread thread, Throwable ex) {
		Log.e("AndAR EXCEPTION", ex.getMessage());
		if(updateThread!= null)
			updateThread.setRunning(false);
		finish();
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		if(updateThread!= null)
			updateThread.setRunning(false);
	}

	
	public static final UUID MY_UUID = UUID
			.fromString("f895eaf0-867f-11e3-baa7-0800200c9a66");
	
	private static final int SOCKET_CONNECTED = 1;
	private static final int DATA_RECEIVED = 2;
	public static final int WRITE_POS_DATA = 3;
	
	private BluetoothAdapter mBluetoothAdapter = null;
	
	private ConnectionThread mBluetoothConnection;
	
	private boolean connected = false;
	
	private Handler btHandler = new Handler(new Handler.Callback() {
		
		private Gson gson = new Gson();
		
		@Override
		public boolean handleMessage(Message msg) {
			switch (msg.what) {
			case SOCKET_CONNECTED:
				mBluetoothConnection = (ConnectionThread) msg.obj;
				BluetoothDevice device = mBluetoothConnection.mmBluetoothSocket
						.getRemoteDevice();
				Log.i(TAG, "Connected to " + device.getName() +
						" (" + device.getAddress() + ")");
				String helloPosition = gson.toJson(new Position(500, 100, 10));				
				connected = true;
				mBluetoothConnection.write(helloPosition.getBytes());
				addNewMessage(mBluetoothAdapter.getName(), "writed Position: " + helloPosition);
				break;
			case DATA_RECEIVED:
				String data = (String) msg.obj;
				addNewMessage(mBluetoothConnection.mmBluetoothSocket
						.getRemoteDevice().getName()
						, data);
				break;
			case WRITE_POS_DATA:
				Position pos = (Position) msg.obj;
				if(connected && (pos != null)) {
					String jsonString = gson.toJson(pos);	
					mBluetoothConnection.write(jsonString.getBytes());
					addNewMessage(mBluetoothAdapter.getName(), "writed Position: " + jsonString);
				}
				break;
			}
			return true;
		}
	});
	
	public class ConnectionThread extends Thread {
		BluetoothSocket mmBluetoothSocket;
		private final Handler mmHandler;
		private InputStream mmInStream;
		private OutputStream mmOutStream;
		
		public ConnectionThread(BluetoothSocket socket, Handler handler) {
			super();
			mmBluetoothSocket = socket;
			mmHandler = handler;
			try {
				mmInStream = mmBluetoothSocket.getInputStream();
				mmOutStream = mmBluetoothSocket.getOutputStream();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		@Override
		public void run() {
			byte[] buffer = new byte[1024];
			int len;
			while (true) {
				try {
					len = mmInStream.read(buffer);
					String data = new String(buffer, 0, len);
					mmHandler.obtainMessage(DATA_RECEIVED, data)
						.sendToTarget();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		
		public void write(byte[] bytes) {
			try {
				mmOutStream.write(bytes);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	class AcceptThread extends Thread {
		private final Handler mmHandler;
		private BluetoothServerSocket mmServerSocket;
		private BluetoothSocket mmSocket = null;
		
		public AcceptThread(Handler handler) {
			mmHandler = handler;
			try {
				mmServerSocket = mBluetoothAdapter
						.listenUsingRfcommWithServiceRecord
						("Bluetooth demo", MY_UUID);
			} catch (IOException e) {
				
			}
		}
		
		public void run() {
			while (true) {
				try {
					mmSocket = mmServerSocket.accept();
					ConnectionThread conn = new ConnectionThread
							(mmSocket, mmHandler);
					mmHandler.obtainMessage(SOCKET_CONNECTED, conn)
						.sendToTarget();
					conn.start();
					mmServerSocket.close();
					break;
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	private void addNewMessage(String name, String message) {
		Log.i(TAG, name + ": " + message + "\n");
	}
	
	private void initializeBluetooth() {
		Log.i(TAG, "Bluetooth is on");
		Log.i(TAG, "My device name: " +
					mBluetoothAdapter.getName() + " (" +
					mBluetoothAdapter.getAddress() + ")");
		new AcceptThread(btHandler).start();
	}
}
